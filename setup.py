#!/usr/bin/env/python3

from setuptools import setup

setup(
  name="poul",
  version="0.1",
  description=("A Python3 tool for handling and analysing",
    " vietnamese poultry farms exploitation data."),
  keywords="poultry database analysis",
  url="https://github.com/bennguvaye/Poultry_farms",
  author="Benjamin Nguyen-Van-Yen",
  author_email="benjamin.nguyen.van.yen@gmail.com",
  modules=[
    'poul',
  ],
  install_requires=[
    "Pillow",
    "numpy",
    "pandas",
    "matplotlib",
    "PyQt5",
  ],
)
