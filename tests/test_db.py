#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
(Partial) unit testing for the `db.dialog` module.


"""

import unittest
import logging
import sys
# import warnings
from string import ascii_uppercase as alphabet

# from sqlalchemy.exc import SAWarning

# import poul.util as util
import poul.db.declar as dec
import poul.db.dialog as dia


class Create_database_test(unittest.TestCase) :
  def setUp(self) :
    self.conn = dia.open_connection(":memory:")
    dia.create_database(
      self.conn,
      ["2000-01", "2010-12"],
      ['A', 'B'],
      [1, 1],
    )
    self.ses_factory = dia.sessionmaker(bind=self.conn)

  def test_btc(self) :
    ses = self.ses_factory()
    btc_l = ses.query(dec.Bird_type.code).all()
    for c1 in ["G", "V"] :
      for c2 in ["T", "D", "C"] :
        self.assertIn((c1 + c2,), btc_l)

  def test_communes(self) :
    ses = self.ses_factory()
    communes = ses.query(dec.Farm.commune).all()
    self.assertEqual(communes, [('A',), ('B',)])

  def test_years(self) :
    ses = self.ses_factory()
    years = ses.query(dec.Date.year).all()
    self.assertEqual(set([ y[0] for y in years]), set(range(2000, 2011)))

  def tearDown(self) :
    self.conn.close()



# def dummy_datapoint(n) :
#   row = dia.Entity_row(
#     Farm=dec.Farm(
#       # "id":n,
#       district=alphabet[n - 1],
#       commune=alphabet[n - 1],
#       farm_number=n
#     ),
#     Date=dec.Date(
#       # "id":n,
#       day=n,
#       month=n,
#       year=n
#     ),
#     Flock=dec.Flock(
#       # "id":n,
#       flock_number=n
#     ),
#     Page_scan=dec.Page_scan(is_recto=False),
#     Bird_type=dec.Bird_type(
#       code=alphabet[n - 1] * 2,
#       descr_en=alphabet[n - 1] * 3
#     ),
#     Confinement=dec.Confinement(
#       code=alphabet[n - 1] * 2,
#       descr=alphabet[n - 1] * 3
#     ),
#     Cause_of_death=dec.Cause_of_death(
#       code=alphabet[n - 1] * 2,
#       descr=alphabet[n - 1] * 3
#     )
#   )
#   row.complete_with_new()

#   return dia.Datapoint(row)



if __name__ == '__main__' :
  logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
  unittest.main()
