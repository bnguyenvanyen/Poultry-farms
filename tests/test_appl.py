#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
Unit test for our project

"""

import logging
import unittest
import sys

from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt

import poul.db.declar as dec
import poul.db.dialog as dia

import poul.appl.blocks as abl
import poul.appl.update as aup
# import poul.appl.layout as ala
import poul.appl.main as ama

logger = logging.getLogger(__name__)


class Text_edit_test(unittest.TestCase) :
  def setUp(self) :
    self.mw = abl.QWidget()
    self.wid = abl.Text_edit(self.mw)
    self.wid_bis = abl.QWidget(self.mw)

  def test_value_set(self) :
    # this can't work, since it's the editor
    self.wid.value = "AAA"
    self.wid.internal_value_set.emit()
    ama.app.processEvents()
    self.assertEqual(self.wid.text(), "AAA")

  @unittest.skip("Can't focusIn / focusOut correctly")
  def test_set_with(self) :
    self.wid.setText("BBB")
    self.wid.setFocus()  # why
    QTest.keyPress(self.wid, Qt.Key_Tab)
    ama.app.processEvents()
    self.assertEqual(self.wid.value, "BBB")


class Action_chain_test(unittest.TestCase) :
  def test_empty_chain(self) :
    chain = abl.Action_chain()
    chain.process()
    # assert completed NOT emitted

  def test_flow_simple(self) :
    pass

  def test_aborted(self) :
    pass


class Update_test(unittest.TestCase) :
  def setUp(self) :
    self.mw = abl.QWidget()
    self.conn = dia.open_connection(':memory:')
    dia.create_database(self.conn, ["2000-01", "2010-12"], ["A"], [5])
    self.ses_factory = dia.sessionmaker(bind=self.conn)
    self.imw = abl.Image_view()

  def test_img(self) :
    ses = self.ses_factory()
    up = aup.Update(self.mw, ses, image_win=self.imw)
    up.set_image("test_form.png")

  def test_data(self) :
    ses = self.ses_factory()
    up = aup.Update(self.mw, ses, image_win=self.imw)
    farm = ses.query(dec.Farm).all()[0]
    date = ses.query(dec.Date).all()[0]
    page = dec.Page(
      id=1,
      is_recto=True,
      path="test_form.png",
      new=True,
      template_key="front_chicken_before_20151103",
      farm=farm,
      date=date
    )
    up.set_data(page)



if __name__ == '__main__' :
  logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
  unittest.main()
