#!/usr/local/bin/python3
# -*- coding: utf-8 -*-

"""
Unit test for our project

"""

import logging
import unittest
import sys

from poul import img

logger = logging.getLogger(__name__)

png_path = "tests/test_form.png"

class Image_test(unittest.TestCase) :
  def setUp(self) :
    self.image = img.open(png_path)

  # just for catching errors
  def test_to_from_db(self) :
    buf = img.to_database(self.image)
    img.from_database(buf)

  def test_bw(self) :
    img.make_black_and_white(self.image)


class Deskew_test(unittest.TestCase) :
  def test_deskew(self) :
    image = img.open(png_path)
    img.deskew(image)



if __name__ == '__main__' :
  logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
  unittest.main()
