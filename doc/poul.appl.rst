poul.appl subpackage
====================

Submodules
----------

poul.appl.blocks module
-----------------------

.. automodule:: poul.appl.blocks
    :members:
    :undoc-members:
    :show-inheritance:

poul.appl.query module
----------------------


poul.appl.update module
-----------------------

.. automodule:: poul.appl.update
    :members:
    :undoc-members:
    :show-inheritance:

poul.appl.main module
---------------------

.. automodule:: poul.appl.main
    :members:
    :undoc-members:
    :show-inheritance:

