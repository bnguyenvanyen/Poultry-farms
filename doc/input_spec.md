Specification of input format
=============================

Nothing (the empty string) is interpreted as `None`,
which stands for missing data (NA).


Boolean values
--------------

- `True` is 'True', 'T', 'Có' or 'C'
- `False` is 'False', 'F', 'Không' or 'K'


Enumeration values
------------------

This is for the 'bird_type', 'confinement' and 'cause_of_death' fields.
Starting to edit one of those fields should show a list of possibilities.
Inputing one of the codes (the first part in caps) and hitting Return
or changing to another field completes with the right value.
If the code doesn't exist, an error is returned.
If you want to create a new value for the enumeration, just follow the format
'{code} - {english description} - {vietnamese description}'.
The new value will be then be present in the list at the next query.

For missing values, we use a special code instead of nothing.  
- missing 'bird_type' : invalid
- missing 'confinement' : 'NA'
- missing 'cause_of_death' : 'NUL' (only when no deaths)


Query screen
------------

- A comma is an `OR` so that '2,3' means `2 OR 3`
- Integers separated by a hyphen specifies a python-like range,
so that '3-6' means `3 OR 4 OR 5`
- Empty fields `None` are ignored
- The values for the different fields are `AND`ed


Edit screen
-----------

You can only create a new flock directly to the right of an existing flock.
Otherwise it's an error.
If a flock is created by error,
just leave the bird type empty and it will be ignored on completing.

