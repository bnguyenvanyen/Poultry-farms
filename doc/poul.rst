poul package
============

.. automodule:: poul
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages and submodules
--------------------------

.. toctree::
    poul.interface
    poul.db
    poul.appl
    poul.img
    poul.flockmatch
    poul.plot
    poul.util

