.. Poultry-farms documentation master file, created by
   sphinx-quickstart on Mon Apr 24 11:02:11 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Poultry-farms's documentation!
=========================================

.. toctree::
    :maxdepth: 2

    tutorial
    input_spec
    poul


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
