Tutorial
========


Introduction
------------

This document presents the program to interface with smallholder poultry farm
databases.
The interface to the code is through `poul.py`,
which has both a GUI (relying on Qt) and (lighter) no GUI mode.

Complex operations like adding or editing data must use the GUI,
but simple operations like extracting or plotting some data can be done
without it.

For installation instructions and generalities, read the README file.


First steps
-----------

For the full tutorial we will place ourselves in the directory 
'3_database_and_tools'.

Under it we find :  
- 'README.md' contains with some simple information about the program.
- 'poul.py' the script to call to run the code.
- 'poul' a folder with the sources to the program
- 'tests' a folder that contains scripts to test the program (not up to date).
- 'doc' contains the documentation, including this tutorial.
- 'db' contains the database files by default.

First of all you should run the test suite on your system, 
to check that everything is well :  
```
cd /path/to/3_database_tools
python3 -m unittest tests
```
Warnings are normal, errors or failures are not.

Now, let's try the program.
Some help about the commands you can run is returned by
```
python3 poul.py -h
```

To try out the GUI, run
```
python3 poul.py gui
```
This launches the GUI by default, which should inform you 
that you are working in memory.

The bar at the top of the window contains the menus,
the bar at the bottom contains some useful information,
like the database file loaded, the language, or the ongoing action.


Setup
-----

The first menu, Setup, allows to change a few settings, 
like the database file we are connected to,
the model directory some settings are read from,
etc.
This can also be done from the command line.

Click on `Setup > Set database`, and choose 'db/example.db' in the list of files,
then confirm 'sqlite' as the dialect.
This could also be achieved by putting `-db db/example.db` on the command line.

Notice also `Setup > Set language` makes it possible to switch 
from english to vietnamese for the form field names, 
which can make it easier to add or edit data while 
looking at the scans (in vietnamese) (WIP).

We can now explore the actions available to us.
The different menus are `Add`, `Edit`, `Extract` and `Delete`.

- `Display` is for looking at some subset of the data spreadsheet-style
- `Edit` is for editing some data in the database.  
  You can for example query data that might contain errors via some filter,
  then correct the errors.
- `Extract` makes it possible to get data out of the database,
  to a csv format for example manipulable with R or Excel (or Python !).
- `Delete` deletes data corresponding to some filter (be careful !).


Edit
----

`New pages` is for those forms that have never been edited to completion.
`Pages from query` show a first screen where you can select the pages you want to edit.


Display
-------

`Display by flock id` on a database where the flock ids are set (with the 'match' action)
will display for each flock the data for each month side by side.



Extract
-------

Let's extract the data to see what it looks like.
Under `Extract`, we see several options :  
- `Extract > All datapoints` All columns and rows as one csv file.
- `Extract > Datapoints from query` All columns and the rows 
corresponding to the constraints
- `Extract > Data subset from query` The specified columns 
and specified rows.

Click `Extract > Data subset from query`.

The first step allows you to choose what columns 
you want to extract from the database.
Choose `Farm.district`, `Flock.age`, `Flock.number`, then click Choose.

The next step makes it possible to create a filter to query against.
Put "1-3" into `Flock.flock\_number`.
This tells the program that we only want flocks with a flock number of 1, 2,or 3.

It is also possible to give a list of values, like "1,2,3",
a single value like "1", or "None" to indicate that we want missing values.

Click Query.

The program should ask you to what file you want to extract to.
Choose `../test\_extract.csv`.
If you open the file, you should see a line giving the field names,
`Farm.district`, `Flock.flock_number`, `Flock.age`
then a few lines, where the last one corresponds to the flock we just added :
TB, 1, 3.


Simple use of the no GUI mode
-----------------------------

Some useful commands in No GUI mode are

```
python3 poul.py -db path/to/database.db extract-csv
```
Extracts all the data from the file at 'path/to/database.db' and puts it into
'../4_output/database.csv'.

```
python3 poul.py -db path/to/database.db match
```
rematches all flocks from scratch.
(finding which flocks are the same from month to month 
and from there giving them a 'flock_id')

```
python3 poul.py -db path/to/database.db plot missing
```
Puts a figure at '../4_output/database_missing.pdf' where the colored squares
correspond to the data present in the database.
Grey squares are for absent pages, then the intensity of color is for the number of flocks.

Other plot functions like 'plot simple' and 'plot farm' also create pdf files
into '4_output' with useful visualization of the data.


Other scripts
-------------

'poul_pdf_to_png.sh'

The two arguments are the source directory and the destination directory.
The script will look for pdf files for the Tan Phu and Tan Loc commune,
separate them into png files, and puts those in the destination directory
with always the same basename.

