#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
This script launches the Graphical User Interface for our poultry farms
with some simple things already set.

As such it will simply call application.Main_window() with some arguments
parsed from the command line.

The Graphical User Interface has several aims :
  Providing a convenient way to go through simple tasks.
  Being a robust fallback when the automatic software
  (diverse image processing routines) fail.

This second part is the most important.
As such, the program should always try to automate stuff, but allow the user
to correct mistakes every step of the way.
This should make for a good trade-off between speed and robustness.

"""

import argparse
import sys
import logging
import logging.config

from poul import parse, interface

logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(
  description="Interface to the poultry farms program.")

parser = parse.add_base_arguments(parser)
parser = parse.add_action_arguments(parser)

if __name__ == '__main__' :
  args = parser.parse_args(sys.argv[1:])
  d = vars(args)
  log_config = parse.log_config(**d)
  logging.getLogger("sqlalchemy.engine")
  logging.config.dictConfig(log_config)
  # logging.info("Launching GUI")
  conn = interface.open_connection(**d)
  try :
    # if args.safe :  FIXME
    #   interf.set_safe(conn)
    if args.action == 'create-db' :
      interface.create_database(conn=conn, **d)
    if args.action == 'add-forms' :
      interface.add_forms(conn=conn, **d)
    elif args.action == 'add-csv' :
      interface.add_csv(conn=conn, **d)
    elif args.action == 'sync' :
      interface.sync(conn=conn, **d)
    elif args.action == 'copy' :
      interface.copy(conn=conn, **d)
    elif args.action == 'match' :
      interface.match(conn=conn, **d)
    elif args.action == 'plot' :
      cmd_l = ['simple', 'pop', 'farm', 'traj', 'flock', 'match', 'missing']
      if d['cmd'] != 'all' :
        f_l = interface.plot(conn=conn, **d)
        interface.save_plot(f_l, **d)
      else :
        for cmd in cmd_l :
          d['cmd'] = cmd
          f_l = interface.plot(conn=conn, **d)
          interface.save_plot(f_l, **d)
    elif args.action == 'extract' :
      df = interface.extract(conn=conn, **d)
      interface.save_extract(df, **d)
    elif args.action == 'gui' :
      import matplotlib
      matplotlib.use('Qt5Agg')
      from poul.appl import blocks, main
      mw = main.Main_window(conn=conn, **d)
      gui_handler = blocks.Log_message_handler()
      logging.getLogger().addHandler(gui_handler)
      mw.process()
      sys.exit(main.app.exec_())
    else :
      # do the help thing
      pass
  finally :
    conn.close()
