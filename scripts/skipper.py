#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Save copies of forms with new naming schemes :
{year}-{month}_{TPH|TLO}_{farm number}_{front|back}_{0-9}.png
An image of the form is shown.
Hitting 'Return' will copy the image with the naming scheme.
Hitting 'Escape' will append the path to the image
for the next step with unskipper.py.

"""

import sys
import glob
from numpy import asarray
from PIL import Image as pili
from cv2 import imencode
import poul.util as util
import poul.appl.blocks as abl

app = abl.QApplication([])

try :
  pattern = sys.argv[1]  # protect with ""
except IndexError :
  pattern = "../2_split_png_files/*/*_phieu_*.png"

for img_path in glob.iglob(pattern) :
  folder_path, fname = util.path.split(img_path)
  re_d = util.decompose_image_fname(fname)
  farm_number = (1 + re_d['number']) // 2
  if re_d['number'] % 2 == 1 :
    side = 'front'
  else :
    side = 'back'
  new_fname = "{}-{:02d}_{}_{}_{}_1.png".format(
    re_d['year'],
    re_d['month'],
    re_d['commune'],
    farm_number,
    side,
  )
  new_img_path = "{}/{}".format(folder_path, new_fname)
  print(new_img_path)
  wid = abl.QWidget()
  sk = abl.Skipper(parent=wid)
  sk._pix.load(img_path)
  sk.reshrink()
  wid.show()
  app.exec_()
  if sk.skip :
    l = "Skipping : {} ; {}".format(fname, new_fname)
    print(l)
    with open("skipped_renames.txt", 'a') as f :
      f.write("{}\n".format(l))
  else :
    # print(new_img_path)  # FIXME
    util.copyfile(img_path, new_img_path)
