#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Check that each front has a back

"""
import glob
import os
import re


reg_front = re.compile(
  "(?P<year>201[0-9])-"
  "(?P<month>[0-9]{2})_"
  "(?P<commune>[TPHLO]{3})_"
  "(?P<farm_number>[0-9]+)_"
  "(?P<side>front)_"
  "(?P<page>[0-9]).png"
)


for folder in glob.iglob("../2_split_png_files/*") :
  print(folder)
  l = os.listdir(folder)
  for f in l :
    m = re.match(reg_front, f)
    if m :
      reg_back = re.compile(
        ".*{}-{}_{}_{}_back_{}\.png.*".format(
          m.group('year'),
          m.group('month'),
          m.group('commune'),
          m.group('farm_number'),
          m.group('page'),
        )
      )
      check = re.match(reg_back, ' '.join(l))
      if not check :
        print(f)
