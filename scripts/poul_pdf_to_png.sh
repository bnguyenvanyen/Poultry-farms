#!/bin/bash

# script arguments
# source folder
source=$1
# destination folder
dest=$2
date=$(basename "$source") # normally "yyyy-mm"


# Takes two arguments : source file and destination pattern
function pdf_to_png {
  pdfseparate  "$1" ""$2"_%d.pdf"
  for i in {1..100} # max page number. Careful
  do
    if [ -e ""$2"_"$i".pdf" -a ! -e ""$2"_"$i".png" ] 
    # if the pdf page doesn't exist or the png file already exists we skip.
    then
      # to png
      convert -density 300 ""$2"_"$i".pdf" ""$2"_"$i".png"
      rm ""$2"_"$i".pdf"
      # to grayscale average
      convert ""$2"_"$i".png" -set colorspace Gray -separate -average ""$2"_"$i".png"
    fi
  done
}

# regex to identify files
re=".*[Tt]an[- _][Pp]hu.*
.*[Tt]an[- _][Ll]oc.*"
# naming scheme
dis="phu
loc"

for n in 1 2 # loop over lines of $re and $dis
do
  pdf_l=$(find $source -regex "$(echo "$re" | sed ""$n"q;d")")
  # we want the uncorrected file (has all pages)
  pdf=$(echo "$pdf_l" | grep -v "orrect")
  echo "Working on original file $pdf"
  base_dest=""$dest"/"$date"_phieu_tan_"$(echo "$dis" | sed ""$n"q;d")""
  pdf_to_png "$pdf" "$base_dest"
  # do the same on the correction file if it exists, with different name
  pdf=$(echo "$pdf_l" | grep "orrect")
  if [ -n "$pdf" ]
  then 
    echo "Working on correction file $pdf"
    base_dest=""$base_dest"_cor"
    pdf_to_png "$pdf" "$base_dest"
  fi
done

