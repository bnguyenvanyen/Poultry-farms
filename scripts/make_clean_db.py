#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Make a database file containing all forms
(but no flock transcribed data).

If there are two pages, we combine them side to side.

"""
import glob

import sqlite3
import numpy as np
from PIL import Image as pili
from cv2 import imencode

import poul.util as util
import poul.img.preprocess as pre


reg = util.compile(
  "(?P<year>201[0-9])-"
  "(?P<month>[0-9]{2})_"
  "(?P<commune>[TPHLO]{3})_"
  "(?P<farm_number>[0-9]+)_"
  "(?P<side>(front)|(back))_"
  "(?P<page>[0-9]).png"
)

# we already have a db file with tables created thanks to
# dec.Base.metadata.create_all
conn = sqlite3.connect('new.db')
conn.row_factory = sqlite3.Row
c = conn.cursor()


# insert farms
i = 1
for commune, rg in [('TLO', range(1, 27)), ('TPH', range(1, 28))] :
  for j in rg :
    c.execute(
      "INSERT INTO farms(id, district, commune, farm_number) VALUES (?,?,?,?)",
      (i, 'TB', commune, j)
    )
    i += 1


# insert dates
i = 1
for year, rg in [(2015, range(6, 13)), (2016, range(13))] :
  for month in rg :
    c.execute(
      "INSERT INTO dates(id, month, year) VALUES (?,?,?)",
      (i, month, year)
    )
    i += 1
conn.commit()

# insert scans
i = 1
# FIXME
for path in glob.iglob("../2_split_png_files/**/*_T*_1.png") :
  print(path)
  folder_path, fname = util.os.path.split(path)
  m = util.match(reg, fname)
  year = int(m.group('year'))
  month = int(m.group('month'))
  commune = m.group('commune')
  farm_number = int(m.group('farm_number'))
  if year == 2015 and month < 11 :
    era = 'before_20151103'
  else :
    era = 'after_20151103'
  if m.group('side') == 'front' :
    is_recto = True
    sp = 'front_chicken'
  else :
    is_recto = False
    sp = 'back_duck'

  # look for following pages
  for j in range(10) :
    npath = "{}/{}-{:02d}_{}_{}_{}_{}.png".format(
      folder_path,
      year,
      month,
      commune,
      farm_number,
      m.group('side'),
      j
    )
    try :
      img = pili.open(npath)
    except FileNotFoundError :
      pass
    else :
      print(npath)
      na = pre.deskew(np.asarray(img))
      try :
        a = np.concatenate([a, na[:a.shape[0]]], axis=1)
      except NameError :
        a = na
  print(a.shape)
  # convert to raw
  _, raw_img = imencode(".png", a)
  del a  # for next pass
  # query farm_id
  farm_id = c.execute(
    "SELECT id FROM farms "
    "WHERE farms.commune = ? AND farms.farm_number = ?",
    (commune, farm_number)
  ).fetchone()['id']

  # query date_id
  date_id = c.execute(
    "SELECT id FROM dates "
    "WHERE dates.month = ? AND dates.year = ?",
    (month, year)
  ).fetchone()['id']

  c.execute(
    "INSERT INTO page_scans"
    "(id, farm_id, date_id, is_recto, fname, image, template_key, new) "
    "VALUES (?,?,?,?,?,?,?,?)",
    (i, farm_id, date_id, is_recto, fname, raw_img, sp + era, True)
  )
  i += 1
  conn.commit()

conn.close()
