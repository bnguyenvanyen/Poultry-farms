#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Save copies of forms with new naming schemes :
{year}-{month}_{TPH|TLO}_{farm number}_{front|back}_{0-9}.png
with visual confirmation. Skips badly numbered forms and outputs to stdout.


"""

import numpy as np
from PIL import Image as pili
import matplotlib.pyplot as plt

import poul.util as util

reg_skip = util.compile("Skipping : (?P<old_name>\S+) ; (?P<new_name>\S+)")
reg_name = util.compile(
  "(?P<date>201[0-9]-[0-9]{2})_"
  "(?P<commune>[TPHLO]{3})_"
  "(?P<new_fn>[0-9]+)_"
  "(?P<new_sd>(front)|(back))_"
  "[0-9].png"
)
reg_un = util.compile("(?P<old_path>\S+) -> (?P<new_path>.+)")

with open("skipped_renames.txt", 'r') as fr :
  with open("unskipped_renames.txt", 'r+') as fw :
    for line in fr :
      ms = util.match(reg_skip, line)
      old_name = ms.group('old_name')
      new_name = ms.group('new_name')
      mn = util.match(reg_name, new_name)
      new_fn = mn.group('new_fn')
      new_sd = mn.group('new_sd')
      old_path = "../2_split_png_files/{}/{}".format(
        mn.group('date'),
        old_name,
      )
      print(old_path)
      try :
        l = next(fw)
        mu = util.match(reg_un, l)
        if old_path != mu.group('old_path') :
          raise ValueError("Inconsistent files")
        # FIXME check lines correspond
      except StopIteration :
        img = np.asarray(
          pili.open("../2_split_png_files/{}/{}".format(
            mn.group('date'),
            old_name
          ))
        )
        plt.imshow(img[:1000])
        plt.show()
        fn = input("farm number ? [{}]".format(new_fn))
        sd = input("side ? [{}]".format(new_sd))
        n = input("number ? [1]")
        farm_number = new_fn if fn == '' else fn
        side = new_sd if sd == '' else sd
        number = '1' if n == '' else n

        new_path = "../2_split_png_files/{}/{}_{}_{}_{}_{}.png".format(
          mn.group('date'),
          mn.group('date'),
          mn.group('commune'),
          farm_number,
          side,
          number,
        )
        if util.path.exists(new_path) :
          fw.write("{} -> exists\n".format(old_path))
          print("File exists : {}".format(new_path))
        else :
          fw.write("{} -> {}\n".format(old_path, new_path))
          util.copyfile(old_path, new_path)
