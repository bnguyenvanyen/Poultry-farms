#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import glob
import os.path
import sys

import numpy as np

from poul import img

try :
  pattern = sys.argv[1]
except IndexError :
  pattern = "../2_split_png_files/*/*_T*_*_*_*.png"

print(pattern)

reg = re.compile(
  "(?P<year>201[0-9])-"
  "(?P<month>[0-9]{2})_"
  "(?P<commune>[TPHLO]{3})_"
  "(?P<farm_number>[0-9]+)_"
  "(?P<side>(front)|(back))_"
  "(?P<page>[0-9]).png"
)

for path in glob.iglob(pattern) :
  folder_path, fname = os.path.split(path)
  print(fname)
  m = re.match(reg, fname)
  year = int(m.group('year'))
  month = int(m.group('month'))
  commune = m.group('commune')
  farm_number = int(m.group('farm_number'))

  npath = os.path.join(
    folder_path,
    "{}-{:02d}_{}_{}_{}.png".format(
      year,
      month,
      commune,
      farm_number,
      m.group('side'),
    )
  )
  print(npath)
  if not os.path.exists(npath) :
    # look for following pages
    for j in range(10) :
      jpath = os.path.join(
        folder_path,
        "{}-{:02d}_{}_{}_{}_{}.png".format(
          year,
          month,
          commune,
          farm_number,
          m.group('side'),
          j
        )
      )
      try :
        na = img.deskew(img.open(jpath))
      except FileNotFoundError :
        pass
      else :
        print(jpath)
        try :
          a
        except NameError :
          a = na
        else :
          a = np.concatenate([a, na[:a.shape[0]]], axis=1)
    img.save(npath, a)
    del a
