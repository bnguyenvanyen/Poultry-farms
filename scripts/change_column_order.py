#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import sqlalchemy

from poul.db import declar, dialog

db_path = sys.argv[1]

engine = sqlalchemy.create_engine("sqlite:///{}".format(db_path))
conn = engine.connect()
conn.execute("ALTER TABLE flocks RENAME TO flocks_old")
declar.Flock.__table__.create(bind=conn)
conn.execute(
  "INSERT INTO flocks SELECT "
  "fo.page_id, fo.flock_number, fo.flock_id, fo.type_code, fo.size, "
  "fo.age, fo.age_of_depop, fo.all_in_all_out, fo.in_since_last, "
  "fo.sell_or_slaughter_since_last, fo.commercial_feed, fo.confinement_code, "
  "fo.grazing_duration, fo.grazing_range, fo.boot_change, fo.other_people, "
  "fo.which_people, fo.disinfectant, fo.death_since_last, "
  "fo.cause_of_death_code, fo.hpai, fo.date_hpai, "
  "fo.newcastle, fo.date_newcastle, fo.duck_plague, fo.date_duck_plague, "
  "fo.other, fo.date_other "
  "FROM flocks_old AS fo"
)
conn.execute("DROP TABLE flocks_old")
