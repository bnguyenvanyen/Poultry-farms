#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Summary
-------

This module handles all the image processing relative to the forms,
needed for reading in data.

"""
import logging
from io import BytesIO

import numpy as np
import PIL.Image as pili

from poul.resources import config, images
import poul.util as util

logger = logging.getLogger(__name__)

config_img_path = images.__path__[0]
config_img_path_l = [ util.path.join(config_img_path, sub)
                      for sub in images.__all__ ]

def open(source) :
  return np.asarray(pili.open(source))


def save(path, a) :
  from cv2 import imwrite
  imwrite(path, a)

def from_database(raw_string) :
  return open(BytesIO(raw_string))

def to_database(a) :
  from cv2 import imencode
  _, buf = imencode(".png", a)
  return buf


def make_black_and_white(img) :
  """
  Return a binary threshold filtered `img`.

  The threshold is read from `resources.config`

  """
  from cv2 import threshold, THRESH_BINARY_INV
  _, filt = threshold(
    img,
    config.img_bw_thresh,
    255,
    THRESH_BINARY_INV,
  )

  return filt


@util.name_logger
def deskew(img) :
  """
  Deskew image with Hough lines detection.

  Parameters
  ----------
  img : Image in the cv2 format (a numpy array)
    The image to crop and deskew.

  Returns
  -------
  a cvimg

  """
  import cv2
  height, width = img.shape
  # we inverse and make a binary picture
  filt = make_black_and_white(img)
  # Find the lines present in the picture (the layout)
  lines = cv2.HoughLines(
    filt,
    rho=config.img_hl_rho,
    theta=config.img_hl_theta,
    threshold=config.img_hl_thresh,
  )[:, 0, :]
  logger.debug("lines.shape " + repr(lines.shape))
  # rhos = lines[:, 0]
  thetas = lines[:, 1]
  # We separate vertical and horizontal lines
  vert_cond_1 = np.abs(thetas) < config.img_angle_thresh
  vert_cond_2 = np.abs(thetas - np.pi) < config.img_angle_thresh
  horiz_cond = np.abs(thetas - np.pi / 2) < config.img_angle_thresh
  vert_angles_1 = thetas[vert_cond_1]
  vert_angles_2 = thetas[vert_cond_2] - np.pi
  vert_angles = np.hstack([vert_angles_1, vert_angles_2])
  vert_angle = np.mean(vert_angles)
  horiz_angles = thetas[horiz_cond]
  horiz_angle = np.mean(horiz_angles)
  logger.debug("angles -- horizontal, vertical :"
             + repr(horiz_angle) + ", " + repr(vert_angle))

  ## These next to steps can be one step ?
  # Rotate the picture to make the vertical lines actually vertical
  non_zero_pix = cv2.findNonZero(filt)
  center, wh, theta = cv2.minAreaRect(non_zero_pix)
  rot_mat = cv2.getRotationMatrix2D(
    center,
    vert_angle * 180 / np.pi,
    1
  )
  rotated = cv2.warpAffine(
    img,
    rot_mat,
    (width, height),
    flags=cv2.INTER_CUBIC
  )

  # Now affine transformation to make the horizontal lines horizontal
  line_vert_1 = np.array(
    [0., 0.],
    dtype=np.float32
  )
  line_vert_2 = np.array(
    [1000., 0.],
    dtype=np.float32
  )
  line_horiz_1 = np.array(
    [0., horiz_angle - vert_angle],
    dtype=np.float32
  )
  line_horiz_2 = np.array(
    [1000., horiz_angle - vert_angle],
    dtype=np.float32
  )
  pt_ul = util.intersect(line_vert_1, line_horiz_1)
  pt_ur = util.intersect(line_vert_2, line_horiz_1)
  pt_ll = util.intersect(line_vert_1, line_horiz_2)
  new_line_horiz_1 = np.array(
    [0., np.pi / 2],
    dtype=np.float32
  )
  npt_ur = util.intersect(line_vert_2, new_line_horiz_1)
  from_pts = np.vstack([pt_ul, pt_ur, pt_ll])
  to_pts = np.vstack([pt_ul, npt_ur, pt_ll])
  warp_mat = cv2.getAffineTransform(
    from_pts.astype('float32'),
    to_pts.astype('float32')
  )

  warped = cv2.warpAffine(rotated, warp_mat, (width, height))

  return warped


def cast_coords(coord, ev, tv) :
  return ev * coord + tv


# And if we want only one rectangle ?
def cast_rectangles(rectangle_d, ev, tv) :
  tile_ev = np.tile(ev, 2)
  tile_tv = np.tile(tv, 2)
  return {k : cast_coords(rect, tile_ev, tile_tv)
          for k, rect in rectangle_d.items()}


def col_number_to_flock_number(col_number, template_key) :
  col_info = util.read_csv(
    util.path.join(config_img_path, "col_info.csv"),
    index_col='template_key'
  )
  first_fl_nb, nb_cols, next_fl_nb = col_info[template_key]
  if col_number > nb_cols - 1 :
    flock_number = col_number - nb_cols + next_fl_nb
  else :
    flock_number = col_number + first_fl_nb

  return flock_number


def flock_number_to_col_number(flock_number, template_key) :
  col_info = util.read_csv(
    util.path.join(config_img_path, "col_info.csv"),
    index_col='template_key'
  )
  first_fl_nb, nb_cols, next_fl_nb = col_info[template_key]
  if flock_number >= next_fl_nb :
    col_number = flock_number - next_fl_nb + nb_cols
  else :
    col_number = flock_number - first_fl_nb

  return col_number
