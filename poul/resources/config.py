#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Summary
-------

Configuration variables used throughout the project and gathered here.


Constants
---------
default_dest : where output should go by default

img_bw_thresh : threshold for binary thresholding
img_hl_rho : Hough lines stride resolution
img_hl_theta : Hough lines angle resolution
img_hl_thresh : score to attain for a line to be considered
img_angle_thresh : abs max angle to be considered

match_a1 : penalty for first letter difference in bird type
match_a2 : penalty for second letter difference in bird type
match_b : L1 penalty for size difference
match_c : L1 penalty for age difference
match_d : penalty for death
match_e : special penalty for a None / not None matching
match_coeffs : (a1, a2, b, c) tuple

"""
from numpy import pi


default_dest = "../4_output"

flock_nb_d = {
  'front_chicken_before_20151103' : (list(range(1, 6))
                                   + list(range(11, 16))),
  'back_duck_before_20151103' : (list(range(6, 11))
                               + list(range(16, 21))),
  'front_chicken_after_20151103' : (list(range(1, 10))
                                  + list(range(19, 28))),
  'back_duck_after_20151103' : (list(range(10, 19))
                              + list(range(28, 37))),
}

img_bw_thresh = 175
img_hl_rho = 1
img_hl_theta = pi / 1080
img_hl_thresh = 1000
img_angle_thresh = pi * 10 / 180

match_a1 = 100
match_a2 = 10
match_b = 1
match_c = 2
match_d = 200
match_e = 10
match_coeffs = (match_a1, match_a2, match_b, match_c, match_d)
