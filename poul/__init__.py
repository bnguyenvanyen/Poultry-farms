#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
*******
Summary
*******

This package provides script to manipulate and analyze the data
from the smallholder poultry farm data.

It defines and provides way to communicate with a sql database via SQLAlchemy,
provides a graphical interface via PyQt,
plotting capabilities via Matplotlib and Seaborn,
and analysis via Numpy and Pandas.

The tool to add or edit data to databases, and formulate complex requests
against the database is `parse_qt.py` and relies on PyQt.
The tool to extract and analyze the data, and otherwise run automatic routines
is `parse_cli.py`.

A good starting point for the user is to look at the help messages of those two
commands.

The developper will first want to familiarize himself with PyQt and SQLAlchemy,
who have and extensive and clear documentations.

"""
