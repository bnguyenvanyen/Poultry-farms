#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
High level functions to be called from poul.py or from within poul.appl.

"""
import logging
import readline
import glob
import sqlite3
import itertools

import numpy as np
import pandas as pd
from sqlalchemy import event, create_engine
from sqlalchemy.dialects import sqlite as dialect_sqlite
from sqlalchemy.engine import Engine
from sqlalchemy.orm import sessionmaker

from poul import util, flockmatch
from poul.db import declar, dialog

import poul.resources.database as prd
config_db_path = prd.__path__[0]

logger = logging.getLogger(__name__)

sqlite3.register_adapter(np.int64, lambda val : int(val))
sqlite3.register_adapter(np.bool_, lambda val : bool(val))


@event.listens_for(Engine, 'connect')
def set_sqlite_pragma(dbapi_connection, connection_record) :
  """
  Enforce foreign key constraints.

  """
  cursor = dbapi_connection.cursor()
  cursor.execute("PRAGMA foreign_keys=ON")


def open_connection(database, **kw) :
  return dialog.open_connection(database)


def create_database(conn, dates, communes, farms, **kw) :
  """
  Create the tables and fill them in with basic data at conn.

  Dates and farms ranges are inferred from `dates` and `communes`.

  """
  dialog.create_database(conn, dates, communes, farms)


def add_csv(conn, source, **kw) :
  """
  Adds the data in the csv file at `source` to the database at `conn`.

  The csv file should be formatted like 'empty_model.csv'.
  This uses 'INSERT OR REPLACE' which is NOT upsert.
  Columns absent from the csv file will be NULLed on replace.
  Notably the flock_id column.
  The enum tables should already contain all the corresponding rows.

  Parameters
  ----------
  conn : sqlalchemy.engine.Connectable
  source : str
    Path.

  """
  dialog.add_csv(conn, source)


def sync(conn, **kw) :
  """
  'Insert or update' for 'bird_types', 'confinements', 'causes_of_death'
  with the rows from 'poul/resources/database'.

  """
  dialog.sync(conn)


def copy(conn, source, **kw) :
  """
  Attach the database at 'source' to conn and copy the contents.
  The conn database should have no pages yet.

  """
  dialog.copy(conn, source)


def add_forms(conn, pattern, **kw) :
  """
  Add the forms corresponding to `pattern` to `conn`.

  """
  dialog.add_forms(conn, pattern)


def extract(conn, **kw) :
  """
  Extract the relevant data from `conn` as a pandas DataFrame.

  """
  dialog.extract(conn)


@util.name_logger
def save_extract(df, database, dest, **kw) :
  """
  Save the pandas DataFrame `df` to `dest`.

  Default (dest is None) is to automatically gen from `database`.

  """
  if dest is None :
    dest = util.path.join(
      '..',
      '4_output',
      '{}.csv'.format(util.root_name(database))
    )
  df.to_csv(dest)


def match(conn, all_, commune, farm_number, is_recto, **kw) :
  """
  Match flocks corresponding to `commune`, `farm_number` and `is_recto`.

  """
  Session = sessionmaker(bind=conn)
  ses = Session()
  constr = [
    dialog.cast_expr(s, col) for s, col in zip(
      [commune, farm_number, is_recto],
      [declar.Farm.commune, declar.Farm.farm_number, declar.Page.is_recto]
    )
    if s is not None
  ]
  if all_ :
    flockmatch.rematch_all_flocks(ses, constr)
    ses.commit()
  else :
    flockmatch.match_new_flocks(ses, constr)
    ses.commit()


def plot(conn, cmd, **kw) :
  from poul import plot
  Session = sessionmaker(bind=conn)
  ses = Session()
  if cmd == 'simple' :
    f_l = plot.plot_simple_stats(ses)
  elif cmd == 'pop' :
    f_l = [plot.plot_pop_traj(ses)]
  elif cmd == 'farm' :
    f_l = plot.plot_all_farm_traj(ses)
  elif cmd == 'traj' :
    f_l = plot.plot_all_flock_traj(ses)
  elif cmd == 'flock' :
    f_l = plot.plot_flock_stats(ses)
  elif cmd == 'match' :
    f_l = plot.plot_flock_distance(ses)
  elif cmd == 'missing' :
    f_l = [plot.plot_missing(ses)]
  else :
    raise ValueError("Invalid command for plot subcommand")

  return f_l


def save_plot(f_l, database, cmd, dest, **kw) :
  from matplotlib.backends.backend_pdf import PdfPages
  if dest is None :
    dest = util.path.join(
      '..',
      '4_output',
      '{}_{}.pdf'.format(util.root_name(database), cmd)
    )
  with PdfPages(dest) as pdf :
    for f in f_l :
      pdf.savefig(f)
