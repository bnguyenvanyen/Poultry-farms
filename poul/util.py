#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
This module handles all the image processing relative to the forms,
needed for reading in data.

"""
from os import listdir, path
# import time
from shutil import copyfile  # NOQA
from re import match, compile
import logging
from functools import partial  # NOQA
from copy import deepcopy  # NOQA
from collections import OrderedDict
from contextlib import contextmanager  # NOQA
import datetime

import numpy as np
from pandas import DataFrame, read_csv  # NOQA
# from sqlalchemy.util._collections import KeyedTuple
# can this replace OrderedDict totally ?

logger = logging.getLogger(__name__)

def name_logger(f) :
  def deco_f(*l, **kw) :
    mod_name = f.__module__
    i = '' if len(l) == 0 else l[0]
    o = f(*l, **kw)
    s = "{} : ({}...) -> {}".format(
      f.__qualname__,
      i,
      o,
    )
    logging.getLogger(mod_name).debug(s)
    return o

  return deco_f


def method_logger(f) :
  def deco_f(*l, **kw) :
    mod_name = f.__module__
    i = '' if l == [] else l[1:]  # we don't want self
    o = f(*l, **kw)
    s = "{} : (self, {}) -> {}".format(
      f.__qualname__,
      i,
      o,
    )
    logging.getLogger(mod_name).debug(s)
    return o

  return deco_f


class Ordinal_timedelta(datetime.timedelta) :
  """
  We try to cheat matplotlib by defining a bijection to ordinals
  as starting from 0.

  """
  _origin = datetime.date(1, 1, 1)

  def toordinal(self) :
    return (self._origin + self).toordinal()

  def fromordinal(cls, ordinal) :
    return datetime.date.fromordinal(ordinal) - cls._origin


# Flock_scan = namedtuple(
#   'Flock_scan',
#   ['id',
#    'flock_number',
#    'bird_type',
#    'size',
#    'age',
#    'age_of_depop',
#    'all_in_all_out',
#    'in_since_last',
#    'sell_or_slaughter_since_last',
#    'grazing_duration',
#    'grazing_range',
#    'commercial_feed',
#    'confinement',
#    'boot_change',
#    'other_people',
#    'disinfectant',
#    'death_since_last',
#    'cause_of_death',
#    'hpai_and_date',
#    'newcastle_and_date',
#    'duck_plague_and_date',
#    'other_and_date']
# )

# class Base_row(namedtuple('Base_row', _names)) :
#   """
#   Represents our base object structure to manipulate.
#   (Data under and OrderedDict).
#
#   """
#   def __new__(cls, *l, **kw) :
#     for n in cls._fields[len(l):] :
#       if n not in kw.keys() :
#         kw[n] = None
#     super().__new__(cls, *l, **kw)

class Base_row(OrderedDict) :
  """
  Represents our base object structure to manipulate.
  (Data under and OrderedDict).

  """
  _names = ['Bird_type', 'Confinement', 'Cause_of_death',
            'Farm', 'Date', 'Page', 'Flock']

  def __init__(self, tup_l=[]) :
    super().__init__(tup_l)

  @classmethod
  def from_kt(cls, kt) :
    return cls(list(zip(kt.keys(), kt)))

  def keys(self) :
    for name, stg in super().items() :
      try :
        itr = stg.keys()
      except AttributeError :
        yield name
      else :
        for subname in itr :
          yield name, subname

  def values(self) :
    for name, stg in super().items() :
      try :
        itr = stg.values()
      except AttributeError :
        yield stg
      else :
        for val in itr :
          yield val

  def items(self) :
    for name, stg in super().items() :
      try :
        itr = stg.items()
      except AttributeError :
        yield name, stg
      else :
        for subname, val in itr :
          yield name, subname, val

  def sub(self, *name_l) :
    return Base_row([ (name, value)  # or self.class ?
                      for name, value in super().items()
                      if name in name_l ])

  def reorder(self, name_l) :
    try :
      assert set(name_l) == set(super().keys())
    except AssertionError :
      logger.error("Missing keys in reorder : %s",
                   set(super().keys()).difference(set(name_l)))
      raise
    return Base_row([ (name, self[name])
                      for name in name_l ])

def map_od(f, od) :
  return OrderedDict([ (name, f(val)) for name, val in od.items() ])

def map_row(f, br) :
  out = Base_row()
  for name, attr_name, x in br.items() :
    y = f(x)
    try :
      out[name][attr_name] = y
    except KeyError :
      out[name] = OrderedDict([ (attr_name, y) ])

  return out

def filter_row(f, br) :
  out = Base_row()
  for name, attr_name, x in br.items() :
    if f(x) :
      try :
        out[name][attr_name] = x
      except KeyError :
        out[name] = OrderedDict([ (attr_name, x) ])

  return out

def mapi_row(f, br) :
  out = Base_row()
  for name, attr_name, x in br.items() :
    y = f(name, attr_name, x)
    try :
      out[name][attr_name] = y
    except KeyError :
      out[name] = OrderedDict([ (attr_name, y) ])


def filteri_row(f, br) :
  out = Base_row()
  for name, attr_name, x in br.items() :
    if f(name, attr_name, x) :
      try :
        out[name][attr_name] = x
      except KeyError :
        out[name] = OrderedDict([ (attr_name, x) ])

  return out


def empty_fun(*l, **kw) :
  """
  Do nothing.

  """
  pass


def identity(x) :
  """
  Return the input.

  """
  return x


def day_to_week(x) :
  return x / 7.


def month_to_week(x) :
  return x * 4.357


def stg_or_none(f, *l, **kw) :
  """
  Return function output or None if caught error (Name, Attribute, Key).

  """
  try :
    return f(*l, **kw)
  except (NameError, AttributeError, KeyError) :
    return None


# I think we replaced this by convert_bool
def str_to_bool(s) :
  if s == "True" :
    return True
  elif s == "False" :
    return False
  elif s == "None" :
    return None
  else :
    raise ValueError("Invalid str literal for str_to_bool :", s)


# Don't know if we use this
def empty_to_none(x) :
  return None if x == "" else x


def conv_or_none(x, conv=identity) :
  """
  Pass a string through some conversion, unless it corresponds to None.

  """
  return None if x in ["", "None"] else conv(x)

def int_or_none(x) :
  return conv_or_none(x, int)

def float_or_none(x) :
  return conv_or_none(x, float)


def risk_factor(flock_l) :
  """
  Evaluate how susceptible to disease propagation a flock is.

  Parameters
  ----------
  flock_l : dec.Flock list
    A flock from its birth to its death.

  """
  # lower for small-sized short-lived confined vaccinated flocks
  # for now does a very partial job
  # order ( duck > rest, layer/breeding > rest, unconfined > rest )
  # FIXME ignores vaccination !
  fl = flock_l[0]
  sc = 2 * (fl.confinement_code == 'UN') \
     + 4 * (fl.type_code[1] == 'D') \
     + 8 * (fl.type_code[0] == 'V')
  return sc


def volatility(flock_l) :
  """
  Evaluate how "dynamic" a flock is during its lifetime.
  Flocks are considered more dynamic if they are short-lived and/or
  their population size changes a lot.

  Parameters
  ----------
  flock_l : dec.Flock list
    A flock from its birth to its death.

  """
  # lower for long-lived flocks with very constant sizes
  a = len(flock_l)

  def size_var(flock) :
    fisl = flock.in_since_last \
           if flock.in_since_last is not None else 0
    fsossl = flock.sell_or_slaughter_since_last \
             if flock.sell_or_slaughter_since_last is not None else 0
    fdsl = flock.death_since_last \
           if flock.death_since_last is not None else 0
    return fisl - fsossl - fdsl

  b = np.sum(np.abs([ size_var(fl) for fl in flock_l[1:-1] ]))
  # return some weighing of a against b
  return (b + 1) / a


"""
OS functions.

"""
# listdir = os.listdir


# FIXME needs docstring
# def backup_status(dir_path, db_name) :
#   l = []
#   regex = compile("{}.+\.db".format(db_name.split(".")[0]))
#   ctime = time.time()  # seconds since epoch
#   for fname in os.listdir(dir_path) :
#     if match(regex, fname) :
#       path = "{}/{}".format(dir_path, fname)
#       modif_time = os.path.getmtime(path)
#       l.append((fname, ctime - modif_time))
#
#   return l


# FIXME needs docstring
# def canon_backup_name(db_name) :
#   ts = time.strftime("%Y-%m-%d")
#   base = db_name.split(".")[0]
#   return "{}_{}.db".format(base, ts)


"""
Regexpr functions.

"""
def decompose_image_fname(fname) :
  regex = compile(
    r'(?P<indb>(INDB_)|())'
    r'(?P<year>\d{4})-'
    r'(?P<month>\d{2})_'
    r'phieu_tan_(?P<commune>(phu)|(loc))_'
    r'(?P<cor>(cor_)|())'
    r'(?P<nb>\d+)\.png'
  )
  m = match(regex, fname)
  d = dict()
  if m :
    # assert (m.group('indb') == '')
    d['indb'] = (m.group('indb') == 'INDB_')
    d['year'] = int(m.group('year'))
    d['month'] = int(m.group('month'))
    d['commune'] = 'TPH' if m.group('commune') == 'phu' else 'TLO'
    d['cor'] = (m.group('cor') == 'cor_')
    d['number'] = int(m.group('nb'))

  return d


def get_era(year, month) :
  if year > 2015 :
    return 'after_20151103'
  elif month > 10 :
    return 'after_20151103'
  else :
    return 'before_20151103'


def convert_week(s) :
  dec = r"\d*\.?\d*"
  re = compile(
    ("((?P<mth>{0}):(?P<wk>{0}):(?P<day>{0}))|").format(dec)
    #  "((?P<mth>{0}) m(onths)?(?P<wk>)(?P<day>)$)|"
    #  "((?P<mth>)(?P<wk>{0}) w(eeks)?(?P<day>)$)|"
    #  "((?P<mth>)(?P<wk>)(?P<day>{0}) d(ays)?$").format(dec)
  )
  m = match(re, s)
  mth, wk, day = [ float(y) if y != '' else 0.
                   for y in [m.group('mth'), m.group('wk'), m.group('day')] ]

  return month_to_week(mth) + wk + day_to_week(day)


def convert_bool(s) :
  if s in ['T', 'True', 't', 'true', 'C', 'Co', 'Có́', 'c', 'co', 'có'] :
    return True
  elif s in ['F', 'False', 'f', 'false', 'K', 'Không', 'Không', 'k', 'không'] :
    return False
  elif s in ['None'] :
    return None
  else :
    raise ValueError("Invalid input for converting to bool")


"""
Tuples functions.

"""
di_one = (1., 1.)

def di_add(a, b) :
  return (a[0] + b[0], a[1] + b[1])

def di_sub(a, b) :
  return (a[0] - b[0], a[1] - b[1])

def di_mul(a, b) :
  return (a[0] * b[0], a[1] * b[1])

def di_div(a, b) :
  return (a[0] / b[0], a[1] / b[1])

def ceildiv(x, y) :
  return -((- x) // y)


"""
File functions.

"""
def read_line(fname) :
  with open(fname, 'r') as fich :
    data = fich.readline()
  return data

def root_name(pth) :
  return path.splitext(path.basename(pth))[0]


"""
Pandas functions.

"""
def odict_by_index(df) :
  return OrderedDict([ (tup[0], np.array(tup[1:]))
                       for tup in df.itertuples() ])

def create_dataframe(row_l, col_names) :
  df = DataFrame.from_records(row_l,
                                 columns=col_names)
  return df



"""
List functions.

"""
def last_not_none(l) :
  revl = l[::-1]
  revi = iter(revl)
  v = next(revi)
  while v is None :
    v = next(revi)
  return v


# FIXME needs docstring
def chunks(l, cuts) :
  complete_cuts = cuts.copy()
  if len(complete_cuts) > 0 :
    complete_cuts.sort()
    if complete_cuts[0] > 0 :
      complete_cuts.insert(0, 0)
    if (complete_cuts[-1] != -1) and (complete_cuts[-1] < len(l)) :
      complete_cuts.append(len(l))
    for i, j in zip(complete_cuts[:-1], complete_cuts[1:]) :
      yield l[i:j]


def filterin_nlargest(l, f, n) :
  """
  Return the `n` largest elements of `l` as weighed by `f`
  in their original order.
  This is naive dichotomic search, not optimized whatsoever.

  Attributes
  ----------
  l : 'a list
  f : ('a -> int or float or any ordered type)
  n : int
    If `n` is larger than `len(l)` then a copy of `l` is returned.

  """
  # note that this fails horribly if for example, `f` is constant on `l`
  a = f(min(l, key=f))
  b = f(max(l, key=f))  # this doesn't change
  i = 0
  while len(l) > n and i < 100 :
    # after the first step, x = min(l, key=f) here
    i += 1
    x = (a + b) / 2
    print(a, b, x, len(l))
    # l1 = [ y in l if y <= x ]
    l2 = [ z for z in l if f(z) > x ]  # but what do we do if len(l2) < n
    if len(l2) >= n :
      l = l2
      a = x
    else :
      b = x

  while len(l) > n :
    print(len(l))
    i = l.index(min(l, key=f))
    l.pop(i)

  return l


"""
Geometry functions

"""
def intersect(line1, line2) :
  """
  Find the point of intersection between two lines, if it exists.
  This uses the format of opencv.
  The lines are expected to be of the format returned by HoughLines :
  two values, where the first is the distance between the origin (upper-left)
  and the line, and the second is the angle between the normal to the line
  and the horizontal line, in radians.
  So, 0. is a vertical line, and a low angle is a steep line
  (for the usual coordinates).

  Parameters
  ----------
  line1 : array-like
    First line.
  line2 : array-like
    Second line.
  Returns
  -------
  x : array-like
    The intersection point (abs, -ord).

  """
  (rho1, thet1), (rho2, thet2) = line1, line2
  if thet1 == thet2 :
    raise ValueError("The two lines are parallel and don't intersect : ",
                     thet1, thet2)
  # we're looking for x such that a1 * x + b1 = a2 * x + b2
  a1 = np.array([np.cos(thet1), np.sin(thet1)])
  a2 = np.array([np.cos(thet2), np.sin(thet2)])
  sc = a1[0] * a2[1] - a1[1] * a2[0]
  x = np.flipud((a1 * rho2 - a2 * rho1) / np.array([sc, -sc]))
  return x
