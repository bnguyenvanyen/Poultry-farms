#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Summary
-------

Where we manipulate databases.
Interface module to `db.declar`.

"""
import logging
import sqlite3
import itertools
import glob

from sqlalchemy import event, create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.schema import MetaData
from sqlalchemy.sql import select
import sqlalchemy.sql.expression as sql_expr
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound
from pandas import read_csv, read_sql_query

import poul.resources.database as prd
import poul.util as util
import poul.db.declar as dec


logger = logging.getLogger(__name__)

config_db_path = prd.__path__[0]


# possibly move this to declar
ft = dec.Farm.__table__
dt = dec.Date.__table__
pt = dec.Page.__table__
flt = dec.Flock.__table__
dct = dec.Death_count.__table__

page_joined = pt.join(
  ft,
  pt.c.farm_id == ft.c.id
).join(
  dt,
  pt.c.date_id == dt.c.id
)


def attr_key(attr) :
  """
  Unique key for the attribute as (class name, attr key).

  """
  return (attr.class_.__name__, attr.key)


def col_to_label(col) :
  """
  Associate a label as used in `appl` to a column.

  Parameters
  ----------
  col : sqlalchemy Column

  Returns
  -------
  r : {'combo', 'int', 'text', 'float', 'time', 'bool', 'scan'}

  """
  t = col.type
  try :
    col.table
  except AttributeError :  # raised by dec.Flock.bird_species
    logger.error("This col has no table : %s", col)
    raise
  if col.table.name in ['bird_types', 'confinements', 'causes_of_death'] :
    r = 'combo'
  elif isinstance(t, dec.Integer) :
    r = 'int'
  elif isinstance(t, dec.String) :
    r = 'text'
  elif isinstance(t, dec.Float) :
    r = 'float'
  elif isinstance(t, dec.Week) :
    r = 'time'
  elif isinstance(t, dec.Boolean) :
    r = 'bool'
  elif isinstance(t, dec.LargeBinary) :
    r = 'scan'
  else :
    logger.error("Not implemented in col_to_label for %s", col)
    raise NotImplementedError

  return r


def attr_to_label(attr) :
  """
  Parameters
  ----------
  attr : sqlalchemy InstrumentedAttribute

  Returns
  -------
  label : {'combo', 'int', 'text', 'float', 'time', 'bool', 'scan'}

  """
  try :
    col = attr.property.columns[0]
  except AttributeError :
    col = list(attr.property.local_columns)[0]
  return col_to_label(col)


def descr_of_attr(attr) :
  """
  Description of attribute.

  Parameters
  ----------
  attr : sqlalchemy InstrumentedAttribute

  Returns
  -------
  descr_d : ({'en', 'vn'}, str) dict
    the description can be typically used as a title

  """
  descr_d = dict()
  for lang in ['en', 'vn'] :
    try :
      descr = attr.info["descr_" + lang]
    except KeyError :
      descr = attr.key.title().replace("_", " ")
    descr_d[lang] = descr
  return descr_d


def one_col_of_attr(attr) :
  return attr.prop.columns[0]


def nonid(attr) :
  """
  Does the name of the attribute end with 'id' ?

  """
  return (attr.key[-2:] != 'id')


def nonforeign(attr) :
  """
  Does this attribute have foreign keys ?

  """
  return (one_col_of_attr(attr).foreign_keys == set())


def nonbinary(attr) :
  """
  Is this attribute not a LargeBinary ?

  """
  return not isinstance(one_col_of_attr(attr).type,
                        dec.LargeBinary)


def recto(attr) :
  """
  Does this attribute belong to the recto ('front_chicken') ?

  By default an attribute belongs both to the recto and the verso.

  """
  try :
    return attr.info['is_recto']
  except KeyError :
    return True


def verso(attr) :
  """
  Does this attribute belong to the verso ('back_duck') ?

  By default an attribute belongs both to the recto and the verso.

  """
  try :
    return not attr.info['is_recto']
  except KeyError :
    return True


################################### event stuff ###############################
@event.listens_for(Engine, 'connect')
def set_sqlite_pragma(dbapi_connection, connection_record) :
  """
  Enforce foreign key constraints.

  """
  cursor = dbapi_connection.cursor()
  cursor.execute("PRAGMA foreign_keys=ON")



############################# expression stuff ##############################
def cast_expr(expr_str, col) :
  return sql_expr.or_(*[
    cast_element(elt, col) for elt in expr_str.split(',')
  ])


def cast_element(or_elt, col) :
  """
  Build a sqlalchemy expression from a str `or_elt`.

  Parameters
  ----------
  or_elt : str
    Constraint, where `-` gives a range between two values.
  col : sqlalchemy.Column
    The column the constraints apply to.

  Returns
  -------
  expr :
    Sql expression

  """
  assert (or_elt != "")
  assert (or_elt is not None)
  if isinstance(col.type, dec.Integer) :
    and_l = or_elt.split("-")  # could be a range of values
    # and_l should be length 1 (not a range) or 2 (a range)
    if len(and_l) == 1 :
      expr = (col == util.int_or_none(and_l[0]))
    elif len(and_l) == 2 :
      # ranges are treated like python range (high boundary excluded)
      expr = sql_expr.or_(*[
        (col == val) for val in range(int(and_l[0]), int(and_l[1]))
      ])
    else :
      raise ValueError(
        "Invalid int query spec. "
        "Expected an int or a range of int. Instead got :",
        and_l
      )

  elif (isinstance(col.type, dec.Float)
     or isinstance(col.type, dec.Week)) :
    and_l = or_elt.split("-")
    if len(and_l) == 1 :
      expr = (col == util.float_or_none(and_l[0]))
    elif len(and_l) == 2 :
      expr = sql_expr.and_(col > float(and_l[0]), col < float(and_l[1]))
    else :
      raise ValueError(
        "Invalid float query spec. "
        "Expected a float or a range of float. Instead got :",
        and_l
      )

  elif (isinstance(col.type, dec.String)
     or isinstance(col.type, dec.Enum_ref)) :
    expr = (col == or_elt)
  elif isinstance(col.type, dec.Boolean) :
    expr = (col == util.convert_bool(or_elt))
  else :
    raise NotImplementedError("Can't query for this type : ", col)

  return expr



####################### PURE SQL STUFF ############################
def open_connection(database) :
  engine = create_engine(
    "sqlite:///{}".format(database),
    connect_args={'detect_types' : sqlite3.PARSE_DECLTYPES},
    module=sqlite3,  # ensures the adapters are registered ?
  )
  conn = engine.connect()

  return conn


def create_database(conn, dates, communes, farms) :
  dec.Base.metadata.create_all(conn)  # does this work ?
  # can we use the sqlite auto incrementing behaviour ?
  n = itertools.count(1)
  farm_t_l = [ (next(n), 'TB', com, j) for i, com in enumerate(communes)
                              for j in range(1, farms[i] + 1) ]
  for farm_t in farm_t_l :
    conn.execute(
      "INSERT INTO farms(id, district, commune, farm_number) VALUES (?,?,?,?)",
      farm_t
    )
  reg = util.compile("(?P<year>\d{4})-(?P<month>\d{2})")
  m_l = [ util.match(reg, date) for date in dates ]
  yr_l, yr_u = [ int(m.group('year')) for m in m_l ]
  mth_l, mth_u = [ int(m.group('month')) for m in m_l ]
  year_l = range(yr_l, yr_u + 1)
  mth_l_l = [mth_l] + [1] * (yr_u - yr_l)
  mth_u_l = [12] * (yr_u - yr_l) + [mth_u]
  n = itertools.count(1)
  date_t_l = [
    (next(n), mth, yr)
    for yr, mth_l, mth_u in zip(year_l, mth_l_l, mth_u_l)
    for mth in range(mth_l, mth_u + 1)
  ]
  for date_t in date_t_l :
    conn.execute(
      "INSERT INTO dates(id, month, year) VALUES (?,?,?)",
      date_t
    )
  # FIXME we should also add bird_types, confinements, causes_of_death
  for tname, usecols, qumarks in [
    ('bird_types', None, '(?,?,?)'),
    ('confinements', ['code', 'descr'], '(?,?)'),
    ('causes_of_death', ['code', 'descr', 'descr_vn'], '(?,?,?)'),
  ] :
    df = util.read_csv(
      util.path.join(config_db_path, "{}.csv".format(tname)),
      usecols=usecols,
      na_values=[],
      keep_default_na=False,  # why ?
    )
    for i, row in df.iterrows() :
      conn.execute(
        "INSERT INTO {} VALUES {}".format(tname, qumarks),
        tuple(row)
      )
  # autocommits, because out of transaction


def read_csv(dest) :
  return read_csv(
    dest,
    true_values=['True'],
    false_values=['False'],
    dtype={
      'farm_number' : int,
      'month' : int,
      'year' : int,
      'flock_number' : int,
      'size' : float,
      'age' : float,
      'age_of_depop' : float,
      'in_since_last' : float,
      'sell_or_slaughter_since_last' : float,
      'grazing_duration' : float,
      'grazing_range' : float,
      'death_since_last' : float,
    },
  )


def old_to_new_csv(old_csv, new_csv_path) :
  old_csv['is_recto'] = True
  old_csv.loc[old_csv['Bird_type.code'] == 'VT', 'is_recto'] = False
  old_csv.loc[old_csv['Bird_type.code'] == 'VD', 'is_recto'] = False
  old_csv.loc[old_csv['Bird_type.code'] == 'VC', 'is_recto'] = False
  old_csv.to_csv(
    new_csv_path,
    header=[
      'district', 'commune', 'farm_number', 'month', 'year', 'is_recto',
      'flock_number', 'type_code', 'size', 'age', 'age_of_depop',
      'all_in_all_out', 'in_since_last', 'sell_or_slaughter_since_last',
      'grazing_duration', 'grazing_range', 'commercial_feed',
      'confinement_code', 'boot_change', 'other_people', 'which_people',
      'disinfectant', 'death_since_last', 'cause_of_death_code',
      'hpai', 'date_hpai', 'newcastle', 'date_newcastle', 'duck_plague',
      'date_duck_plague', 'other', 'date_other',
    ],
    columns=[
      'Farm.district', 'Farm.commune', 'Farm.farm_number',
      'Date.month', 'Date.year', 'is_recto',
      'Flock.flock_number', 'Bird_type.code',
      'Flock.size', 'Flock.age', 'Flock.age_of_depop', 'Flock.all_in_all_out',
      'Flock.in_since_last', 'Flock.sell_or_slaughter_since_last',
      'Flock.grazing_duration', 'Flock.grazing_range', 'Flock.commercial_feed',
      'Confinement.code', 'Flock.boot_change', 'Flock.other_people',
      'Flock.which_people', 'Flock.disinfectant', 'Flock.death_since_last',
      'Cause_of_death.code',
      'Flock.hpai', 'Flock.date_hpai',
      'Flock.newcastle', 'Flock.date_newcastle',
      'Flock.duck_plague', 'Flock.date_duck_plage',
      'Flock.other', 'Flock.date_other',
    ],
    index=False
  )


def add_csv(conn, source) :
  """
  Adds the data in the csv file at `source` to the database at `conn`.

  The csv file should be formatted like 'empty_model.csv'.
  This uses 'INSERT OR REPLACE' which is NOT upsert.
  Columns absent from the csv file will be NULLed on replace.
  Notably the flock_id column.
  The enum tables should already contain all the corresponding rows.

  Parameters
  ----------
  conn : sqlalchemy.engine.Connectable
  source : str
    Path.

  """
  def to_tuple(row) :
    # flock fields, then page identifier
    t1 = tuple(row['flock_number':].tolist())
    t2 = tuple(row['commune':'is_recto'].tolist())
    return t1 + t2
  df = read_csv(source)
  inp = [ to_tuple(df.loc[i]) for i in range(df.shape[0]) ]
  inp_short = [ tuple(df.loc[i]['commune':'is_recto'].tolist())
                for i in range(df.shape[0]) ]
  with conn.begin() :
    dbapi_conn = conn.connection
    # Insert the flocks from the csv. If they already exist, replace.
    dbapi_conn.executemany(
      "INSERT OR REPLACE INTO flocks SELECT "
      "pages.id,?,NULL,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? "
      "FROM pages "
      "JOIN farms ON pages.farm_id = farms.id "
      "JOIN dates ON pages.date_id = dates.id "
      "WHERE farms.commune = ? AND farms.farm_number = ? "
      "AND dates.month = ? AND dates.year = ? "
      "AND pages.is_recto = ?",
      inp
    )
    # All affected pages are not new anymore if they were
    # Pb : empty pages (many ducks in what we already have)
    dbapi_conn.executemany(
      "UPDATE pages "
      "SET new = '0' WHERE id IN "
      "(SELECT pages.id FROM pages "
      "JOIN farms ON farms.id = pages.farm_id "
      "JOIN dates ON dates.id = pages.date_id "
      "WHERE farms.commune = ? AND farms.farm_number = ? "
      "AND dates.month = ? AND dates.year = ?"
      "AND pages.is_recto = ?)",
      inp_short
    )


def sync(conn, **kw) :
  """
  'Insert or update' for 'bird_types', 'confinements', 'causes_of_death'
  with the rows from 'poul/resources/database'.

  """
  with conn.begin() :
    for tname, usecols, qumarks in [
     ('bird_types', None, '(?,?,?)'),
     ('confinements', ['code', 'descr'], '(?,?)'),
     ('causes_of_death', ['code', 'descr', 'descr_vn'], '(?,?,?)'),
    ] :
      df = util.read_csv(
        util.path.join(config_db_path, "{}.csv".format(tname)),
        usecols=usecols,
        na_values=[],
        keep_default_na=False,  # why ?
      )
      for i, row in df.iterrows() :
        conn.execute(
          "INSERT OR REPLACE INTO {} VALUES {}".format(tname, qumarks),
          tuple(row)
        )



def copy(conn, source, **kw) :
  """
  Attach the database at 'source' to conn and copy the contents.
  The conn database should have no pages yet.

  """
  with conn.begin() :
    conn.execute("ATTACH '{}' AS src".format(source))
    # dbapi_conn.executemany(
    conn.execute(
      "INSERT INTO pages SELECT "
      "p.id,farms.id,dates.id,p.is_recto,p.path,p.template_key,p.new "
      "FROM src.pages AS p "
      "JOIN src.farms AS f ON p.farm_id = f.id "
      "JOIN src.dates AS d ON p.date_id = d.id "
      "JOIN farms ON farms.district = f.district "
                "AND farms.commune = f.commune "
                "AND farms.farm_number = f.farm_number "
      "JOIN dates ON dates.month = d.month AND dates.year = d.year "
    )
    conn.execute(
      "INSERT INTO flocks SELECT "
      "pages.id,fl.flock_number,fl.flock_id,fl.type_code,"
      "fl.size,fl.age,fl.age_of_depop,fl.all_in_all_out,"
      "fl.in_since_last,fl.sell_or_slaughter_since_last,"
      "fl.commercial_feed,fl.confinement_code,fl.grazing_duration,"
      "fl.grazing_range,fl.boot_change,fl.other_people,fl.which_people,"
      "fl.disinfectant,fl.death_since_last,fl.cause_of_death_code,"
      "fl.hpai,fl.date_hpai,fl.newcastle,fl.date_newcastle,"
      "fl.duck_plague,fl.date_duck_plague,fl.other,fl.date_other "
      "FROM src.flocks AS fl "
      "JOIN pages ON fl.page_id = pages.id "
      # "JOIN farms ON pages.farm_id = farms.id "
      # "JOIN dates ON pages.date_id = dates.id "
    )
    conn.execute(
      "INSERT INTO death_counts SELECT "
      "pages.id,fl.flock_number,fl.cause_of_death_code,fl.death_since_last "
      "FROM src.flocks as fl "
      "JOIN pages ON fl.page_id = pages.id "
      "WHERE fl.death_since_last > 0 "
    )


# Note : pd.read_sql_query looks cool
fname_reg = util.compile(
  "(?P<year>201[0-9])-"
  "(?P<month>[0-9]{2})_"
  "(?P<commune>[TPHLO]{3})_"
  "(?P<farm_number>[0-9]+)_"
  "(?P<side>(front)|(back)).png"
  # "(?P<page>[0-9]).png"
)


def to_database(a) :
  from cv2 import imencode
  _, buf = imencode('.png', a)
  return buf


def add_forms(conn, pattern) :
  # hypotheses :
  # all farms and dates are already in the connected database
  # c = conn.cursor()
  try :
    n = itertools.count(conn.execute(
      "SELECT max(id) FROM pages"
    ).fetchone()[0] + 1)
  except TypeError :  # None
    n = itertools.count(1)
  for path in glob.iglob(pattern) :
    folder_path, fname = util.path.split(path)
    m = util.match(fname_reg, fname)
    year = int(m.group('year'))
    month = int(m.group('month'))
    commune = m.group('commune')
    farm_number = int(m.group('farm_number'))
    if year == 2015 and month < 11 :
      era = 'before_20151103'
    else :
      era = 'after_20151103'
    if m.group('side') == 'front' :
      is_recto = True
      sp = 'front_chicken'
    else :
      is_recto = False
      sp = 'back_duck'
    spera = "_".join((sp, era))

    # query farm_id
    farm_id = conn.execute(
      "SELECT id FROM farms "
      "WHERE farms.commune = ? AND farms.farm_number = ?",
      (commune, farm_number)
    ).fetchone()['id']

    # query date_id
    date_id = conn.execute(
      "SELECT id FROM dates "
      "WHERE dates.month = ? AND dates.year = ?",
      (month, year)
    ).fetchone()['id']

    # if the page already exists according to farm_id, date_id, is_recto,ignore
    conn.execute(
      "INSERT OR IGNORE INTO pages"
      "(id, farm_id, date_id, is_recto, path, template_key, new) "
      "VALUES (?,?,?,?,?,?,?)",
      (next(n), farm_id, date_id, is_recto, path, spera, True)
    )
    # autocommits


def extract(conn) :
  return read_sql_query(
    "SELECT f.commune, f.farm_number, d.month, d.year, p.is_recto, "
    "fl.flock_number, fl.flock_id, "
    "fl.type_code, bt.descr_en, (CASE "
    "WHEN (bt.code IN ('GT', 'GD', 'GC')) THEN 'front_chicken' "
    "WHEN (bt.code IN ('VT', 'VD', 'VC')) THEN 'back_duck' "
    "ELSE 'front_other' END) AS classif, "
    "fl.size, fl.age, fl.age_of_depop, fl.all_in_all_out, fl.in_since_last, "
    "fl.sell_or_slaughter_since_last, fl.commercial_feed, "
    "fl.confinement_code, cf.descr, fl.grazing_duration, fl.grazing_range, "
    "fl.boot_change, fl.other_people, fl.which_people, fl.disinfectant, "
    "fl.death_since_last, fl.cause_of_death_code, cd.descr, "
    "group_concat(dc.death_code || ':' || dc.count, ';') AS death_field, "
    "fl.hpai, fl.date_hpai, fl.newcastle, fl.date_newcastle, "
    "fl.duck_plague, fl.date_duck_plague, fl.other, fl.date_other "
    "FROM flocks AS fl "
    "LEFT OUTER JOIN death_counts AS dc ON dc.page_id = fl.page_id "
                                "AND dc.flock_number = fl.flock_number "
    "JOIN pages AS p ON p.id = fl.page_id "
    "JOIN farms AS f ON f.id = p.farm_id "
    "JOIN dates AS d ON d.id = p.date_id "
    "LEFT OUTER JOIN bird_types AS bt ON bt.code = fl.type_code "
    "LEFT OUTER JOIN confinements AS cf ON cf.code = fl.confinement_code "
    "LEFT OUTER JOIN causes_of_death AS cd ON cd.code=dc.death_code "
    "GROUP BY p.id, fl.flock_number "
    "ORDER BY f.commune, f.farm_number, d.year, d.month, p.is_recto",
    conn,
  )


base_select = (
  "SELECT f.commune, f.farm_number, d.month, d.year, p.is_recto, "
  "fl.flock_number, fl.flock_id, "
  "fl.type_code, bt.descr_en, (CASE "
  "WHEN (bt.code IN ('GT', 'GD', 'GC')) THEN 'front_chicken' "
  "WHEN (bt.code IN ('VT', 'VD', 'VC')) THEN 'back_duck' "
  "ELSE 'front_other' END) AS classif, "
  "fl.size, fl.size + fl.sell_or_slaughter_since_last "
  "+ fl.death_since_last - fl.in_since_last AS proj_prev_size, "
  "fl.age, fl.age_of_depop, fl.all_in_all_out, fl.in_since_last, "
  "fl.sell_or_slaughter_since_last, fl.commercial_feed, "
  "fl.confinement_code, cf.descr, fl.grazing_duration, fl.grazing_range, "
  "fl.boot_change, fl.other_people, fl.which_people, fl.disinfectant, "
  "fl.death_since_last, "
  "dc.death_code, cd.descr, dc.count, "
  "fl.hpai, fl.date_hpai, fl.newcastle, fl.date_newcastle, "
  "fl.duck_plague, fl.date_duck_plague, fl.other, fl.date_other "
)


base_join = (
  "JOIN pages AS p ON p.id = fl.page_id "
  "JOIN farms AS f ON f.id = p.farm_id "
  "JOIN dates AS d ON d.id = p.date_id "
  "LEFT OUTER JOIN bird_types AS bt ON bt.code = fl.type_code "
  "LEFT OUTER JOIN confinements AS cf ON cf.code = fl.confinement_code "
  "LEFT OUTER JOIN causes_of_death AS cd ON cd.code=dc.death_code"
)

group_order_by = (
  "GROUP BY p.id, fl.flock_number "
  "ORDER BY f.commune, f.farm_number, d.year, d.month, p.is_recto"
)

def long_extract(conn) :
  return read_sql_query(
    ("{} "
     "FROM death_counts AS dc "
     "LEFT OUTER JOIN flocks AS fl ON dc.page_id = fl.page_id "
                                 "AND dc.flock_number = fl.flock_number "
     "{} "
     "UNION "
     "{} "
     "FROM flocks AS fl "
     "LEFT OUTER JOIN death_counts AS dc ON dc.page_id = fl.page_id "
                                       "AND dc.flock_number = fl.flock_number "
     "{} {}").format(
      base_select,
      base_join,
      base_select,
      base_join,
      group_order_by
    ),
    conn,
  )


def death_totals_extract(conn) :
  return read_sql_query(
    "SELECT "
    "SUM(fl.size "
      "+ fl.sell_or_slaughter_since_last "
      "+ fl.death_since_last "
      "- fl.in_since_last) AS proj_prev_size, "
    "dc.death_code, SUM(dc.count) AS count "
    "FROM death_counts AS dc "
    "JOIN flocks AS fl ON dc.page_id = fl.page_id "
                     "AND dc.flock_number = fl.flock_number "
    "GROUP BY dc.death_code",
    conn,
  )
