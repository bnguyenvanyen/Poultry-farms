#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Summary
-------

This module defines the database model as used by the SQLAlchemy ORM.

"""
import logging
from datetime import date

from sqlalchemy import select, func, type_coerce
from sqlalchemy.sql import literal_column, column
from sqlalchemy.sql.expression import case  # , and_
from sqlalchemy.schema import ForeignKeyConstraint
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import \
  types, Column, Integer, Float, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship, validates

from poul import util

logger = logging.getLogger(__name__)

############ Custom base provides methods for all mappings ###########

class Custom_base() :
  """
  This abstract class adds a few convenience methods
  to access attributes and columns.

  """
  @classmethod
  def get_col_attrs(cls) :
    return ( prop.class_attribute for prop in cls.__mapper__.column_attrs )


Base = declarative_base(cls=Custom_base)

class Week(types.TypeDecorator) :
  """
  The column type for weeks.

  """
  # just used for isinstance
  impl = types.Float

class Enum_ref(types.TypeDecorator) :
  """
  The column type used for foreign keys to reference table primary keys.

  """
  impl = types.String


################################ Enum types ###################################
class Bird_type(Base) :
  __tablename__ = 'bird_types'

  code = Column(String(2), primary_key=True)
  descr_en = Column(String(30))
  descr_vn = Column(String(30))

  # species = Column(String(10))
  @hybrid_property
  def species(self) :
    if self.code in ['GT', 'GD', 'GC'] :
      return 'front_chicken'
    elif self.code in ['VT', 'VD', 'VC'] :
      return 'back_duck'
    else :
      return 'front_other'

  @species.expression
  def species(cls) :
    return case([
      (cls.code == 'GT', 'front_chicken'),
      (cls.code == 'GD', 'front_chicken'),
      (cls.code == 'GC', 'front_chicken'),
      (cls.code == 'VT', 'back_duck'),
      (cls.code == 'VD', 'back_duck'),
      (cls.code == 'VC', 'back_duck'),
    ], else_='front_other')

  @hybrid_property
  def descr(self) :
    return self.descr_en + " - " + self.descr_vn

  def to_text(self) :
    return self.code + (" - " + self.descr_en
                        if self.descr_en is not None
                        else "")

  # @util.name_logger
  @classmethod
  def from_text(cls, text) :
    logger.debug("text is : %s", text)
    l = text.split(" - ")
    code = l[0]
    descr_en = " - ".join(l[1:])
    new_inst = cls(code=code, descr_en=descr_en)
    logger.debug("new inst is : %s", new_inst)
    return new_inst

  def __repr__(self) :
    return "< dec.Bird_type({}) at {} >".format(self.code, hex(id(self)))

  def __str__(self) :
    return "dec.Bird_type({})".format(self.code)


class Confinement(Base) :
  __tablename__ = 'confinements'

  code = Column(String(2), primary_key=True)
  descr = Column(String(30))

  def to_text(self) :
    return self.code + (" - " + self.descr
                        if self.descr is not None
                        else "")

  @classmethod
  def from_text(cls, text) :
    code, descr = text.split(" - ")
    return cls(code=code, descr=descr)


class Cause_of_death(Base) :
  __tablename__ = 'causes_of_death'

  code = Column(String(3), primary_key=True)
  descr = Column(String(30))
  descr_vn = Column(String(30))

  def to_text(self) :
    return (' - '.join([self.code, self.descr, self.descr_vn])
            if self.descr is not None
            else self.code)

  @classmethod
  def from_text(cls, text) :
    code, descr = text.split(" - ")
    return cls(code=code, descr=descr)


############################# The actual data #################################
class Farm(Base) :
  """
  Farm location of the observation.
  District, Commune, and Farm number columns.

  """
  __tablename__ = 'farms'

  id = Column(Integer, unique=True, nullable=False)
  district = Column(String, primary_key=True,
    info={'descr_vn' : "huyện"})
  commune = Column(String, primary_key=True,
    info={'descr_vn' : "xã"})
  farm_number = Column(Integer, primary_key=True,
    info={'descr_vn' : "trang trại"})

  scans = relationship('Page',
                       cascade='',
                       back_populates='farm')
  # flocks = relationship('Flock',
  #                       cascade='',
  #                       back_populates='farm')

  def __repr__(self) :
    return "< dec.Farm({}, {}, {}) at {} with {} >".format(
      self.district,
      self.commune,
      self.farm_number,
      hex(id(self)),
      self.id
    )

  def __str__(self) :
    return "dec.Farm({}-{}-{}-{})".format(
      self.id,
      self.district,
      self.commune,
      self.farm_number
    )


class Date(Base) :
  """
  Date of the observation.
  Day, month and year.

  """
  __tablename__ = 'dates'

  id = Column(Integer, unique=True, nullable=False)
  # day = Column(Integer, primary_key=True,
  #              info={'descr_vn' : "ngày"})
  month = Column(Integer, primary_key=True,
    info={'descr_vn' : "tháng"})
  year = Column(Integer, primary_key=True,
    info={'descr_vn' : "năm"})

  scans = relationship('Page',
                       cascade='',  # careful
                       back_populates='date')
  # flocks = relationship('Flock',
  #                       cascade='',
  #                       back_populates='date')

  def to_datetime(self) :
    return date(self.year, self.month, 26)

  def __repr__(self) :
    return "< dec.Date({}, {}) at {} with {} >".format(
      self.month,
      self.year,
      hex(id(self)),
      self.id
    )

  def __str__(self) :
    return "dec.Date({}-{}-{})".format(
      self.id,
      # self.day,
      self.month,
      self.year
    )


################################## scans ######################################
# TODO might rename Page and pages
class Page(Base) :
  """
  Images associated with a `Farm` and `Date` couple.

  """
  __tablename__ = 'pages'

  id = Column(Integer, unique=True, nullable=False)
  farm_id = Column(Integer, ForeignKey('farms.id'), primary_key=True)
  date_id = Column(Integer, ForeignKey('dates.id'), primary_key=True)
  is_recto = Column(Boolean, primary_key=True,
    info={'descr_en' : "Front of the page"})
  path = Column(String)  # image filename
  # deskewed image, loaded only on access
  template_key = Column(String)  # species + version
  new = Column(Boolean)  # undergone manual edit yet or not

  farm = relationship('Farm',
                      cascade='',
                      uselist=False,
                      back_populates='scans')
  date = relationship('Date',
                      cascade='',
                      uselist=False,
                      back_populates='scans')
  flocks = relationship('Flock',
                        cascade='',
                        back_populates='page')

  def __str__(self) :
    return "< dec.Page({}, {}, {}) >".format(
      str(self.farm),
      str(self.date),
      self.is_recto,
    )

  def __repr__(self) :
    return "< dec.Page({}, {}, {}) at {} with >".format(
      self.farm_id,
      self.date_id,
      self.is_recto,
      hex(id(self)),
      self.id,
    )


########################### flocks ################################

class Flock(Base) :
  """
  Main table, describes one observation, in time and space.
  (a column on a form).

  """
  __tablename__ = 'flocks'

  # id = Column(Integer, unique=True)
  page_id = Column(Integer, ForeignKey('pages.id'), primary_key=True)
  flock_number = Column(Integer, primary_key=True,
    info={
      'descr_vn' : "Đàn số"
    })
  flock_id = Column(Integer)
  type_code = Column(Enum_ref,
                     ForeignKey('bird_types.code'),
                     nullable=False)
  size = Column(Integer,
    info={
      'descr_vn' : "Số lượng hiện tại"
    })
  age = Column(Week,
    info={
      'descr_en' : "Age in weeks",
      'descr_vn' : "Tuần tuổi"
    })
  age_of_depop = Column(Week,
    info={
      'descr_en' : "Intended age of depopulation in weeks",
      'descr_vn' : "Tuần tuổi xuất chuồng dự kiến"
    })
  all_in_all_out = Column(Boolean(name='aiao'))
  in_since_last = Column(Integer,
    info={
      'descr_en' : "Number bought in the last month",
      'descr_vn' : "Số lượng mua thêm trong tháng rồi"
    })
  sell_or_slaughter_since_last = Column(Integer,
    info={
      'descr_en' : "Number sold or slaughtered in the last month",
      'descr_vn' : "Số lượng bán/giết mổ trong tháng rồi"
    })
  commercial_feed = Column(Boolean(name='cf'), nullable=True)
  confinement_code = Column(Enum_ref,
                            ForeignKey('confinements.code'),
                            nullable=False)
  grazing_duration = Column(Float,
    info={
      'descr_en' : "Days spent grazing in the last month",
      'descr_vn' : "Số ngày chăn thả",
      'is_recto' : False
    })  # in days
  grazing_range = Column(Integer,  # in meters
    info={
      'descr_en' : "Grazing range (meters)",
      'descr_vn' : "Cách từ bãi chăn thả đến nơi ở (met)",
      'is_recto' : False
    })  # in meters
  boot_change = Column(Boolean(name='bc'))  # possibly NA
  other_people = Column(Boolean(name='op'),
    info={
      'descr_en' : "Did other people visit ?",
      'descr_vn' : "Có ai khác vào khu vực nuôi không ?",
     })
  which_people = Column(String,
    info={
      'descr_en' : "If yes, which people ?",
      'descr_vn' : "Là ai ?",
    })
  disinfectant = Column(Boolean(name='dis'))
  death_since_last = Column(Integer,
    info={
      'descr_en' : "Number of deaths in the last month",
      'descr_vn' : "Số lượng chết trong tháng rồi"
    })
  cause_of_death_code = Column(Enum_ref,
                               ForeignKey('causes_of_death.code'),
                               nullable=False)
  hpai = Column(Boolean(name='hpai'),
    info={
      'descr_en' : "Vaccinated against HPAI ?",
      'descr_vn' : "Tiêm chủng cúm gia cầm",
    })
  date_hpai = Column(String,
    info={
      'descr_en' : "Date of vaccination against HPAI",
      'descr_vn' : "Ngày tiêm chủng cúm gia cầm",
    })
  newcastle = Column(Boolean(name='newc'),
    info={
      'descr_en' : "Vaccinated against Newcastle ?",
      'descr_vn' : "Có tiêm chủng Newcastle không ?",
      'is_recto' : True
    })
  date_newcastle = Column(String,
    info={
      'descr_en' : "Date of vaccination against Newcastle (yyyy-mm-dd)",
      'descr_vn' : "Ngày tiêm chủng Newcastle",
      'is_recto' : True
    })
  duck_plague = Column(Boolean(name='dp'),
    info={
      'descr_en' : "Vaccinated against the duck plague ?",
      'descr_vn' : "Có tiêm chủng dịch tả vịt không ?",
      'is_recto' : False
    })
  date_duck_plague = Column(String,
    info={
      'descr_en' : "Date of vaccination against the duck plague "
                   "(yyyy-mm-dd)",
      'descr_vn' : "Ngày tiêm chủng dịch tả vịt",
      'is_recto' : False
    })
  other = Column(String,
    info={
      'descr_en' : "Other treatments and vaccinations",
      'descr_vn' : "Bệnh khác",
    })
  date_other = Column(String,
    info={
      'descr_en' : "Date for others (yyyy-mm-dd)",
      'descr_vn' : "Ngày bệnh khác",
    })

  # enums
  bird_type = relationship('Bird_type',
                           cascade='save-update, merge')
  confinement = relationship('Confinement',
                             cascade='save-update, merge')
  cause_of_death = relationship('Cause_of_death',
                                cascade='save-update, merge')

  page = relationship(
    'Page',
    cascade='',
    back_populates='flocks')

  death_counts = relationship('Death_count',
                              cascade="all, delete-orphan",
                              back_populates='flock')

  @hybrid_property
  def death_field(self) :
    s = ';'.join([ dc.flock_text
                   for dc in self.death_counts ])
    logger.debug("Death field : %s", s)
    return s

  @death_field.expression
  def death_field(cls) :
    # FIXME need to do something like
    # SELECT group_concat(dst, ';') AS death_field
    # FROM (SELECT page_id, flock_number, death_code || ':' || count AS dst
    #       FROM death_counts)
    # GROUP BY page_id, flock_number
    ft = Death_count.flock_text.label('flock_text')
    return select([
      func.group_concat(
        column('flock_text'),  # ft,  # Death_count.flock_text,
        literal_column("';'", type_=String)
      )
    ]).select_from(
      select([
        Death_count.page_id,
        Death_count.flock_number,
        ft,  # Death_count.flock_text
      ]).where(
        Death_count.page_id==cls.page_id
      ).where(
        Death_count.flock_number==cls.flock_number
      )
    ).group_by(
      column('page_id')
      # Death_count.page_id
    ).group_by(
      column('flock_number')
      # Death_count.flock_number
    ).as_scalar()

  @death_field.setter
  # @util.method_logger
  def death_field(self, value) :
    self.death_counts = [
      Death_count.from_flock_text(self.page_id, self.flock_number, v)
      for v in value.split(';')
    ]
    logger.debug("Death field set : %s", len(self.death_counts))

  @validates('flock_number', 'size', 'in_since_last',
             'sell_or_slaughter_since_last', 'grazing_range',
             'death_since_last')
  def valid_int(self, key, s) :
    if isinstance(s, str) :
      return util.int_or_none(s)
    else :
      return s

  @validates('all_in_all_out', 'commercial_feed', 'boot_change',
             'other_people', 'disinfectant', 'hpai', 'newcastle',
             'duck_plague')
  def valid_bool(self, key, s) :
    if isinstance(s, str) :
      return util.conv_or_none(s, conv=util.convert_bool)
    else :
      return s

  @validates('grazing_duration')
  def valid_float(self, key, s) :
    if isinstance(s, str) :
      return util.float_or_none(s)
    else :
      return s

  @validates('age', 'age_of_depop')
  def valid_week(self, key, s) :
    if isinstance(s, str) :
      return util.conv_or_none(s, conv=util.convert_week)

  _repr = "< dec.Flock({}, {}) at {} >"

  def __repr__(self) :
    return self._repr.format(
      self.page.id,
      self.flock_number,
      hex(id(self)),
    )

  def __str__(self) :
    return "dec.Flock({}-{}-{})".format(
      str(self.page.farm),
      str(self.page.date),
      self.flock_number
    )


######################### death counts ############################

class Death_count(Base) :
  """
  Contains counts of death by flock by types (both unplanned and planned)

  """
  __tablename__ = 'death_counts'
  page_id = Column(Integer,
                   # ForeignKey('pages.id'),
                   primary_key=True)
  flock_number = Column(Integer,
                        # ForeignKey('flocks.flock_number'),
                        primary_key=True)
  death_code = Column(Enum_ref,
                      ForeignKey('causes_of_death.code'),
                      primary_key=True)
  count = Column(Integer, nullable=True)

  __table_args__ = (
    ForeignKeyConstraint(
      [page_id, flock_number],
      [Flock.page_id, Flock.flock_number],
    ),
  )

  flock = relationship('Flock',
                       cascade='',
                       back_populates='death_counts')

  @hybrid_property
  def flock_text(self) :
    s = self.death_code + ':' + str(self.count)
    logger.debug("Flock text : %s", s)
    return s

  @flock_text.expression
  def flock_text(cls) :
    return type_coerce(cls.death_code, String) \
         + literal_column("':'", type_=String) \
         + type_coerce(cls.count, String)

  @classmethod
  @util.method_logger
  def from_flock_text(cls, page_id, flock_number, value) :
    death_code, count = value.split(':')
    return cls(page_id=page_id,
        flock_number=flock_number,
        death_code=death_code,
        count=count if count is not '' else None)
