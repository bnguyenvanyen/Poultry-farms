#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Summary
-------
Query constraints specification capabilities.
Very much like `up.Update`, but an empty value is not emitted,
instead of emitting `None`.

The format of the constraint strings is to be seen in
`database.dialogue.Querier.update_constraints`.
`'` is a union, `-` a union over a range.

"""

import logging

from PyQt5.QtCore import Qt, QAbstractTableModel
from PyQt5.QtWidgets import QTableView, QSplitter

# from poul import interface, util
import poul.appl.blocks as abl
# import poul.db.declar as dec
import poul.db.dialog as dia

logger = logging.getLogger(__name__)


class With_page_attr(object) :
  _col_l = [ c for c in dia.page_joined.columns
                     if c.key[-2:] != 'id' ]
  _attr_l = [ c.key for c in dia.page_joined.columns
                          if c.key[-2:] != 'id' ]


class With_flock_attr(object) :
  _col_l = [ c for c in dia.flt.columns
                     if c.key[-2:] != 'id' ]
  _attr_l = [ c.key for c in dia.flt.columns
                          if c.key[-2:] != 'id' ]


class With_dc_attr(object) :
  _col_l = [ c for c in dia.dct.columns
                     if c.key[-2:] != 'id' ]
  _attr_l = [ c.key for c in dia.dct.columns
                          if c.key[-2:] != 'id' ]


class Constraint_model(QAbstractTableModel) :
  def __init__(self, parent=None) :
    super().__init__(parent)
    self._constraints = [ "" for c in self._col_l ]

  def rowCount(self, index) :
    return len(self._attr_l)

  def columnCount(self, index) :
    return 1

  # @util.method_logger
  def data(self, index, role=Qt.DisplayRole) :
    if index.isValid() and role==Qt.DisplayRole :
      return str(self._constraints[index.row()])

  # @util.method_logger
  def headerData(self, section, orientation, role=Qt.DisplayRole) :
    if role == Qt.DisplayRole :
      if orientation == Qt.Vertical :
        return self._attr_l[section]
      elif orientation == Qt.Horizontal :
        return ''

  def setData(self, index, value, role=Qt.EditRole) :
    if index.isValid() and role == Qt.EditRole :
      try :
        self._constraints[index.row()] = dia.cast_expr(
          value,
          self._col_l[index.row()]
        )
        return True
      except (AssertionError, ValueError) :
        return False

  def flags(self, index) :
    return super().flags(index) | Qt.ItemIsEditable


class Page_constraint_model(Constraint_model, With_page_attr) :
  pass


class Flock_constraint_model(Constraint_model, With_flock_attr) :
  pass


class Dc_constraint_model(Constraint_model, With_dc_attr) :
  pass


class Query(abl.Action_link) :
  """
  Query on page attributes

  """
  def __init__(self, parent) :
    super().__init__(parent)
    self.split = QSplitter(self)
    self.split.setMinimumSize(self.size())
    self.page_view = QTableView()
    btns = abl.laidout_buttons(
      [self.act_complete, self.passed.emit, self.act_abort]
    )
    self.split.addWidget(self.page_view)
    self.split.addWidget(btns)

  def set_data(self) :
    self.page_model = Page_constraint_model()
    self.page_view.setModel(self.page_model)
    self.page_view.setColumnWidth(0, 300)
    # self.view.verticalHeader().show()
    self.updateGeometry()
    self.split.updateGeometry()

  def process(self) :
    self.page_view.show()
    self.show()

  def close(self) :
    self.page_view.close()
    super().close()

  def act_complete(self) :
    self.completed.emit(None)

  def act_abort(self) :
    self.completed.emit(abl.AbortException())


# FIXME add death_counts attributes
class Full_query(abl.Action_link) :
  """
  Query on page attributes and flock attributes

  """
  def __init__(self, parent) :
    super().__init__(parent)
    self.split = QSplitter(self)
    self.split.setMinimumSize(self.size())

    self.page_view = QTableView()
    self.dc_view = QTableView()
    self.flock_view = QTableView()

    btns = abl.laidout_buttons(
      [self.act_complete, self.passed.emit, self.act_abort]
    )

    self.left_split = QSplitter(orientation=Qt.Vertical)
    self.left_split.addWidget(self.page_view)
    self.left_split.addWidget(self.dc_view)

    self.split.addWidget(self.left_split)
    self.split.addWidget(self.flock_view)
    self.split.addWidget(btns)

  def set_data(self) :
    self.page_model = Page_constraint_model()
    self.flock_model = Flock_constraint_model()
    self.dc_model = Dc_constraint_model()

    self.page_view.setModel(self.page_model)
    self.flock_view.setModel(self.flock_model)
    self.dc_view.setModel(self.dc_model)

    self.page_view.setColumnWidth(0, 300)
    self.flock_view.setColumnWidth(0, 300)
    self.dc_view.setColumnWidth(0, 300)

    self.updateGeometry()
    self.split.updateGeometry()
    self.left_split.updateGeometry()

  def process(self) :
    self.page_view.show()
    self.flock_view.show()
    self.show()

  def close(self) :
    self.page_view.close()
    self.flock_view.close()
    self.dc_view.close()
    super().close()

  def act_complete(self) :
    self.completed.emit(None)

  def act_abort(self) :
    self.completed.emit(abl.AbortException())
