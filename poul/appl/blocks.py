#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Summary
-------
Common ground functions for all of `appl`.

Implements some simple widgets,
subclasses of `QPushButton`, `QLineEdit`, `QComboBox` etc.

Also defines our action chaining paradigm,
to go sequentially through many widgets, with `Action_link` and `Action_chain`.
The can all be completed, passed, or aborted.
Completing means a function will be executed before the next action,
passing means nothing will be done before the next action,
and aborting means the chain will emit aborted too.
So aborting goes out the nesting of chains until "caught".

"""

import logging
import PyQt5.QtCore as QtCore
from PyQt5.QtWidgets import \
  QApplication, \
  QMessageBox, \
  QGridLayout, QStackedLayout, QHBoxLayout, QVBoxLayout, \
  QWidget, \
  QScrollArea, \
  QLabel, QLineEdit, QRadioButton, QComboBox, QPushButton, QButtonGroup, \
  QSizePolicy  # NOQA
from PyQt5.QtGui import QPalette, QPixmap
from matplotlib.backends.backend_qt5agg import FigureCanvas  # NOQA
import traceback as tb
import re

import poul.resources.config as config
import poul.util as util
import poul.db.dialog as dia

logger = logging.getLogger(__name__)


class AbortException(Exception) :
  """
  Exception raised when a "Abort" button is clicked.

  """
  pass


def except_abort(slot_method) :
  def wrapper(self, *l, **kw) :
    try :
      # logger.debug("except_abort")
      return slot_method(self, *l, **kw)
    except Exception as e :  # FIXME we'd like to not leave on Warning
      logger.debug("except_abort with %s", tb.format_exc())
      self.aborted.emit(e)
  return wrapper


def ar_of_qpoint(pt) :
  return util.np.array([pt.x(), pt.y()])


class Log_message_handler(logging.Handler) :
  """
  Log info messages or higher spawn message boxes on emit.

  """
  def __init__(self, **kw) :
    super().__init__(**kw)
    self.setLevel(logging.INFO)
    self.addFilter(lambda rec : 'sqlalchemy' not in rec.name)

  def emit(self, record) :
    message = self.format(record)
    msg_box = QMessageBox()
    msg_box.setIcon(QMessageBox.Information)
    msg_box.setText(message)
    msg_box.setStandardButtons(QMessageBox.Yes)
    if msg_box.exec_() == QMessageBox.Yes :
      pass
    else :
      raise ValueError("QMessageBox returned an unexpected value.")


class Push_button(QPushButton) :
  """
  Our custom uncheckable Push Button, that emits nothing on click.

  """
  oclicked = QtCore.pyqtSignal()

  def __init__(self, *l, **kw) :
    super().__init__(*l, **kw)
    self.clicked.connect(self.oclicked.emit)

  def setCheckable(self) :
    raise NotImplementedError

  def isCheckable(self) :
    raise NotImplementedError

  def isChecked(self) :
    raise NotImplementedError


def laidout_buttons(connections, texts=['Complete', 'Pass', 'Abort']) :
  wid = QWidget()
  lyt = QVBoxLayout()
  lyt.setSpacing(2)
  for connection, text in zip(connections, texts) :
    btn = Push_button(text, None)
    btn.oclicked.connect(connection)
    lyt.addWidget(btn)
  wid.setLayout(lyt)

  return wid


class Push_edit(Push_button) :
  """
  An abstract parent class. Implement convert in children.

  """
  def __init__(self, qlineedit) :
    super().__init__("")
    self.setSizePolicy(25, 25)
    self.oclicked.connect(self.convert)

    self.qtextedit = qlineedit

  def convert(self) :
    raise NotImplementedError("Reimplement in subclasses")


class Push_format_date(Push_edit) :
  """
  A Push button to convert a date to our canonical format yyyy-mm-dd.

  """
  def convert(self) :
    new_text = dia.format_date(self.qlineedit.text())
    self.qlineedit.setText(new_text)


class Push_month_to_week(Push_button) :
  """
  A Push button to convert a number of months to a number of weeks.
  (it just multiplies by 4.357)

  """
  def __init__(self, parent, sibling) :
    super().__init__(parent)
    self.sibling = sibling
    self.setSizePolicy(50, 50)
    self.setFocusPolicy(QtCore.Qt.NoFocus)
    self.oclicked.connect(self.convert)

  def convert(self) :
    wn = util.month_to_week(float(self.sibling.text()))
    approx_wn = int(wn * 10) / 10.
    new_text = str(approx_wn)
    self.sibling.setText(new_text)


class Push_lunar_to_gregorian(Push_button) :
  """
  A Push button to convert a lunar calendar date (format year-month-day)
  to a gregorian calendar date (format year-month-day)

  """
  def convert(self) :
    new_text = dia.lunar_to_gregorian(self.qlineedit.text())
    self.qlineedit.setText(new_text)


class Value_edit(QWidget) :
  def set_internal_from_display(self) :
    # should give a value to self.value
    raise NotImplementedError("Implement in subclasses.")

  def set_display_from_internal(self) :
    raise NotImplementedError("Implement in subclasses.")

  @util.name_logger
  def focusOutEvent(self, e) :
    self.set_internal_from_display()
    self.set_display_from_internal()
    super().focusOutEvent(e)


class Line_edit(QLineEdit, Value_edit) :
  """
  Convenience class built around a line edit,
  to set the value of an attribute with type constraints
  with the text filled in by the user.

  Parameters
  ----------
  text : string
    Initial text displayed in the text field.
  parent : QWidget
    Parent of the instance.
  entity :
    Object whose attribute will be set.
  attr : string
    Attribute name to be set.
  field_type : {"text", "float", "int", "time", "date"}
    Field_type for type casting.

  Raises
  ------
  ValueError
    when field_type is not valid
    when the user passes an invalid string

  """
  internal_value_set = QtCore.pyqtSignal()

  def __init__(self, parent) :
    super().__init__(parent)
    self.pal = QPalette()
    self.pal.setColor(QPalette.Text, QtCore.Qt.red)
    self.value = None
    self.internal_value_set.connect(self.set_display_from_internal)

  def focusOutEvent(self, e) :
    QLineEdit.focusOutEvent(self, e)
    Value_edit.focusOutEvent(self, e)
    # self.editingFinished.emit()

  def unconverter(self, x) :
    return str(x) if x is not None else ""

  def converter(self, x) :
    return util.conv_or_none(x, util.identity)

  @util.name_logger
  def set_internal_from_display(self) :
    conv = self.converter(self.text())
    if conv != self.value :
      self.setPalette(self.pal)
      self.value = conv

  @util.name_logger
  def set_display_from_internal(self) :
    self.setText(self.unconverter(self.value))


class Text_edit(Line_edit) :
  pass


class Int_edit(Line_edit) :
  def converter(self, x) :
    return util.conv_or_none(x, int)


class Float_edit(Line_edit) :
  def converter(self, x) :
    return util.conv_or_none(x, float)


class Time_edit(Line_edit) :
  long_re = re.compile(r'([\.\d]*):([\.\d]*):([\.\d]*)')
  day_re = re.compile(r'([\.\d]+) d(ays)?$')
  week_re = re.compile(r'([\.\d]+)( w(eeks)?)?$')
  month_re = re.compile(r'([\.\d]+) m(onths)?$')

  def converter(self, x) :
    if re.match(self.long_re, x) :
      mth, wk, day = [ float(y) if y != '' else 0.
                       for y in re.match(self.long_re, x).groups() ]
      z = util.month_to_week(mth) + wk + util.day_to_week(day)
    elif re.match(self.day_re, x) :
      day = re.match(self.day_re, x).groups()[0]
      z = util.day_to_week(float(day))
    elif re.match(self.month_re, x) :
      mth = re.match(self.month_re, x).groups()[0]
      z = util.month_to_week(float(mth))
    elif re.match(self.week_re, x) :
      wk = re.match(self.week_re, x).groups()[0]
      z = float(wk)
    else :
      raise ValueError("Invalid string in Time_edit :", x)
    return z


class Query_edit(Line_edit) :
  def converter(self, x) :
    return util.empty_to_none(x)


class Combo_edit(QComboBox, Value_edit) :
  """
  A convenience class for a Combo Box.

  Parameters
  ----------
  text : string
    Initial option set, must be in items.
  parent : QWidget
    Parent widget.
  enum : sqlalchemy row
    Starting enum object.
  enum_l : sqlalchemy row list
    List of options the user can choose from.
  enum_factory :
    Sqlalchemy mapped class to generate new enums.

  """
  internal_value_set = QtCore.pyqtSignal()

  def __init__(self, parent, enum_s_l) :
    super().__init__(parent)
    # self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
    # self.setMinimumSize(200, 100)
    self.text_l = enum_s_l + [""]
    self.addItems(self.text_l)
    self.setEditable(True)
    self.value = None
    self.pal = QPalette()
    self.pal.setColor(QPalette.Text, QtCore.Qt.red)
    self.setCurrentIndex(self.text_l.index(""))
    self.internal_value_set.connect(self.set_display_from_internal)

  def focusOutEvent(self, e) :
    QComboBox.focusOutEvent(self, e)
    Value_edit.focusOutEvent(self, e)

  def reset_items(self) :
    self.clear()
    self.addItems(self.text_l)
    self.setCurrentIndex(self.text_l.index(""))

  @util.name_logger
  def set_internal_from_display(self) :
    val = util.conv_or_none(self.currentText())  # default identity
    if val != self.value :
      self.setPalette(self.pal)
      self.value = val
      logger.debug("combo_set_with : %s", val)

  @util.name_logger
  def set_display_from_internal(self) :
    t = "" if self.value is None else self.value
    self.setCurrentIndex(self.text_l.index(t))


class Out_btn(QRadioButton, Value_edit) :
  def set_internal_from_display(self) :
    self.parentWidget().set_internal_from_display()

  def set_display_from_internal(self) :
    self.parentWidget().set_display_from_internal()


class Radio_yes_no(QWidget) :
  """
  A widget containing a Yes RadioButton and a No RadioButton.

  It is navigable by Tab, and allows for a value input of NA (None).
  The attribute is set
  to True when only the Yes button is checked,
  to False when only the No button is checked,
  to None when no button is checked.

  Parameters
  ----------
  parent : QWidget
    Parent widget.
  boolean : {True, False, None}
    Initial value for the field.

  Attributes
  ----------
  set_with : function
    Convenience function.
  yes_btn : QRadioButton
    "Yes" button.
  no_btn : QRadioButton
    "No" button.

  """
  internal_value_set = QtCore.pyqtSignal()

  def __init__(self, parent) :
    super().__init__(parent)
    self.setFocusPolicy(QtCore.Qt.StrongFocus)
    self.pal = QPalette()
    self.pal.setColor(QPalette.Text, QtCore.Qt.red)

    self.value = None

    self.yes_btn = QRadioButton("Yes")
    self.no_btn = Out_btn("No")
    self.yes_btn.setAutoExclusive(False)
    self.no_btn.setAutoExclusive(False)

    l = QHBoxLayout()
    self.setLayout(l)
    l.addWidget(self.yes_btn)
    l.addWidget(self.no_btn)

    self.setFocusProxy(self.yes_btn)
    self.yes_btn.toggled.connect(self.when_yes_toggled)
    self.no_btn.toggled.connect(self.when_no_toggled)
    self.internal_value_set.connect(self.set_display_from_internal)

  @util.name_logger
  def set_display_from_internal(self) :
    if self.value :
      self.yes_btn.setChecked(True)
      self.no_btn.setChecked(False)
    elif self.value is False :  # not None
      self.yes_btn.setChecked(False)
      self.no_btn.setChecked(True)
    else :
      self.yes_btn.setChecked(False)
      self.no_btn.setChecked(False)

  @util.name_logger
  def set_internal_from_display(self) :
    logger.debug("yes_btn : %s | not_btn : %s",
                 self.yes_btn.isChecked(), self.no_btn.isChecked())
    if self.yes_btn.isChecked() and not self.no_btn.isChecked() :
      logger.debug("true")
      val = True
    elif self.no_btn.isChecked() and not self.yes_btn.isChecked() :
      logger.debug("false")
      val = False
    elif not self.yes_btn.isChecked() and not self.no_btn.isChecked() :
      logger.debug("none")
      val = None
    else :
      raise RuntimeError("Unexpected Radio_yes_no state.")
    if val != self.value :
      logger.debug("field set with " + repr(val))
      self.yes_btn.setPalette(self.pal)
      self.no_btn.setPalette(self.pal)
      self.value = val

  def when_yes_toggled(self, checked) :
    if checked :
      self.no_btn.setChecked(False)

  def when_no_toggled(self, checked) :
    if checked :
      self.yes_btn.setChecked(False)


class Radio_one(QWidget) :
  """
  Widget for choosing one option out of a list of options

  """
  def __init__(self, parent, option_l=[]) :
    super().__init__(parent)
    self.value = None

    layout = QHBoxLayout()
    layout.setSpacing(1)

    option_itr = iter(option_l)

    n_rows = len(option_l)
    n_big_cols = util.ceildiv(n_rows, config.appl_blocks_n_max_rows)
    for big_col in range(n_big_cols) :
      vlyt = QVBoxLayout()
      vlyt.setSpacing(2)
      for row in range(config.appl_blocks_n_max_rows * 2) :
        try :
          option = next(option_itr)
          # logger.debug("Radio_one key : %s", option)
        except StopIteration :
          logger.debug("StopIteration at big_col=%s, row=%s",
                       big_col, row)
          break

        def f(checked, option=option) :
          # logger.debug("f called with %s", checked)
          if checked :
            self.value = option
          else :
            pass
        btn = QRadioButton(option)
        btn.toggled.connect(f)
        vlyt.addWidget(btn)
      layout.addLayout(vlyt)

    self.setLayout(layout)
    self.close()



class Radio_many(QWidget) :
  """
  Widget for choosing a subset of a list of options

  Parameters
  ----------
  parent : QWidget
    Parent widget.
  option_l : str list
    List of option keys.
    The key is used on screen to designate the choice.

  Attributes
  ----------
  values : str set
    Contains the subset of options having a toggled `QRadioButton`.

  """
  def __init__(self, parent, option_l=[]) :
    super().__init__(parent)
    self.values = set()

    layout = QHBoxLayout()
    layout.setSpacing(1)

    option_itr = iter(option_l)

    n_rows = len(option_l)
    n_big_cols = util.ceildiv(n_rows, config.appl_blocks_n_max_rows)
    for big_col in range(n_big_cols) :
      vlyt = QVBoxLayout()
      vlyt.setSpacing(2)
      for row in range(config.appl_blocks_n_max_rows * 2) :
        try :
          option = next(option_itr)
          # logger.debug("Radio_many key : %s", option)
        except StopIteration :
          logger.debug("StopIteration at big_col=%s, row=%s",
                       big_col, row)
          break

        def f(checked, option=option) :
          if checked :
            self.values.add(option)
          else :
            self.values.remove(option)
        btn = QRadioButton(option)
        btn.setAutoExclusive(False)
        btn.toggled.connect(f)
        vlyt.addWidget(btn)
      layout.addLayout(vlyt)

    self.setLayout(layout)
    self.close()


class Pix(QLabel) :
  """
  `QLabel` widget displaying the image `raw_img` as a `QPixmap`.

  Attributes
  ----------
  value_set : pyqtSignal
  value :
    The image to display in a buffer.

  Returns
  -------
  pixlab : QLabel
    The QLabel displaying the image.

  """
  internal_value_set = QtCore.pyqtSignal()

  def __init__(self, parent, shrink=3) :
    super().__init__(parent)
    self.setSizePolicy(QSizePolicy.MinimumExpanding,
                       QSizePolicy.MinimumExpanding)
    self.value = None
    self._pix = QPixmap()
    self._shrink = shrink
    self.internal_value_set.connect(self.set_display_from_internal)

  def set_internal_from_display(self) :
    raise NotImplementedError("This should never get called on a Pix")

  def reshrink(self) :
    w = self._pix.width() / self._shrink
    h = self._pix.height() / self._shrink
    pix_resized = self._pix.scaled(w, h)
    self.setMinimumSize(w, h)
    self.setPixmap(pix_resized)

  def set_display_from_internal(self) :
    self._pix.loadFromData(self.value)
    self.reshrink()

  def set_pix(self, raw_img) :
    self.value = raw_img
    self.set_display_from_internal()


  def get_shrink(self) :
    return self._shrink

  def set_shrink(self, shrink) :
    self._shrink = shrink
    self.reshrink()


class Pix_click(Pix) :
  """
  Display pics in a QLabel and record clicked positions.

  """
  def __init__(self, **kw) :
    super().__init__(**kw)
    self.setFocusPolicy(QtCore.Qt.StrongFocus)
    self.clicked_pos_l = []

  def mousePressEvent(self, event) :
    if event.button() == QtCore.Qt.LeftButton :
      self.clicked_pos_l.append(self._shrink * ar_of_qpoint(event.pos()))

  def keyPressEvent(self, event) :
    if event.key() == QtCore.Qt.Key_Return :
      self.parent().close()
      self.close()


class Skipper(Pix) :
  """
  Display pics in a QLabel, catch 'Return' and 'Escape' to close.

  """
  def __init__(self, **kw) :
    super().__init__(**kw)
    self.setFocusPolicy(QtCore.Qt.StrongFocus)
    self.skip = False

  def keyPressEvent(self, event) :
    if event.key() == QtCore.Qt.Key_Return :
      self.parent().close()
      self.close()
    if event.key() == QtCore.Qt.Key_Escape :
      self.skip = True
      self.parent().close()
      self.close()


class Image_view(QScrollArea) :

  def keyPressEvent(self, event) :
    # widget() should be a Pix
    if event.key() == QtCore.Qt.Key_Minus :
      sh = self.widget().get_shrink()
      self.widget().set_shrink(sh * 1.1)
    if event.key() == QtCore.Qt.Key_Plus :
      sh = self.widget().get_shrink()
      self.widget().set_shrink(sh * 0.9)
    super().keyPressEvent(event)


class Editor_block(QWidget) :
  """
  Defines the interface to widgets that can exchange data with our database
  (via a dict), and edit this data from user input.
  Blocks can be built upon each other to create more complex input gathering
  systems.

  Attributes
  ----------
  set_in :
    This function modifies a dict from the state of the widget.
  get_from :
    This function sets the state of the widget from a dict.
  set_called : QtCore.pyqtSignal
    Emitted when `set` is called.
  get_called : QtCore.pyqtSignal
    Emitted when `get` is called.

  """
  set_called = QtCore.pyqtSignal()
  get_called = QtCore.pyqtSignal()

  def __init__(self, set_in, get_from) :
    self.set_in = set_in
    self.get_from = get_from
    super().__init__()


def new_page(stacked_layout) :
  holder = QWidget()
  holder.setSizePolicy(QSizePolicy.MinimumExpanding,
                       QSizePolicy.MinimumExpanding)
  gl = QGridLayout()
  holder.setLayout(gl)
  stacked_layout.addWidget(holder)

  return gl


def yield_page(stacked_layout, n_max_rows) :
  while True :
    page = new_page(stacked_layout)
    change = True
    for n in range(n_max_rows) :
      yield page, n, change
    change = False


class Action_link(QWidget) :
  """
  This QWidget subclass is the basic block for our program flow :
  We chain widgets that can have three exit status.
  `completed` means the widget completed successfully, some ulterior
  action could be executed, then the flow should continue.
  `passed` means the widget was passed.
  Nothing should be done but the flow should continue.
  `aborted` means the flow should be stopped.
  Should be emitted when exceptions are raised.

  Attributes
  ----------
  working : boolean
    True while in `process`, otherwise False.
  completed : QtCore.pyqtSignal
    Signal emitted when the action has been completed normally.
  passed : QtCore.pyqtSignal
    Signal emitted when the action has been passed.
  aborted : QtCore.pyqtSignal
    Signal emitted when the flow should be stopped.

  """
  completed = QtCore.pyqtSignal(object)
  passed = QtCore.pyqtSignal()
  aborted = QtCore.pyqtSignal(Exception)

  def __init__(self, parent=None) :
    if parent is None :
      super().__init__()
    else :
      super().__init__(parent)
    self.setSizePolicy(QSizePolicy.MinimumExpanding,
                       QSizePolicy.MinimumExpanding)
    if parent is not None :
      self.setMinimumSize(parent.size())
    self.working = False
    self.completed.connect(lambda x : logger.debug("completed %s", self))

  def process(self) :
    """
    Execute the action for the widget.

    """
    self.completed.emit(None)  # reimplement

  def act_abort(self, e=AbortException()) :
    self.aborted.emit(e)



class Paged_link(Action_link) :
  """
  Action_link with several pages.
  A 'Next' button shows the next page until the last page is reached,
  then on next click 'completed' is emitted.
  A 'Previous' button show the previous page until the first page is reached,
  then on next click 'passed' is emitted.
  A 'Abort' button emits 'aborted'.

  """
  def __init__(self, parent, complete_descr) :
    super().__init__(parent)

    self.btns = laidout_buttons(
      [self.on_next_complete, self.on_prev_pass, self.act_abort],
      ["Next / {}".format(complete_descr), "Prev / Pass", "Abort"],
    )
    self.hide()

  def pager_layout(self) :
    raise NotImplementedError("Implement in subclasses")

  def set_layout(self) :
    layout = QHBoxLayout()

    stacked_layout = self.pager_layout()

    layout.addLayout(stacked_layout)
    layout.addWidget(self.btns)

    self.setLayout(layout)
    self.updateGeometry()

  def get_current_page_layout(self) :
    return self.layout().itemAt(0).currentWidget().layout()

  def on_prev_pass(self) :
    n = self.layout().itemAt(0).currentIndex()
    if n > 0 :
      self.layout().itemAt(0).setCurrentIndex(n - 1)
      self.set_page_focus()
      # logger.debug("went from index %s to index %s",
      #              n, n - 1)
    else :
      logger.debug("Passed")
      self.passed.emit()

  def on_next_complete(self) :
    n = self.layout().itemAt(0).currentIndex()
    if n < self.layout().itemAt(0).count() - 1 :
      self.layout().itemAt(0).setCurrentIndex(n + 1)
      self.set_page_focus()
      # logger.debug("went from index %s to index %s",
      #              n, n + 1)
    else :
      logger.debug("Completed")
      self.completed.emit(self.complete_result())

  def complete_result(self) :
    return None

  def process(self) :
    """
    Receive user input until one of the closing signals is emitted.

    """
    # try :
    self.set_layout()
    self.set_page_focus()

    self.show()



class Action_chain(Action_link) :
  """
  This materializes the basic flow of our program.
  Action_link widgets are associated to actions (slots) and chained
  through this widget.
  Since Action_chain is again a subclass of Action_link,
  we can also chain Action_chains

  Parameters
  ----------
  link_spec_l : (Action_link * dict * function) list
    The links to chain in order,
    each associated to keyword-arguments to pass to process,
    and to a function to call on `completed`.
  end : function
    Function to call when the end of the chain is reached.

  Attributes
  ----------
  working : see Action_link
  appended : QtCore.pyqtSignal
    Signal emitted when append was called.
  completed : see Action_link
  passed : see Action_link
  aborted : see Action_link

  """
  appended = QtCore.pyqtSignal()

  def __init__(self,
               link_spec_l=[None],
               end=util.empty_fun) :
    super().__init__()
    self.working = False
    link1_it = iter(link_spec_l[:-1])
    link2_it = iter(link_spec_l[1:])
    for link1, link2 in zip(link1_it, link2_it) :
      self.chain_link(link1, link2)

    def act_end() :
      try :
        logger.debug("act_end")
        end()
        self.working = False
      except Exception as e :
        logger.debug("act_end_exception : %s", e)
        self.working = False
        self.aborted.emit(e)
    self._end = act_end
    self.in_connect(link_spec_l[0])
    self.out_connect(link_spec_l[-1])
    self.set_bypass_gen([])

  def in_connect(self, link_spec) :
    """
    Sets up the ingoing connection.

    Parameters
    ----------
    link_spec : (Action_link * dict * function)
      The link to connect with the usual specifications.

    """
    # note : link_spec could be None
    self._start_link = link_spec

  def out_connect(self, link_spec) :
    """
    Sets up the outgoing connection.
    The last link will trigger end and Chain level signal emission.

    Parameters
    ----------
    link_spec : (Action_link * dict * function)
      The link to connect with the usual specifications.

    """
    self._end_link = link_spec
    try :
      wid, act = link_spec
    except TypeError :  # NoneType. Arises when the chain is empty
      pass  # don't connect to
    else :
      def act_pass() :
        logger.debug("last_link_pass")
        self._end()
        wid.close()
        self.in_disconnect()
        self.out_disconnect()
        self.completed.emit(None)

      def act_complete(*emitted) :
        logger.debug("last_link_complete")
        act(*emitted)
        self._end()
        wid.close()
        self.in_disconnect()
        self.out_disconnect()
        self.completed.emit(None)
      wid.completed.connect(act_complete)
      wid.passed.connect(act_pass)
      wid.aborted.connect(self.act_abort)

  def in_disconnect(self) :
    """
    Disconnect the first link.

    """
    # this only means making it inaccessible by _start_link
    start_link = self._start_link
    self._start_link = None

    return start_link

  def out_disconnect(self) :
    """
    Disconnect the last link.

    """
    end_link = self._end_link
    try :
      wid, act = end_link
    except TypeError :  # NoneType
      pass  # already disconnected
    else :
      wid.completed.disconnect()
      wid.passed.disconnect()
      wid.aborted.disconnect()
      self._end_link = None

    return end_link

  def chain_link(self, link_spec, next_link_spec) :
    """
    Connect `link` to `next_link` (and to its action).

    Attribute
    ----------
    link_spec : (Action_link * dict * function)
    next_link_spec : (Action_link * dict * function)

    """
    wid, act = link_spec
    next_wid, _ = next_link_spec

    def act_pass() :
      logger.debug("link_pass")
      self.in_disconnect()
      self.in_connect(next_link_spec)
      wid.close()
      next_wid.process()

    def act_complete(*emitted) :
      logger.debug("link_complete")
      act(*emitted)
      self.in_disconnect()
      self.in_connect(next_link_spec)
      wid.close()
      bypass = next(self.bypass_gen)  # thank you late binding
      if bypass is not None :
        logger.debug("bypassing wid %s", next_wid)
        next_wid.completed.emit(bypass)
      else :
        next_wid.process()

    wid.completed.connect(act_complete)
    wid.passed.connect(act_pass)
    wid.aborted.connect(self.act_abort)

  @util.name_logger
  def append(self, link_spec) :
    """
    Builds the new last link of the chain.
    Normally, this can be called safely even during process.

    Parameters
    ----------
    link_spec : (Action_link * dict * function)
      The link to connect with the usual specifications.

    """
    end_link_spec = self.out_disconnect()
    # logger.debug("append link ( %s ) to ( %s )", link_spec, end_link_spec)
    if end_link_spec is not None :
      self.chain_link(end_link_spec, link_spec)
    else :
      self.in_connect(link_spec)
    self.out_connect(link_spec)
    self.appended.emit()

  def working_link(self) :
    if self.working :
      return self._start_link
    else :
      return None

  def set_bypass_gen(self, bypass_l) :
    def g() :
      it_bypass = iter(bypass_l)
      while True :
        try :
          yield next(it_bypass)
        except StopIteration :
          yield None
    self.bypass_gen = g()

  @util.name_logger
  def process(self) :
    """
    Executes the chain of actions.

    """
    if not self.working :
      self.working = True
      try :
        wid, a = self.working_link()
      except TypeError :  # NoneType
        self.working = False
        logger.debug("empty chain can't process, doing nothing.")
        self._end()
      else :
        bypass = next(self.bypass_gen)
        if bypass is not None :
          logger.debug("bypassing wid %s", wid)
          wid.completed.emit(bypass)
        else :
          wid.process()
    else :
      logger.warning("Called process but %s already working", self)

  def act_abort(self) :
    self.close()
    self.aborted.emit(AbortException())

  def close(self) :
    link = self.working_link()
    try :
      w, _ = link
    except TypeError :  # NoneType
      pass
    else :
      logger.debug("Closing chain and working link %s", w)
      w.close()
    super().close()
