#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Summary
-------
This module defines `appl.blocks.Action_link` subclasses to update flocks
from the database.

It depends on `db.declar` and `db.dialog` only to define
convenience subclasses.
So this module doesn't need to know aout the precise model from `dec`.

"""

import logging

from PyQt5.QtCore import Qt, QAbstractListModel, QAbstractTableModel
from PyQt5.QtWidgets import QCompleter, QSplitter, \
  QListView, QTableView, QStyledItemDelegate

from poul.resources import config
import poul.util as util
import poul.db.declar as dec
import poul.db.dialog as dia
import poul.appl.blocks as abl

logger = logging.getLogger(__name__)


def label_to_block(s) :  # to block ?
  # what about date
  if s == 'text' :
    cls = abl.Text_edit
  elif s == 'int' :
    cls = abl.Int_edit
  elif s == 'float' :
    cls = abl.Float_edit
  elif s == 'bool' :
    cls = abl.Radio_yes_no
  elif s == 'combo' :
    cls = abl.Combo_edit
  elif s == 'scan' :
    cls = abl.Pix
  elif s == 'time' :
    cls = abl.Time_edit
    # cls = abl.Float_edit
  else :
    raise ValueError("parameter s must be one of int, text, bool or combo")
  return cls


class With_flock_attr(object) :
  """
  Infix, where a lot of the magic happens.
  Sets the private attributes _attr_l and _type_l,
  then used in the other classes.

  """
  def __init__(self, **kw) :
    super().__init__(**kw)
    # Also need some ref to death_counts ? Or how do we add it ?
    # need to ref death_field
    # how do we get to it ?
    # we will just replace it in enum for now
    _enum_rel_d = {
      'type_code' : 'bird_type',
      'confinement_code' : 'confinement',
      # 'cause_of_death_code' : 'cause_of_death',
      'cause_of_death_code' : 'death_field'
    }
    # FIXME on display it would be nice to also see the id cols ?
    self._attr_l = [ _enum_rel_d[c.key] if c.key in _enum_rel_d.keys()
                    else c.key
                    for c in dec.Flock.__table__.columns
                    if c.key[-2:] != 'id' ]
    # problem for 'cause_of_death_code' / 'death_field'
    # we don't really want it to be 'Enum_ref'
    self._type_l = [ c.type
                     for c in dec.Flock.__table__.columns
                     if c.key[-2:] != 'id' ]


class Enum_model(QAbstractListModel) :
  """
  Model for enums.

  """
  def __init__(self, enum_cls, data, parent=None) :
    self._enum_cls = enum_cls
    self._enum_l = data
    super().__init__(parent)

  def rowCount(self, index) :
    return len(self._enum_l)

  def data(self, index, role=Qt.DisplayRole) :
    if index.isValid() and role==Qt.DisplayRole :
      return self._enum_l[index.row()].to_text()

  @util.method_logger
  def setData(self, index, value, role=Qt.EditRole) :
    if index.isValid() and role == Qt.EditRole :
      self._enum_l.append(self._enum_cls.from_text(value))
      self.dataChanged.emit()
      # FIXME Doesn't get reflected in the view for some reason.
      return True

  def flags(self, index) :
    fl = super().flags(index)
    # if index.row() >= self.rowCount(index) :
    return fl | Qt.ItemIsEditable
    # else :
    #   return fl


class Enum_completer(QCompleter) :
  """
  Completer for enums.

  """
  def __init__(self, model, view) :
    super().__init__()
    self.setCompletionMode(QCompleter.UnfilteredPopupCompletion)
    self.setModel(model)
    # self.setPopup(view)


class Flock_delegate(QStyledItemDelegate, With_flock_attr) :
  """
  Delegate for flocks.

  """
  def createEditor(self, parent, styleOption, index) :
    t = self._type_l[index.row()]
    editor = super().createEditor(parent, styleOption, index)
    k = self._attr_l[index.row()]
    # ugly fix for edge case 'death_field'
    if isinstance(t, dec.Enum_ref) and k != 'death_field' :
      model = self._enum_model_d[k]
      completer = Enum_completer(model, self._view)
      editor.setCompleter(completer)
    # and should already be connected correctly
    return editor

  def set_context(self, enum_model_d, view) :
    self._enum_model_d = enum_model_d
    self._view = view

  def setEditorData(self, editor, index) :
    super().setEditorData(editor, index)

  def setModelData(self, editor, model, index) :
    super().setModelData(editor, model, index)

  def updateEditorGeometry(self, editor, styleOption, index) :
    super().updateEditorGeometry(editor, styleOption, index)


class Page_info_model(QAbstractListModel) :
  """
  Model that contains info on pages.

  """
  def __init__(self, data=list(), parent=None) :
    super().__init__(parent)
    page = data
    self._row_l = [
      page.path,
      page.farm.commune, page.farm.farm_number,
      page.date.month, page.date.year,
      page.template_key, page.new
    ]

  def rowCount(self, index) :
    return len(self._row_l)

  def data(self, index, role=Qt.DisplayRole) :
    if index.isValid() and role==Qt.DisplayRole :
      return str(self._row_l[index.row()])


class Base_flock_model(QAbstractTableModel, With_flock_attr) :
  """
  Base model for flocks.

  """
  def __init__(self, data=list(), parent=None) :
    super().__init__(parent)
    self._flocks = data

  def rowCount(self, index) :
    return len(self._attr_l)

  def columnCount(self, index) :
    return len(self._flock_nbs)

  def data(self, index, role=Qt.DisplayRole) :
    if index.isValid() and role==Qt.DisplayRole :
      try :
        fl = self._flocks[index.column()]
      except IndexError :
        return None
      else :
        t = self._type_l[index.row()]
        k = self._attr_l[index.row()]
        # ugly fix for edge case 'death_field'
        if isinstance(t, dec.Enum_ref) and k != 'death_field' :
          try :
            s = getattr(fl, k).to_text()
          except AttributeError :  # None
            s = ''
        else :
          # s = str(getattr(fl, k))
          s = getattr(fl, k)
          if k == 'death_field' :
            logger.debug("Base_flock_model.data got %s", s)
        return s

  def headerData(self, section, orientation, role=Qt.DisplayRole) :
    if role == Qt.DisplayRole :
      if orientation == Qt.Vertical :
        return self._attr_l[section]
      elif orientation == Qt.Horizontal :
        return self._flock_nbs[section]

  def setData(self, index, value, role=Qt.EditRole) :
    raise NotImplementedError("Implement in subclasses")

  def flags(self, index) :
    return super().flags(index) | Qt.ItemIsEditable


# As long as we query on Page (flocks grouped by page)
# this is useless
class Flock_model(Base_flock_model) :
  """
  Model for editing any collection of flocks.

  """
  def __init__(self, data=list(), parent=None) :
    super().__init__(data, parent)
    self._flock_nbs = [ fl.flock_number for fl in data ]

  def setData(self, index, value, role=Qt.EditRole) :
    if index.isValid() and role == Qt.EditRole :
      fl = self._flocks[index.column()]
      setattr(fl, self._attr_l[index.row()], value)
      return True


class Page_model(Base_flock_model) :
  """
  Model for editing a page.

  """
  def __init__(self, page, data=list(), parent=None) :
    # Empty init doesn't work
    super().__init__(data, parent)
    self._page = page
    self._flock_nbs = config.flock_nb_d[page.template_key]

  def set_context(self, enum_model_d) :
    self._enum_model_d = enum_model_d

  @util.method_logger
  def setData(self, index, value, role=Qt.EditRole) :
    if index.isValid() and role == Qt.EditRole :
      # logger.debug("col vs flock : %s | %s",
      #              index.column(), len(self._flocks))
      try :
        fl = self._flocks[index.column()]
      except IndexError :
        if index.column() > len(self._flocks) :
          # not setting the next flock, can't be done
          pass
        else :
          fl = dec.Flock(
            page=self._page,
            page_id=self._page.id,  # Forced to flush
            flock_number=self._flock_nbs[index.column()]
          )
          # self._flocks.append(fl)  # FIXME works without it. why ?
      try :
        # If we handle the enum tables
        # need to setattr foreign with .from_text()
        t = self._type_l[index.row()]
        k = self._attr_l[index.row()]
        # ugly fix for edge case 'death_field'
        if isinstance(t, dec.Enum_ref) and k != 'death_field' :
          enum_model = self._enum_model_d[k]
          enums = enum_model._enum_l
          e_match = [ e for e in enums if e.code == value ]
          if len(e_match) == 1 :  # existing code
            e = e_match[0]
          elif len(e_match) == 0 :  # from_text
            e = enum_model._enum_cls.from_text(value)
          else :  # redundant codes
            raise ValueError("Several codes match which should be impossible")
          value = e
        setattr(fl, k, value)
        return True
      except (ValueError, TypeError) as e :  # value isn't validated
        logger.exception("On setData")
        return False


class Update(abl.Action_link) :
  """
  Link for an update (brings it all together).

  """
  def __init__(self, parent, session, lang='en', image_win=None) :
    super().__init__(parent)
    self._session = session
    self.image_win = image_win
    self.pix = abl.Pix(self.image_win)
    self.image_win.setWidget(self.pix)

    self.view = QTableView()
    self.page_view = QListView()
    compl_view = QListView()
    self.cod_view = QListView()

    self.split = QSplitter(self)
    self.split.setMinimumSize(self.size())
    self._enum_model_d = {
      k : Enum_model(e_cls, session.query(e_cls).all())
      for k, e_cls in
      [ ('bird_type', dec.Bird_type),
        ('confinement', dec.Confinement),
        ('cause_of_death', dec.Cause_of_death) ]
    }
    self.cod_view.setModel(self._enum_model_d['cause_of_death'])
    dlg = Flock_delegate(self)
    dlg.set_context(self._enum_model_d, compl_view)
    self.view.setItemDelegate(dlg)

    btns = abl.laidout_buttons(
      [self.act_complete, self.act_pass, self.act_abort]
    )
    spl = QSplitter()
    spl.setOrientation(Qt.Vertical)
    spl.addWidget(self.page_view)
    spl.addWidget(self.cod_view)
    spl.addWidget(btns)

    self.split.addWidget(self.view)
    self.split.addWidget(spl)

  def set_pages(self, page_l) :
    self.iter_pages = iter(page_l)

  def set_image(self, path_img) :
    # can we tie scrollbars ? would not work
    self.pix._pix.load(path_img)
    self.pix.reshrink()
    # self.image_win.setWidget(pix)
    self.image_win.resize(self.size())

  def set_data(self, page) :
    page_info = Page_info_model(page)
    self.model = Page_model(page, page.flocks)
    self.model.set_context(self._enum_model_d)
    self.page_view.setModel(page_info)
    self.view.setModel(self.model)
    self.updateGeometry()
    self.split.updateGeometry()
    self.split.setStretchFactor(0, 1)
    self.split.setStretchFactor(1, 0)

  def process(self) :
    self.act_complete()
    self.image_win.show()
    self.image_win.widget().show()
    self.view.show()
    self.show()

  def close(self) :
    try :
      self.image_win.widget().close()
    except AttributeError :  # no page
      pass
    self.view.close()
    super().close()

  def act_complete(self) :
    try :
      self.model._page.new = False  # If it was new, it's not anymore.
      for fl in self.model._flocks :
        if fl.flock_number is None :
          logger.info("Dropping flock with empty flock number.")
        else :
          self._session.add(fl)
    except AttributeError :  # first pass
      pass

    try :
      self._session.flush()
      self._session.commit()
    except (dia.SQLAlchemyError, dia.DBAPIError) :
      self._session.rollback()
      self._session.rollback()  # need twice ?
      logger.error("Error on committing forms. Fix then re-commit.")
    else :
      logger.info("Committed page successfully.")  # FIXME first pass
      try :
        page = next(self.iter_pages)
      except StopIteration :
        self.completed.emit(None)
      else :
        self.set_image(page.path)
        self.set_data(page)

  def act_pass(self) :
    try :
      page = next(self.iter_pages)
    except StopIteration :
      self._session.flush()
      self.passed.emit()
    else :
      self.set_image(page.path)
      self.set_data(page)

  def act_abort(self) :
    self.completed.emit(abl.AbortException())


class Display(abl.Action_link) :
  """
  Boiled down link that only displays.

  """
  def __init__(self, parent, lang='en') :
    super().__init__(parent)
    self.view = QTableView()
    self.split = QSplitter(self)
    self.split.setMinimumSize(self.size())
    btns = abl.laidout_buttons(
      [self.act_complete, self.act_pass, self.act_abort]
    )
    self.split.addWidget(self.view)
    self.split.addWidget(btns)

  def set_flocks(self, flock_ll) :
    self.iter_flocks = iter(flock_ll)

  def set_data(self, flock_l) :
    self.model = Flock_model(flock_l)
    self.view.setModel(self.model)
    self.updateGeometry()
    self.split.updateGeometry()
    self.split.setStretchFactor(0, 1)
    self.split.setStretchFactor(1, 0)

  def process(self) :
    self.act_complete()
    self.view.show()
    self.show()

  def close(self) :
    self.view.close()
    super().close()

  def act_complete(self) :
    try :
      flock_l = next(self.iter_flocks)
    except StopIteration :
      self.completed.emit(None)
    else :
      self.set_data(flock_l)

  def act_pass(self) :
    try :
      flock_l = next(self.iter_flocks)
    except StopIteration :
      self.passed.emit()
    else :
      self.set_data(flock_l)
