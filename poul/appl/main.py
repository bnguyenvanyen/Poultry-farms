#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Summary
-------
Interface to the application, through a `Main_window`
and many actions acessible from menus or from the command line.

Actions all inherit from `blocks.Action_link`
and are chained in a `Main_action_chain`
that inherits from `blocks.Action_chain`.
Many of those actions are actually chains themselves.

"""

from PyQt5.QtWidgets import QMainWindow, QStatusBar, QMenuBar, \
  QFileDialog, QInputDialog, QMessageBox
import PyQt5.QtCore as QtCore
import logging

from poul import util, interface
from poul.db import declar, dialog
from poul.appl import blocks, update, query

logger = logging.getLogger(__name__)
app = blocks.QApplication([])
app.setQuitOnLastWindowClosed(True)


class Get_option_link(blocks.Action_link) :
  """
  `blocks.Action_link` subclass to choose one option from a `util.OrderedDict`.

  """
  # completed = QtCore.pyqtSignal(object)

  def __init__(self, parent=None, option_od=util.OrderedDict()) :
    super().__init__(parent)
    self.option_od = option_od
    s_l = [ ".".join([k, subk])
            for k, subk in option_od.keys() ]
    self.radio_one = blocks.Radio_one(parent, s_l)
    layout = self.radio_one.layout()
    cbtn = blocks.Push_button("Complete")
    cbtn.oclicked.connect(self.on_complete)
    abtn = blocks.Push_button("Abort")
    abtn.oclicked.connect(self.act_abort)
    vlyt = blocks.QVBoxLayout()
    vlyt.addWidget(cbtn)
    vlyt.addWidget(abtn)
    layout.addLayout(vlyt)

  def on_complete(self) :
    k, subk = self.radio_one.value.split(".")
    self.completed.emit(self.option_od[k][subk])

  def process(self) :
    self.radio_one.layout().itemAt(0).itemAt(0).widget().setFocus()
    self.radio_one.show()

  def close(self) :
    self.radio_one.close()
    super().close()


class Get_options_link(blocks.Action_link) :
  """
  `blocks.Action_link` subclass to choose a subset of options
  from a `util.OrderedDict`.

  """
  # completed = QtCore.pyqtSignal(util.OrderedDict)

  def __init__(self, parent=None, option_br=util.Base_row()) :
    super().__init__(parent)
    self.option_br = option_br
    s_l = [ ".".join([k, subk])
            for k, subk in option_br.keys() ]
    self.radio_many = blocks.Radio_many(parent, s_l)
    layout = self.radio_many.layout()
    cbtn = blocks.Push_button("Complete")
    cbtn.oclicked.connect(self.on_complete)
    abtn = blocks.Push_button("Abort")
    abtn.oclicked.connect(self.act_abort)
    vlyt = blocks.QVBoxLayout()
    vlyt.addWidget(cbtn)
    vlyt.addWidget(abtn)
    layout.addLayout(vlyt)

  def on_complete(self) :
    k_d = dict()
    for s in self.radio_many.values :
      k, subk = s.split(".")
      try :
        k_d[k].append(subk)
      except KeyError :
        k_d[k] = [subk]
    del k

    def f(k, subk, x) :
      if k in k_d.keys() :
        if subk in k_d[k] :
          return True
        else :
          return False
      else :
        return False

    res = util.filteri_row(f, self.option_br)
    self.completed.emit(res)

  def process(self) :
    self.radio_many.layout().itemAt(0).itemAt(0).widget().setFocus()
    self.radio_many.show()

  def close(self) :
    self.radio_many.close()
    super().close()


def choose_db(parent, db_path) :
  if db_path is None :
    db_path, _ = QFileDialog.getSaveFileName(
      None,
      "Database file",
      "db/")
    if db_path == '' :
      ok1 = False
    else :
      ok1 = True
    # cancel should set to False
  return db_path, ok1


class Set_language(blocks.Action_link) :
  def __init__(self, main_window, lang, **kw) :
    self._mw = main_window
    self.lang = lang
    self.status = ''
    super().__init__()

  def process(self, lang=None) :
    langs = ['en', 'vn']
    curind = langs.index(self.lang)
    if lang is None :
      lang, _ = QInputDialog.getItem(
        None,
        "Choose language",
        'lang',
        ['en', 'vn'],
        curind,
        False
      )
    self._mw.set_language(lang)
    self.completed.emit(None)


class Sessioned_chain(blocks.Action_chain) :
  """
  A `blocks.Action_chain` subclass that maintains a unique active session.

  """
  def __init__(self, session_factory, link_l=[None], end=util.empty_fun, **kw) :
    self.set_session_factory(session_factory)
    self.end = end
    super().__init__(link_l, end=self.commit)

  def set_session_factory(self, session_factory) :
    """
    Sets the methods `get_session` and `close_session`.

    """
    self._session = None
    self.session_factory = session_factory

  def get_session(self) :
    if self._session is None :
      self._session = self.session_factory()
    return self._session

  def close_session(self) :
    if self._session is not None :
      self._session.close()
      self._session = None

  def close(self) :
    self.close_session()
    super().close()

  def commit(self) :
    self.end()
    self.get_session().commit()
    self.close_session()
    self.completed.emit(None)

  def act_abort(self) :
    self.inform("Aborting action : {}".format(self.__class__.__name__))
    super().act_abort()

  def inform(self, msg) :
    msg_box = QMessageBox()
    msg_box.setIcon(QMessageBox.Information)
    msg_box.setText(msg)
    msg_box.setStandardButtons(QMessageBox.Ok)
    res = msg_box.exec_()
    if res == QMessageBox.Ok :
      pass
    else :
      raise ValueError("QMessageBox returned an unexpected value.")


class With_select(Sessioned_chain) :
  """
  Append a link to select a subset of fields via `Get_options_link`.

  """
  def __init__(self, link_parent, attr_row, **kw) :
    super().__init__(link_parent=link_parent, **kw)
    attr_chooser = Get_options_link(
      link_parent,
      attr_row
    )
    self.append([attr_chooser, self.attr_action])

  @blocks.except_abort
  def attr_action(self, attr_row) :
    raise NotImplementedError("Reimplement in subclasses")


class With_breather(Sessioned_chain) :
  """
  Append an empty link to the chain.
  Useful with Edit but I don't really remember why.
  Useful if we add links during the life of the chain :
  The link that appends should be followed by at least one other,
  or the appended links will be bypassed.

  """
  def __init__(self, **kw) :
    super().__init__(**kw)
    self.append([blocks.Action_link(), util.empty_fun])


class With_where(Sessioned_chain) :
  """
  Append a link to define query constraints via a `appl.query.Query`.

  """
  @util.name_logger
  def __init__(self, link_parent, **kw) :
    self.query_setter = query.Query(link_parent)
    self.query_setter.set_data()
    super().__init__(link_parent=link_parent, **kw)

    self.append([self.query_setter, self.query_set_action])

  def query_set_action(self, x) :
    raise NotImplementedError("Implement in subclasses")


class With_flock_where(Sessioned_chain) :
  """
  Append a link to define query constraints via a `appl.query.Query`.

  """
  @util.name_logger
  def __init__(self, link_parent, **kw) :
    self.query_setter = query.Full_query(link_parent)
    self.query_setter.set_data()
    super().__init__(link_parent=link_parent, **kw)

    self.append([self.query_setter, self.query_set_action])

  def query_set_action(self, x) :
    raise NotImplementedError("Implement in subclasses")


class Edit_page(Sessioned_chain) :
  @util.name_logger
  def __init__(self, main_window, link_parent, session_factory, lang, **kw) :
    super().__init__(session_factory=session_factory, **kw)
    self._mw = main_window
    self.link_parent = link_parent
    self.lang = lang
    self.image_win = blocks.Image_view()
    self.updater = update.Update(
      parent=self.link_parent,
      session=self.get_session(),
      lang=self.lang,
      image_win=self.image_win,
    )

  def append_updates(self, page_l) :
    if len(page_l) == 0 :
      logger.info("No pages returned. Completed.")
      self.close()
    else :
      self.updater.set_pages(page_l)
      self.append((self.updater, util.empty_fun))

  def edit_complete(self, upr, page) :
    self.get_session().flush()

  def inform_conflict(self, row) :
    self.inform("The row {} won't be added to the database.".format(str(row)))

  def close(self) :
    self.image_win.close()
    super().close()


class Edit_new_pages(Edit_page) :
  def __init__(self, **kw) :
    super().__init__(**kw)
    page_l = self.get_session().query(declar.Page)\
                               .join(declar.Farm)\
                               .join(declar.Date)\
                               .filter(declar.Page.new)\
                               .order_by(declar.Farm.commune,
                                         declar.Farm.farm_number,
                                         declar.Date.year,
                                         declar.Date.month)\
                               .all()
    self.append_updates(page_l)

  def edit_complete(self, upr, page) :
    page.new = False
    self.get_session().flush()


class Edit_subset_pages(With_breather, With_where, Edit_page) :
  def query_set_action(self, constr) :
    constr = [ c for c in self.query_setter.page_model._constraints
                       if not isinstance(c, (type(None), str)) ]
    page_l = self.get_session().query(declar.Page)\
                               .join(declar.Farm)\
                               .join(declar.Date)\
                               .filter(*constr)\
                               .order_by(declar.Farm.commune,
                                         declar.Farm.farm_number,
                                         declar.Date.year,
                                         declar.Date.month)\
                               .all()
    self.append_updates(page_l)


class Edit_subset_flocks(With_breather, With_flock_where, Edit_page) :
  def query_set_action(self, constr) :
    page_constr = [ c for c in self.query_setter.page_model._constraints
                       if not isinstance(c, (type(None), str)) ]
    flock_constr = [ c for c in self.query_setter.flock_model._constraints
                       if not isinstance(c, (type(None), str)) ]
    dc_constr = [ c for c in self.query_setter.dc_model._constraints
                  if not isinstance(c, (type(None), str)) ]
    constr = page_constr + flock_constr + dc_constr
    page_l = self.get_session().query(declar.Page)\
                               .join(declar.Farm)\
                               .join(declar.Date)\
                               .join(declar.Flock)\
                               .join(declar.Death_count)\
                               .filter(*constr)\
                               .order_by(declar.Farm.commune,
                                         declar.Farm.farm_number,
                                         declar.Date.year,
                                         declar.Date.month)\
                               .all()
    self.append_updates(page_l)


class Display_subset_flocks(With_breather, With_flock_where, Sessioned_chain) :
  def __init__(self, link_parent, session_factory, lang, **kw) :
    super().__init__(
      session_factory=session_factory,
      link_parent=link_parent,
      **kw
    )
    self.link_parent = link_parent
    self.lang = lang
    self.displayer = update.Display(
      parent=self.link_parent,
      lang=self.lang,
    )

  def query_set_action(self, constr) :
    page_constr = [ c for c in self.query_setter.page_model._constraints
                       if not isinstance(c, (type(None), str)) ]
    flock_constr = [ c for c in self.query_setter.flock_model._constraints
                       if not isinstance(c, (type(None), str)) ]
    constr = page_constr + flock_constr
    flock_l = self.get_session()\
                  .query(declar.Flock)\
                  .join(declar.Page)\
                  .join(declar.Farm)\
                  .join(declar.Date)\
                  .filter(*constr)\
                  .order_by(declar.Farm.commune,
                            declar.Farm.farm_number,
                            declar.Date.year,
                            declar.Date.month)\
                  .all()
    if len(flock_l) == 0 :
      logger.info("No flocks returned. Completed.")
      self.close()
    else :
      self.displayer.set_flocks([flock_l])
      self.append((self.displayer, util.empty_fun))


class Display_flocks_by_id(Sessioned_chain) :
  def __init__(self, link_parent, session_factory, lang, **kw) :
    super().__init__(session_factory=session_factory, **kw)
    self.link_parent = link_parent
    self.lang = lang
    self.displayer = update.Display(
      parent=self.link_parent,
      lang=self.lang,
    )
    flock_l = self.get_session()\
                  .query(declar.Flock)\
                  .join(declar.Page)\
                  .join(declar.Farm)\
                  .join(declar.Date)\
                  .order_by(declar.Farm.commune,
                            declar.Farm.farm_number,
                            declar.Flock.flock_id,
                            declar.Date.year,
                            declar.Date.month)\
                  .all()
    flock_ll = interface.flockmatch.group_by_id(flock_l)
    self.displayer.set_flocks(flock_ll)
    self.append((self.displayer, util.empty_fun))


class Plot(blocks.Paged_link) :
  def __init__(self, link_parent, action, connection, **kw) :
    super().__init__(parent=link_parent, complete_descr='End')
    self.figures = interface.plot(connection, cmd=action)

  def pager_layout(self) :
    layout = blocks.QStackedLayout()
    for fig in self.figures :
      canvas = blocks.FigureCanvas(fig)
      canvas.draw()
      layout.addWidget(canvas)
    return layout

  def set_page_focus(self) :
    self.layout().itemAt(0).currentWidget().setFocus()


class Main_action_chain(blocks.Action_chain) :
  """
  The main_window control structure :
  As request are handed down to the chain (button clicks),
  they get appended to this chain.
  If a link aborts, the link is closed but the chain keeps processing.
  When the chain reaches its end, it emits the `hit_end` signal
  but doesn't close.

  Attributes
  ----------
  hit_end : pyqtSignal
    Emitted when the chain reaches its end.

  """
  hit_end = QtCore.pyqtSignal()

  def __init__(self) :
    def act_end() :
      self.working = False
      self.hit_end.emit()
      logger.debug("reached the end of the main action chain.")
    super().__init__(end=act_end)

  def act_abort(self, e=blocks.AbortException()) :
    wid, act = self.working_link()
    logger.info("Action %s has aborted. Processing next action.", wid)
    wid.passed.emit()

  def close(self) :
    super().close()


class Main_window(QMainWindow) :
  """
  This is the main window widget for interacting with poultry farm databases.

  It provides tools for adding data to the database,
  editing data already present in the database,
  and extracting data (to .csv) format via SQL queries.

  Parameters
  ----------
  engine_string : string, optional
    Engine specification for SQLAlchemy, by default "sqlite:///:memory:"

  Attributes
  ----------
  closed : pyqtSignal
    Emitted when the `Main_window` is closed.
  _actions : util.OrderedDict
    Specifies and organizes the actions accessible from the menu.
    The string list associated to each action is

  """
  closed = QtCore.pyqtSignal()

  _actions = util.OrderedDict([
    ('settings', util.OrderedDict([
      ('language', Set_language),
    ])),
    ('edit', util.OrderedDict([
      ('new pages', Edit_new_pages),
      ('pages from query', Edit_subset_pages),
      ('flocks from query', Edit_subset_flocks),
    ])),
    ('display', util.OrderedDict([
      ('flocks from query', Display_subset_flocks),
      ('flocks by id', Display_flocks_by_id),
    ])),
    ('plot', util.OrderedDict([
      ('simple', Plot),
      ('pop', Plot),
      ('farm', Plot),
      ('flock', Plot),
      ('missing', Plot),
    ])),
  ])

  def __init__(self, conn, lang, **kw) :
    super().__init__()

    self.resize_to_screen()
    self.set_central_widget()
    self.set_status_bar()
    self.set_menu_bar()

    self.connection = conn
    self.session_factory = dialog.sessionmaker(bind=conn)
    self.set_language(lang)

    self.working = False
    self._pending = Main_action_chain()
    self._pending.appended.connect(self.on_append)

    logger.info("Main window initialized")
    logger.debug("Main window size : %s", self.size())

  def resize_to_screen(self) :
    """
    Resizes the window to the screen.

    """
    desk_w = app.desktop()
    # assume a single screen
    screen_width, screen_height = desk_w.width(), desk_w.height()
    self.resize(int(screen_width), int(0.95 * screen_height))

  def set_central_widget(self) :
    cw = blocks.QWidget()
    cw.setSizePolicy(blocks.QSizePolicy.MinimumExpanding,
                     blocks.QSizePolicy.MinimumExpanding)
    self.setCentralWidget(cw)

  def set_status_bar(self) :
    statusbar = QStatusBar()
    self.database_status = blocks.QLabel()
    self.lang_status = blocks.QLabel()
    self.action_status = blocks.QLabel()

    statusbar.addPermanentWidget(self.database_status, stretch=1.)
    statusbar.addPermanentWidget(self.lang_status, stretch=0.5)
    statusbar.addWidget(self.action_status, stretch=1.)
    self.setStatusBar(statusbar)

  def set_menu_bar(self) :
    """
    Create the menubar for the main window from `_actions`.

    """
    menubar = QMenuBar()
    # create the actions
    for mk, action_d in self._actions.items() :
      menu = menubar.addMenu(mk.capitalize())
      for ak in action_d.keys() :
        menu.addAction(ak.replace("_", " ").capitalize(),
                       self.create_action(menu_key=mk, action_key=ak))

    qtbtn = blocks.Push_button("X")
    qtbtn.setFocusPolicy(QtCore.Qt.ClickFocus)
    qtbtn.oclicked.connect(self.close)  # FIXME message
    menubar.setCornerWidget(qtbtn)

    self.setMenuBar(menubar)

  def create_action(self, menu_key, action_key) :
    """
    Create an action link spec (used in `blocks.Action_Chain`)
    with the initialization arguments in `_actions` grabbed from `self`
    and appends it to the main action chain `_pending`.

    Parameters
    ----------
    menu_key : str
      Key that must be found at the first level of `_actions`.
    action_key : str
      Key that must be found at the second level of `_actions`.

    """
    # should we return to_pending directly ?
    # action_cls, param_l = self._actions[menu_key][action_key]
    action_cls = self._actions[menu_key][action_key]

    def f() :
      logger.debug("pending action %s-%s", menu_key, action_key)
      kw = {
        'lang' : self.lang,
        'connection' : self.connection,
        'session_factory' : self.session_factory,
        'action' : action_key,
      }

      # logger.debug("kw resolved to : %s", kw)
      action = action_cls(
        main_window=self,
        link_parent=self.centralWidget(),
        **kw
      )
      self.action_status.setText(
        "{} - {}".format(
          menu_key,
          action_key,
        )
      )
      self.to_pending((action, util.empty_fun))

    return f

  def close_database(self) :
    """
    Closes the current database. Cleans up the engine (unlisten events).

    """
    pass
    # dialog.mute_session_events(self.session_factory)

  def set_language(self, lang) :
    self.lang = lang
    self.lang_status.setText(lang)

  ### main action chain
  def to_pending(self, link) :
    """
    Appends `link` to the main action chain `_pending`.

    Parameters
    ----------
    link : (blocks.Action_link, dict, function)
      Action link specification as used in `blocks.Action_chain` and children.

    """
    # assume self._pending exists, and is an Action_chain
    self._pending.append(link)

  def process(self) :
    """
    Shows the window and starts the main action chain.

    """
    self.show()
    self._pending.process()

  def on_append(self) :
    """
    Process the actions pending in the main action chain `_pending`,
    if `self` is visible.

    """
    if self.isVisible() :
      self._pending.process()

  ### messages

  def inform(self, message) :
    msg_box = QMessageBox()
    msg_box.setIcon(QMessageBox.Information)
    msg_box.setText(message)
    msg_box.setStandardButtons(QMessageBox.Yes)
    if msg_box.exec_() == QMessageBox.Yes :
      pass
    else :
      raise ValueError("QMessageBox returned an unexpected value.")

  def save_quit(self) :
    msg_box = QMessageBox()
    msg_box.setIcon(QMessageBox.Warning)
    msg_box.setText("Safe mode, changes to the database will be discarded.")
    msg_box.setInformativeText("Are you sure you want to quit ?")
    msg_box.setStandardButtons(QMessageBox.Yes
                             | QMessageBox.Save
                             | QMessageBox.Cancel)
    res = msg_box.exec_()
    if res == QMessageBox.Yes :
      return True
    elif res == QMessageBox.Save :
      db_path, ok = choose_db(None, None)
      if ok :
        self.inform("Saving data in memory to {}".format(db_path))
        dialog.persist_changes(self.engine, db_path)
      return True
    elif res == QMessageBox.Cancel :
      return False
    else :
      raise ValueError("QMessageBox Returned an unexpected value")

  def close(self) :
    """
    Closes the window (after some cleaning up).

    """
    # message for safe and making it possible to persist the changes still
    actually_close = True
    # if self.safe :
    #   actually_close = self.save_quit()
    if actually_close :
      self.close_database()
      self._pending.close()
      self.closed.emit()
      super().close()
