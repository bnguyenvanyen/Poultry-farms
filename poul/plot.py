#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
This module matches flocks.

"""

import logging

import numpy as np
from pandas import DataFrame, melt, pivot_table, to_numeric
import matplotlib.pyplot as plt
import matplotlib.cm as mcm
from matplotlib.dates import MonthLocator, DateFormatter
import seaborn as sb
from sqlalchemy.sql.expression import func

from poul import util, flockmatch
import poul.db.declar as dec
import poul.db.dialog as dia

logger = logging.getLogger(__name__)


# FIXME this should not be here
# def date_to_time(date) :
#  return (365 * (date.year - 2015.5) + 30.5 * date.month + date.day) / 30.5
def date_to_time(date) :
  return (365 * (date.year - 2015.4) + 30.5 * date.month) / 30.5


@util.name_logger
def plot_sub_flock_matching_fit(ax_l, flock_l) :
  """
  Evaluates a flock matching.
  If the two time series have very far, we've got to wonder.

  Parameters
  ----------
  ax_l : plt.axes list
    The axes that will receive the data (for the different dimensions).
  flock_l : dec.Flock list
    The flocks, by date.

  """
  # use a nice blue and a nice red
  # put the names in config ?
  blue = sb.xkcd_rgb["denim blue"]
  red = sb.xkcd_rgb["pale red"]
  times = np.array([ date_to_time(fl.page.date) for fl in flock_l ])
  # fitted values
  fit_a = np.array([ flockmatch.flock_to_simple(fl)
                     for fl in flock_l ])
  # projected values for our flock matching
  proj_a = np.array([ flockmatch.reverse_one_month_to_simple(fl)
                   for fl in flock_l[1:] ])
  print("proj_a", proj_a)
  try :
    p_types, p_sizes, p_ages, p_tolives \
      = proj_a[:, 0], proj_a[:, 1], proj_a[:, 2], proj_a[:, 3]
  except IndexError :  # flock_l is of length 1
    p_types, p_sizes, p_ages, p_tolives \
      = np.array([]), np.array([]), np.array([]), np.array([])
  f_types, f_sizes, f_ages, f_tolives \
      = fit_a[:, 0], fit_a[:, 1], fit_a[:, 2], fit_a[:, 3]
  print("p_sizes", p_sizes)
  for ax, vals, p_vals in zip(ax_l,
                              [f_types, f_sizes, f_ages, f_tolives],
                              [np.hstack([p_types, f_types[-1]]),
                               np.hstack([p_sizes, f_sizes[-1]]),
                               np.hstack([p_ages, f_ages[-1]]),
                               np.hstack([p_tolives, f_tolives[-1]])]) :
    print("times", times.shape)
    print("vals", vals.shape)
    print("p_vals", p_vals.shape)
    ax.plot(times, vals, color=blue)
    ax.plot(times, p_vals, color=red)
    ax.set_xticks([])
    ax.set_yticks([])
  return ax_l


@util.name_logger
def plot_flock_matching_fit(flock_l_d) :
  # each element of flock_l_d is assumed to be a matched list of flocks
  # with the flock_id as key
  print(flock_l_d.keys())
  n = len(list(flock_l_d))
  f, ax_a = plt.subplots(nrows=n, ncols=4, sharex=True, sharey="col")
  for i, flock_l in flock_l_d.items() :
    plot_sub_flock_matching_fit(ax_a[i - 1], flock_l)
  # for ax, tit in zip(ax_a[0, :], ["Type", "Size", "Age", "Time to live"]) :
  #   ax.set_title(tit)
  # ax_a[-1, 0].set_xlabel("t")

  return f


@util.name_logger
def plot_flocks_traj(dates, flock_ll, farm=None) :
  """
  Plots the trajectory of a farm.

  Parameters
  ----------
  dates : dec.Date list
    The dates for which we observe the farm (in order).
  flock_ll : dec.Flock list list
    The flocks present in the farm, each ordered by date.
  farm : dec.Farm
    If a farm's info should be used to write a title. Default None.

  Returns
  -------
  f : plt.Figure
    Figure output.

  """
  # sort by (GD, GC, GT, and VD, VC, VT)
  # create the palettes (no idea how)
  # for now let's just make a random palette with the right number of colors
  n = len(dates)
  m = len(flock_ll)
  f, ax = plt.subplots()
  red = sb.xkcd_rgb["pale red"]
  blue = sb.xkcd_rgb["denim blue"]
  palette = sb.cubehelix_palette(15, start=0.5, rot=-0.75, dark=0.1)
  # let's also assume the bird_type doesn't change during the life of the flock
  # flock_l_l.sort(key=(lambda fl_l : util.volatility(fl_l)))
  # we pad the flocks fo
  size_a = np.zeros([m, n])
  death_a = np.zeros([n])
  month = util.Ordinal_timedelta(days=30.5)
  date_a = (np.array([ date.to_datetime() for date in dates ])
         - month)
  # should we subtract one month from date_a ?
  for j, flock_l in enumerate(flock_ll) :
    for flock in flock_l :
      i = dates.index(flock.page.date)
      size_a[j, i] = flock.size
      dsl = flock.death_since_last if flock.death_since_last is not None else 0
      death_a[i] += dsl  # over-evaluated because of "other"
  # risk_factor_l = [ util.risk_factor(fl_l) for fl_l in flock_l_l ]
  # the size data in greens and blues
  flk = None
  bottom = 0
  for size_line in size_a :
    rf = 0  # FIXME
    logger.debug("risk factor is : %s", rf)
    color = palette[rf]
    # ax.plot(time_a, col, color=color)
    # ax.fill_between(time_a, last_col, col, color=color, alpha=.2)
    # one rectangle per month * flock
    flk = ax.barh(
      [bottom] * n, [month] * n, size_line, date_a,
      linewidth=0,
      color=color,
      align='edge',
      # tick_label=size,
    )
    bottom += size_line.max() + 5
  # then plot overlayed excess death intensity in red
  dth, = ax.plot(
    date_a,
    death_a,
    color=red,
    label="Number of deaths"
  )
  sz, = ax.plot(
    date_a,
    size_a.sum(axis=0),
    color=blue,
    label="Population size"
  )
  ax.xaxis_date()
  ax.xaxis.set_major_locator(MonthLocator(interval=4))
  ax.xaxis.set_major_formatter(DateFormatter("%Y-%m"))
  ax.set_xlabel("t")
  ax.set_ylabel("Number of birds")
  if flk is not None :
    ax.legend(
      (dth, sz, flk),
      ("Number of deaths", "Population size", "Flock size"),
      loc='upper right'
    )
  else :
    ax.legend()

  if farm is None :
    ax.set_title("Flock trajectories and death burden for the population")
  else :
    ax.set_title("Flock trajectories and death burden per month in farm {}-{}"
    .format(farm.commune, farm.farm_number))

  return f


def plot_flock_traj(flock_ll, bird_type) :
  """
  Plots the trajectory of flocks (of a certain bird type).

  Parameters
  ----------
  flock_l_l : dec.Flock list list
    Flocks grouped by date, grouped by flock_id.
    It is assumed they all have the same bird type.

  Returns
  -------
  f : plt.Figure
    Figure output.

  """
  f, ax = plt.subplots()
  # "Blues" + "Greens"
  palette = sb.color_palette("Blues", n_colors=len(flock_ll))
  for j, flock_l in enumerate(flock_ll) :
    size_a = np.array([ fl.size for fl in flock_l ])
    age_a = np.array([ fl.age for fl in flock_l ])
    # FIXME need a function to fill in missing data
    ax.plot(age_a, size_a, color=palette[j])
  ax.set_title("Trajectories of flocks of type {}"
               .format(bird_type.to_text()))
  ax.set_xlabel("age")
  ax.set_ylabel("flock size")

  return f



def plot_simple_stats(ses) :
  """
  Plot some summary statistics on the database contacted by `ses`.
  Returns 5 figures giving the following information :
  - population size in time by main type
  - confinement repartition by bird type
  - cause_of_death distribution
  - flock size distribution by bird type
  - farm flock size distribution

  Parameters
  ----------
  ses : sqlalchemy.orm.Session
    Session bound to the engine connected to the database.

  """
  # Figure 1 : Population size by main type
  df = DataFrame()
  for polytype in ['front_chicken', 'back_duck', 'front_other'] :
    res = ses.query(dec.Date,
                    func.sum(dec.Flock.size))\
             .select_from(dec.Date)\
             .join(dec.Date.scans)\
             .join(dec.Page.flocks)\
             .outerjoin(dec.Flock.bird_type)\
             .group_by(dec.Page.date_id)\
             .filter(dec.Bird_type.species==polytype)\
             .all()
    # TODO use something better than date_to_time ?
    dates = [ np.datetime64(d.to_datetime()) for d, _ in res ]
    ar = np.array([ (d, polytype, sz)
                    for d, (_, sz) in zip(dates, res) ],
                  dtype=[('time', 'datetime64[D]'),
                         ('species', np.str_, 16),
                         ('flock_size', np.int16)])
    polydf = DataFrame(ar,
                       columns=['time', 'species', 'flock_size'])
    df = df.append(polydf)
  df = df.sort_values(by='time')
  df.index = np.arange(len(df))
  g = sb.FacetGrid(
    df,
    hue='species',
    sharex=True,
    size=4,
    legend_out=True,
  )  # size does what ?
  g = g.map(
    plt.plot,
    'time',
    'flock_size'
  )
  # g.add_legend()
  f1 = g.fig
  ax = f1.get_axes()[0]
  ax.legend(loc='upper right')
  ax.xaxis_date()
  ax.xaxis.set_major_locator(MonthLocator(interval=4))
  ax.xaxis.set_major_formatter(DateFormatter("%Y-%m"))

  # Figure 2 : repartition of confinement by type and flock_id
  f2, ax = plt.subplots()
  res = ses.query(dec.Flock.type_code, dec.Flock.confinement_code)\
           .group_by(dec.Flock.flock_id)\
           .order_by(dec.Flock.type_code)\
           .all()

  df = DataFrame(res, columns=['bird_type', 'confinement_code'])
  sb.countplot(x='bird_type', hue='confinement_code', data=df, ax=ax)

  # Figure 3 : repartition of deaths by cause of death
  f3, ax = plt.subplots()
  res = ses.query(
    dec.Flock.cause_of_death_code,
    dia.sql_expr.func.sum(dec.Flock.death_since_last)
    # / size at last
    # / (dec.Flock.size
    #  - dec.Flock.in_since_last
    #  + dec.Flock.sell_or_slaughter_since_last
    #  + dec.Flock.death_since_last))
  ).group_by(
      dec.Flock.cause_of_death_code
  ).all()
  df = DataFrame(res, columns=['death_code', 'death_rate'])
  # print(df)
  sb.barplot(
    x='death_code',
    y='death_rate',
    data=df,
    estimator=np.sum,
    ax=ax
  )


  # repartition of flock size by type and flock_id
  f4, ax = plt.subplots()
  res = ses.query(
    dec.Flock.type_code,
    dia.sql_expr.func.avg(dec.Flock.size)
  ).group_by(
    dec.Flock.flock_id
  ).order_by(
    dec.Flock.type_code
  ).all()
  df = DataFrame(res, columns=['bird_type', 'flock_size'])
  # print("flock size", df)
  sb.boxplot(x='bird_type', y='flock_size', data=df, ax=ax)

  f5, ax = plt.subplots()
  # distribution of farm size # this is pretty ugly and useless
  # more interesting to have some farm comparison
  # a boxplot of mean flock size
  res = ses.query(dec.Page.farm_id, dia.sql_expr.func.avg(dec.Flock.size))\
           .join(dec.Flock.page)\
           .group_by(dec.Flock.flock_id)\
           .all()
  df = DataFrame(res, columns=['farm_id', 'flock_size'])
  sb.boxplot(x='farm_id', y='flock_size', data=df, ax=ax)

  return f1, f2, f3, f4, f5


def plot_pop_traj(ses) :
  """
  Plot the whole population dynamic as a stacked plot of all flocks.

  """
  date_l = ses.query(dec.Date)\
              .order_by(dec.Date.year, dec.Date.month)\
              .all()
  flock_l = ses.query(dec.Flock)\
               .join(dec.Flock.page)\
               .join(dec.Page.date)\
               .order_by(dec.Flock.flock_id, dec.Date.year, dec.Date.month)\
               .all()
  flock_ll = flockmatch.group_by_id(flock_l)
  f = plot_flocks_traj(date_l, flock_ll)

  return f


def plot_all_farm_traj(ses) :
  """
  Plot the population dynamics record for all farms.

  """
  f_l = list()
  farm_l = ses.query(dec.Farm)\
              .order_by(dec.Farm.commune, dec.Farm.farm_number)\
              .all()
  date_l = ses.query(dec.Date)\
              .order_by(dec.Date.year, dec.Date.month)\
              .all()
  # can we group_by farm instead of iterating ?
  for farm in farm_l :
    # code = "_".join([farm.district, farm.commune, str(farm.farm_number)])
    res = ses.query(dec.Flock.flock_id)\
             .join(dec.Flock.page)\
             .filter(dec.Page.farm_id==farm.id)\
             .all()
    flock_id_s = set([ row[0] for row in res ])
    logger.debug("With flock id values : %s", flock_id_s)
    flock_ll = [ ses.query(dec.Flock)
                    .join(dec.Flock.page)
                    .join(dec.Page.date)
                    .filter(dec.Flock.flock_id==flid)
                    .order_by(dec.Date.year, dec.Date.month)
                    .all()
                  for flid in flock_id_s ]
    f = plot_flocks_traj(date_l, flock_ll, farm)
    # f_l.append((code, f))
    f_l.append(f)

  return f_l


def plot_all_flock_traj(ses) :
  """
  Plot all flock trajectories by bird type, to try to characterize
  flock population dynamics.

  """
  f_l = list()
  for bt in ses.query(dec.Bird_type).all() :
    code = bt.code
    res = ses.query(dec.Flock.flock_id)\
             .filter_by(type_code=code)\
             .all()
    flock_id_s = set([ row[0] for row in res ])
    flock_ll = [ ses.query(dec.Flock)
                    .join(dec.Flock.page)
                    .join(dec.Page.date)
                    .filter(dec.Flock.flock_id==flid)
                    .order_by(dec.Date.year, dec.Date.month)
                    .all()
                  for flid in flock_id_s ]
    f = plot_flock_traj(flock_ll, bt)
    # f_l.append((code, f))
    f_l.append(f)

  return f_l


def plot_flock_stats(ses) :
  """
  Plot some useful flock statistics.

  """
  flock_l = ses.query(dec.Flock).all()
  na_confinement = ses.query(dec.Confinement)\
                      .filter(dec.Confinement.code=='NA')\
                      .one()
  flock_ll = flockmatch.group_by_id(flock_l)
  fl_l = [
    flockmatch.characterize_flock(
      flock_l,
      na_values=[None, na_confinement]
    )
    for flock_l in flock_ll
  ]
  df = DataFrame(
    np.array([ (fl.bird_type.code, fl.bird_type.species, fl.confinement.code,
                fl.all_in_all_out, fl.commercial_feed, fl.boot_change,
                fl.other_people, fl.disinfectant, fl.hpai, fl.newcastle,
                fl.duck_plague) for fl in fl_l ]),
    columns=['bird_type', 'species', 'confinement',
             'all_in_all_out', 'commercial_feed', 'boot_change',
             'other_people', 'disinfectant', 'hpai',
             'newcastle', 'duck_plague'],
  )
  # Figure 1 : Bird type distribution
  f1, ax = plt.subplots()
  sb.countplot(x='bird_type', data=df, ax=ax)

  # Figure 2 : confinement distribution by type
  f2, ax = plt.subplots()
  sb.countplot(x='bird_type', hue='confinement', data=df, ax=ax)

  # Figure 3 : boolean proportions by type
  # ça peut marcher avec countplot ?
  # all_in_all_out, commercial_feed, boot_change, other_people, disinfectant
  # hpai, newcastle, duck_plague, other
  mlt = melt(
    df,
    id_vars=['bird_type', 'species', 'confinement'],
    var_name='attr',
  )

  # Where do the None go ?
  f3, ax = plt.subplots()
  sb.countplot(x='attr', hue='value', data=mlt)

#   g = sb.FacetGrid(
#     mlt,
#     row='species',
#     sharey=True,
#     size=3,
#   )
#   g = g.map(
#     sb.countplot,
#     x='attr',  # Could not interpret input
#     hue='value',
#   )
#   f4 = g.fig

  return f1, f2, f3  # , f4


def page_heatmap(df, value, ax_l) :
  """
  Do a heatmap on value with dates on xaxis, farms on yaxis,
  separated by commune.

  Parameters
  ----------
  df : DataFrame
    Should have columns
    'commune', 'farm_number', 'year', 'month', 'is_verso', value
  value : str
    Name of the column to heatmap
  ax_l : axes list

  """
  df['farm_number'] = to_numeric(
    df['farm_number'],
    downcast='integer'
  )
  df['month'] = to_numeric(
    df['month'],
    downcast='integer'
  )
  df['year'] = to_numeric(
    df['year'],
    downcast='integer'
  )
  df[value] = df[value].astype(float)

  vmin = np.nanmin(df[value].values)
  vmax = np.nanmax(df[value].values)

  for com, ax in zip(['TPH', 'TLO'], ax_l) :
    piv_df = pivot_table(
      df[df['commune']==com],
      index=['farm_number', 'is_verso'],
      columns=['year', 'month'],
      values=value,
      # aggfunc=util.identity
    )
    # FIXME something strange with is_verso order
    # I swapped verso order to at least have farm number
    ylabs = [ str(fn) if iv else " " for fn, iv in piv_df.index ]
    cmap = mcm.get_cmap('Blues')
    # too many pages with no data
    # cmap.set_bad(sb.xkcd_rgb["dark red"])
    sb.heatmap(
      piv_df,
      linewidths=0.3,
      xticklabels=6,
      yticklabels=ylabs,
      vmin=vmin,
      vmax=vmax,
      # cmap='Blues',
      cmap=cmap,
      cbar=True,
      ax=ax,
    )
    # plt.setp(ax.xaxis.get_majorticklabels(), rotation=40)
    plt.setp(ax.yaxis.get_majorticklabels(), rotation='horizontal')
    ax.tick_params(labelsize=8, length=2.)
    ax.set_ylabel('Farm')
    ax.set_xlabel('Date')
    ax.set_title(com)

  return ax_l


def plot_missing(ses) :
  """
  Make a masked heatmap for number of flocks present by page.
  This gives a quick view of the data present and missing in the database.

  """
  # FIXME now a color could correspond to the number of flocks for the page
  # with a special color if the page is absent
  farms = ses.query(dec.Farm.commune, dec.Farm.farm_number).all()
  dates = ses.query(dec.Date.month, dec.Date.year).all()
  # need to row count flocks by page
  res = ses.query(dec.Farm.commune, dec.Farm.farm_number,
                  dec.Date.month, dec.Date.year,
                  dec.Page.is_recto,
                  func.count(dec.Flock.flock_number))\
           .select_from(dec.Page)\
           .join(dec.Page.farm)\
           .join(dec.Page.date)\
           .outerjoin(dec.Page.flocks)\
           .group_by(dec.Page.id)\
           .all()
  l = len(farms) * len(dates) * 2
  # using is_verso instead of is_recto because of automatic
  # reordering in pivot_table
  df = DataFrame(
    np.zeros([l, 6]),
    columns=['commune', 'farm_number', 'month', 'year', 'is_verso', 'nfl'],
  )
  # every flock number is missing (masked in heatmap)
  i = 0
  for com, fn in farms :
    for mth, yr in dates :
      for ir in [True, False] :
        df.iloc[i] = [com, fn, mth, yr, not ir, np.nan]
        i = i + 1
  # except those returned by query
  for com, fn, mth, yr, ir, nfl in res :
    df.loc[(df['commune']==com)
         & (df['farm_number']==fn)
         & (df['month']==mth)
         & (df['year']==yr)
         & (df['is_verso']==(not ir)), 'nfl'] = nfl

  # cast float to integer where warranted
  df['farm_number'] = to_numeric(
    df['farm_number'],
    downcast='integer'
  )
  df['month'] = to_numeric(
    df['month'],
    downcast='integer'
  )
  df['year'] = to_numeric(
    df['year'],
    downcast='integer'
  )

  f, ax_l = plt.subplots(1, 2)
  ax_l = page_heatmap(df, 'nfl', ax_l)

  return f


def plot_flock_distance(ses) :
  """
  Shows the distribution of month-to-month distances by flock id,
  and by page.

  We want :
  - two figures for the distributions
  - two sliders for a cutoff
  - two ListViews to show the bad flock_ids and page_ids

  """
  flock_l = ses.query(dec.Flock)\
               .join(dec.Page)\
               .join(dec.Farm)\
               .join(dec.Date)\
               .order_by(dec.Farm.commune, dec.Farm.farm_number,
                         dec.Date.year, dec.Date.month,
                         dec.Page.is_recto)\
               .all()

  paged_l = list()
  flock_llll = flockmatch.group_flocks(flock_l)
  for sp, fl_lll in flock_llll :
    for fl_ll in fl_lll :
      paged_l.extend(list(flockmatch.score_matches(fl_ll)))
  page_a = np.array([
    (p.farm.commune, p.farm.farm_number,
     p.date.year, p.date.month,
     not p.is_recto, scr)
    for p, scr in paged_l
  ])
  page_df = DataFrame(
    page_a,
    columns=('commune', 'farm_number', 'year', 'month', 'is_verso', 'score'),
  )

  flidd_l = list()
  flock_ll = flockmatch.group_by_id(flock_l)
  for fl_l in flock_ll :
    flidd_l.extend(list(flockmatch.score_flid(fl_l)))
  flidd_df = DataFrame(
    flidd_l,
    columns=('flid', 'score'),
  )

  f1, ax = plt.subplots()
  sb.distplot(
    flidd_df['score'],
    kde=False,
    rug=True,
    ax=ax
  )
  leaderboard = flidd_df.sort_values(by='score', ascending=False)\
                        .head(5)['flid']
  ax.legend([ str(int(n)) for n in leaderboard ], loc='upper right')
  ax.set_title("Distribution of summed distance along flocks")

  f2, ax_l = plt.subplots(1, 2)
  ax_l = page_heatmap(page_df, 'score', ax_l)
  f2.suptitle("Match scores at each farm/date")

  return f1, f2
