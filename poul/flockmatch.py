#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
This module matches flocks.

Flocks come into this module with the same type as used throughout the project,
as defined in poul.db.declar.

For the purpose of matching them, we define an injection
to a simplified state in `flock_to_simple`, defined as
`(species, usage, population size, age, death status)`.
where 'usage' is 'T' for meat, 'D' for layer/breeding, and 'C' for the young,
and death status is `True` if the population size has reached 0,
`False` otherwise.

A distance can then be defined for this simplified state,
(which is therefore a pseudo-distance for the original flock state),
in `flock_distance`.
It is a linear combination of L1 (Manhattan) distances
for the continuous components, and the discrete distance for the discrete
components, with a special handling of `None`s.


Flocks will then be matched only farm-by-farm and type-by-type
(questionnaire side by questionnaire side).
`group_flocks` structures the data in this way.

To then match, in a farm, flocks from month $i$ to flocks from month `i-1`,
we must first make the states of the flocks comparable.
`reverse_one_month_to_simple` predicts the expected state of a flock
at month `i-1` from its state at month `i`.

`compute_distances` can then compute the distance matrix
between two successive months.

We can then solve the (generalized) assignment problem for this matrix.
`match_flocks_ideal` first checks whether the matching is ideal,
that is whether each flock can be assigned to its best matching flock
without conflict (where 'dead' and 'unborn' do not cause conflicts).
In that case, the matching is done.

Otherwise, `match_flocks_bruteforce` tries all possible assignments,
and keeps the best.


The next few functions handle the interfacing with the database ;
getting the flocks we want, matching them, then updating the database
with the appropriate flock ids.

Finally the last functions analyze the state (and goodness) of the matching.

"""

import itertools
from copy import deepcopy
import logging

import numpy as np

from poul.resources import config
from poul import util
from poul.db import declar, dialog

logger = logging.getLogger(__name__)


def combinations(n, k) :
  """
  Light wrapper around `itertools.combinations`.

  Parameters
  ----------
  n : int
    The range of values that can be chosen, (0 to n - 1).
  k : int
    The number of values to choose from the range for each combination.

  Returns
  -------
  t : (int k-tuple * int (n-k)-tuple) iterator
    An iterator on all possible combinations of k among n and their complement.

  """
  cb = itertools.combinations(range(n), k)

  return ( (chosen, tuple([ i for i in range(n) if i not in chosen ]))
           for chosen in cb )


def flock_to_simple(flock) :
  """
  Return a tuple of essential dimensions of `flock`.

  Parameters
  ----------
  flock : declar.Flock
    The flock to 'simplify'.

  Returns
  -------
  t : (string * string * float * float * bool)
    A tuple of the species, bird usage, population size, age,
    and death status.

  """
  tc1, tc2 = flock.type_code
  sp = flock.bird_type.species
  sz = flock.size
  dead = (sz == 0)

  return (sp, tc2, sz, flock.age, dead)


def reverse_one_month_to_simple(flock) :
  """
  Returns a "simple flock" version of flock one month in the past.
  Propagates `None` values or considers `0` was the true value depending.

  Parameters
  ----------
  flock : declar.Flock
    The flock to reverse and simplify.
  """
  try :
    tc1, tc2 = flock.type_code
  except ValueError :  # not enough values to unpack (expected 2, got 0)
    # Some type codes in the database were input badly : catch and then correct
    logger.error("Bad type code : %s", repr(flock))
    raise
  sp = flock.bird_type.species
  size = flock.size

  # Some of the in/out flow values can be None, which raises TypeError.
  # In that case, we ignore the operation (None means 0 here).
  try :
    size -= flock.in_since_last
  except TypeError :
    logger.debug("Caught TypeError on reversing %s for in_since_last : %s",
                 flock, flock.in_since_last)
  try :
    size += flock.death_since_last
  except TypeError :
    logger.debug("Caught TypeError on reversing %s for death_since_last : %s",
                 flock, flock.death_since_last)
  try :
    size += flock.sell_or_slaughter_since_last
  except TypeError :
    logger.debug("Caught TypeError on reversing %s for sos_since_last : %s",
                 flock, flock.sell_or_slaughter_since_last)

  # If the age is None, it should be None.
  try :
    age = flock.age - util.month_to_week(1)
  except TypeError :
    logger.debug("Caught TypeError on reversing %s for age : %s",
                  flock, flock.age)
    age = None

  return (sp, tc2, size, age, False)


def group_flocks(flock_l) :
  """
  Group flocks by farm, type, sorted by date.

  Parameters
  ----------
  flock_l : declar.Flock list
    Those flocks should be all the flocks for some farms + species
    between two dates.

  Returns
  -------
  flock_lll : (str, declar.Flock list list) list
    For each species, for each farm, for each date in order, a list of flocks.

  """
  fl_lddd = dict()
  for fl in flock_l :
    f = fl.page.farm
    d = fl.page.date
    try :
      sp = fl.bird_type.species
    except AttributeError :  # None bird type
      # we have this problem that we want to fix
      logger.error(
        "Flock missing bird type with page id %s, number %s, farm %s, date %s",
        fl.page_id, fl.flock_number, f, d
      )
      raise
    try :
      fl_lddd[sp][f][d].append(fl)
    except KeyError :
      try :
        fl_lddd[sp][f][d] = [fl]
      except KeyError :
        try :
          fl_lddd[sp][f] = {d : [fl]}
        except KeyError :
          fl_lddd[sp] = {
            f : {d : [fl]}
          }

  flock_l_l_l_l = [
    (sp,
      [ (f.commune, f.farm_number,
        [ (d.year, d.month, fl_l)
    for d, fl_l in fl_ld.items() ])
    for f, fl_ld in fl_ldd.items() ])
    for sp, fl_ldd in fl_lddd.items()
  ]

  flock_llll = [
    (sp,
      [ [ fl_l
    for _, _, fl_l in sorted(fl_l_l, key=lambda x : (x[0], x[1])) ]
    for _, _, fl_l_l in sorted(fl_l_l_l, key=lambda x : (x[0], x[1])) ])
    for sp, fl_l_l_l in flock_l_l_l_l
  ]

  return flock_llll


def group_by_id(flock_l) :
  fl_ld = util.OrderedDict()
  for fl in flock_l :
    try :
      fl_ld[fl.flock_id].append(fl)
    except KeyError :
      fl_ld[fl.flock_id] = [fl]
  flock_ll = list(fl_ld.values())

  return flock_ll


def flock_distance(fl1, fl2) :
  """
  L1 distance between simple flocks.

  Symmetrical : `flock_distance(fl1, fl2) = flock_distance(fl2, fl1)`.

  Parameters
  ----------
  fl1 : (str, str, int, float)
    Simple flock.
  fl2 : (str, str, int, float)
    Simple flock.

  """
  l1d_l = list()
  for lbd, x1, x2 in zip(config.match_coeffs, fl1, fl2) :
    try :
      y = np.abs(x1 - x2)
    except TypeError :  # str or None
      y = int(x1 != x2)
    if x1 is None or x2 is None :
      lbd = config.match_e
    l1d_l.append(lbd * y)
  return sum(l1d_l)


def compute_one_distance(flock_past, flock_new) :
  fl1 = flock_to_simple(flock_past)
  fl2 = reverse_one_month_to_simple(flock_new)

  return flock_distance(fl1, fl2)


@util.name_logger
def compute_distances(flock_past_l, flock_new_l, sp=None) :
  """
  Array of distances between `flock_past_l` and `flock_new_l` one month before.

  Parameters
  ----------
  flock_past_l : declar.Flock list
    List of length i.
    All flocks should be from the same farm and date.
  flock_new_l : declar.Flock list
    List of length j.
    All flocks should be from the same farm and the next month.
  sp : str
    species. Default `None`.

  Returns
  -------
  d_a : array-like
    2D array of distances of shape (i+1, j+1).
    The two additional dimensions account for the 'unborn' and 'dead' cases.

  """
  fl_l1 = [ flock_to_simple(flock) for flock in flock_past_l ]
  fl_l2 = [ reverse_one_month_to_simple(flock) for flock in flock_new_l ]
  # n1 = len(fl_l1)
  # n2 = len(fl_l2)
  unborn = (sp, None, 0, 0, False)
  dead = (sp, None, 0, None, True)
  pad_fl_l1 = fl_l1 + [unborn]
  pad_fl_l2 = fl_l2 + [dead]
  d_a = np.array([
    [ flock_distance(fl1, fl2) for fl2 in pad_fl_l2 ]
    for fl1 in pad_fl_l1
  ])

  return d_a


def get_best_matches(a) :
  """
  For each row of a, find the col with the minimum value.
  Returns the result as a list of pairs.

  Parameters
  ----------
  a : array-like
    2D-Array of distances.

  Returns
  -------
  l : (int * int) list
    For each row index, the corresponding best matching column index.
  """
  logger.debug("get_best_matches")
  n = a.shape[0]
  return list(zip(np.arange(n),
                  np.argmin(a, axis=1)))


@util.name_logger
def match_flocks_ideal(d_a) :
  """
  Match the past and new flocks if the line and columns minima correspond,
  accomodating for the (dead, unborn) special case.
  If they don't, return `None`.

  Parameters
  ----------
  d_a : np.array
    Distance matrix between flocks as returned by `compute_distances`.

  Returns
  -------
  ind_l : int tuple list
    For each element `(i, j)`,
    `i` is the past flock number,
    `j` is the corresponding new flock position.
    Each index is present exactly once, except the last positions,
    corresponding to (dead, unborn) which can be absent or present many times.

  """
  # the idea here is to see if the "ideal" match exists.
  # if it exists, it is the best, we don't need to test combinations.
  n1, n2 = d_a[:-1, :-1].shape
  ind_l1 = get_best_matches(d_a[:-1, :])
  inv_ind_l2 = get_best_matches(d_a.T[:-1, :])
  ind_l2 = sorted([ (i, j) for j, i in inv_ind_l2 ],
                  key=lambda pair : pair[0])
  logger.debug("we got (%s, %s) :\n%s\nand\n%s",
               n1, n2, ind_l1, ind_l2)
  sub_ind_l1 = [ (i, j) for i, j in ind_l1 if j < n2 ]
  sub_ind_l2 = [ (i, j) for i, j in ind_l2 if i < n1 ]
  subset1 = set([ j for i, j in sub_ind_l1 ])
  subset2 = set([ i for i, j in sub_ind_l2 ])
  if (len(sub_ind_l1) != len(sub_ind_l2)
   or len(subset1) < len(sub_ind_l1)
   or len(subset2) < len(sub_ind_l2)
   or sub_ind_l1 != sub_ind_l2) :
    logger.debug("Invalid ideal match")
    ind_l = None
  else :  # the ideal match is valid
    logger.debug("Valid ideal match")
    # we build the valid ind_l
    ind_l = sub_ind_l1 \
          + [ (i, j) for i, j in ind_l1 if j == n2 ] \
          + [ (i, j) for i, j in ind_l2 if i == n1 ]
    ind_l = sorted(ind_l)

  return ind_l


@util.name_logger
def match_flocks_bruteforce(d_a) :
  """
  Match past and new flocks by minimizing the month-to-month flock distance.
  This is exhaustive, and expensive for large `d_a`.

  Parameters
  ----------
  d_a : np.array
    Distance matrix between flocks as returned by `compute_distances`.

  Returns
  -------
  ind_l : int tuple list
    For each element `(i, j)`,
    `i` is the past flock number,
    `j` is the corresponding new flock position.
    Each index is present exactly once, except the last positions,
    corresponding to (dead, unborn) which can be absent or present many times.

  """
  # numbers of past and new flocks
  n1, n2 = d_a[:-1, :-1].shape
  # min number of dead matchesand -max number of unborn matches
  kmin = n1 - n2
  if kmin < 0 :
    # the problem is symmetrical
    logger.debug("switch")
    switch = True
    n1, n2 = n2, n1
    kmin = - kmin
    d_a = d_a.T
  else :
    switch = False
  # initially the min distance is infinity
  min_d = np.infty
  for k1 in range(kmin, n1 + 1) :
    # k1 number of dead matches in fl_l1
    logger.debug("with number of dead matches k1 : %s", k1)
    # k2 number of dead (unborn) matches in fl_l2
    k2 = n2 - (n1 - k1)
    # choose the dead matches for fl_l1
    for (to_dead_1, alive_1) in combinations(n1, k1) :
      d_dead_1 = np.sum([d_a[i, -1] for i in to_dead_1])
      min_d2 = np.infty
      # choose the dead matches for fl_l2
      for (to_dead_2, alive_2) in combinations(n2, k2) :
        d_dead_2 = np.sum([d_a[-1, j] for j in to_dead_2])
        min_d_alive = np.infty
        # compare assignments between alive flocks
        for perm_alive_1 in itertools.permutations(alive_1) :
          d_alive = np.sum([d_a[i, j] for i, j in zip(perm_alive_1, alive_2)])
          if d_alive < min_d_alive :
            min_d_alive = d_alive
            # min_combin_3 = ((perm_alive_1, to_dead_1), (alive_2, to_dead_2))
            min_combin_3 = list(zip(perm_alive_1, alive_2)) \
                         + [ (i, n2) for i in to_dead_1 ] \
                         + [ (n1, j) for j in to_dead_2 ]
        d2 = d_dead_2 + min_d_alive
        if d2 < min_d2 :
          min_d2 = d2
          min_combin_2 = deepcopy(min_combin_3)
      d = d_dead_1 + min_d2
      if d < min_d :
        min_d = d
        min_combin = deepcopy(min_combin_2)

  if switch :
    # 'unswitch' the result
    min_combin = [ (i, j) for j, i in min_combin ]
  ind_l = sorted(min_combin)

  return ind_l


def match_flocks(fl_past_l, fl_new_l, species) :
  d_a = compute_distances(fl_past_l, fl_new_l, species)
  ind_l = match_flocks_ideal(d_a)
  if ind_l is None :
    logger.info("Ideal matching failed, bruteforce matching instead")
    ind_l = match_flocks_bruteforce(d_a)

  return ind_l


def rearrange_flocks(past_d, n, fl_new_l, m, ind_l, matched) :
  """
  Make `new_d` a dictionary of matched flocks from `ind_l`.

  Parameters
  ----------
  past_d : (int, declar.Flock tuple) dict
    Matched flocks dict indexed by their past flock position.
  n : int
    Number of past flocks (without unborn).
  fl_new_l : declar.Flock list
    The new flocks we matched against.
  m : int
    Number of new flocks (without dead).
  ind_l : int tuple list
    List of corresponding indexes between past and new flocks.
  matched : declar.Flock tuple list
    List of completed flocks.

  Returns
  -------
  new_d : (int, declar.Flock tuple) dict
    Matched flocks dict indexed by their new flock position.

  """
  new_d = dict()
  for (i, j) in ind_l :
    try :
      fl_t = past_d[i]
    except KeyError :
      logger.debug("caught KeyError, i is %s and n is %s",
                   i, n)
      assert i == n  # we have matched unborn
      fl_t = ()

    try :
      new_d[j] = fl_t + (fl_new_l[j],)
    except IndexError :
      logger.debug("caught IndexError, j is %s and m is %s",
                   j, m)
      assert j == m  # we have matched dead
      # once we match dead, the flock is finished so we append it
      matched.append(fl_t)

  return new_d


def propagate_id(matched_fl_l, counter) :
  """
  Try to set one 'flock_id' for all flocks in `matched_fl_l`.

  Parameters
  ----------
  matched_fl_l : declar.Flock list
    A list of flocks ordered by date which belong to the same flock.
  counter : int iterator
    An iterator that yields new free ids to use.

  """
  for fl_t in matched_fl_l :
    flock_id_s = set([ fl.flock_id for fl in fl_t ]) - set([ None ])
    logger.debug("flock_id_s is %s", flock_id_s)
    # Normal case : if no id set, set an id for all
    if len(flock_id_s) == 0 :
      fl_id = next(counter)
    # if one exists, propagate
    elif len(flock_id_s) == 1 :
      fl_id = next(iter(flock_id_s))
    # if more than one, error
    else :
      raise RuntimeError(
        ("Several flock ids \{{}\} are set "
         "in this matched flock lineage :\n{}").format(
          flock_id_s,
          matched_fl_l,
        )
      )
    for fl in fl_t :  # set the flock id
      logger.debug("flock %s has flock id set to %s", fl, fl_id)
      fl.flock_id = fl_id

  return counter


@util.name_logger
def set_flock_id(flock_ll, species=None, counter=itertools.count(1)) :
  """
  Set a `flock_id` for the flocks in `flock_ll`.

  Parameters
  ----------
  flock_ll : declar.Flock list list
    All flocks should correspond to the same farm, and to a range of date.
    For each date, a list of flocks.
  species : str
  counter : int iterator
    Last used flock_id.

  Returns
  -------
  flock_ll : declar.Flock list list
    Same as input, with flock_id set.
  counter : int iterator
    Incremented counter for max used flock_id.

  """
  # get lists of flocks that will have the same id
  # (need to fix block indices but perhaps more)
  matched = list()
  for fl_past_l, fl_new_l in zip(flock_ll[:-1], flock_ll[1:]) :
    n, m = len(fl_past_l), len(fl_new_l)
    ind_l = match_flocks(fl_past_l, fl_new_l, species)
    try :
      past_d = new_d
    except NameError :  # undefined new_d at first time point
      logger.debug("caught NameError, initializing past_d, new_d")
      past_d = { i : (fl,) for i, fl in enumerate(fl_past_l) }
    new_d = rearrange_flocks(past_d, n, fl_new_l, m, ind_l, matched)

  ## Propagate id among matched flocks
  try :
    # Does matched not already contain everything we want ?
    matched.extend(new_d.values())
  except NameError :
    # flock_l_l was of length 1 ? don't set any id
    logger.debug("new_d was not created, len(flock_ll) is %s", len(flock_ll))
  else :
    # now new_d values are tuples of matched flocks
    counter = propagate_id(matched, counter)

  return flock_ll, counter


def rematch_all_flocks(ses, constr) :
  """
  This redoes the matching from 0, the previous matching is lost.

  Parameters
  ----------
  ses : sqlalchemy.Session
  constr : sqlalchemy.BinaryExpression list
    Constraints to filter on.

  """
  logger.info("Rematching selected flocks")
  flock_l = ses.query(declar.Flock)\
               .join(declar.Page)\
               .join(declar.Farm)\
               .filter(*constr)\
               .all()
  for fl in flock_l :
    fl.flock_id = None
  ses.flush()
  maxid = ses.query(dialog.sql_expr.func.max(declar.Flock.flock_id)).one()[0]
  maxid = maxid if maxid is not None else 0
  counter = itertools.count(maxid + 1)
  flock_llll = group_flocks(flock_l)
  for sp, fl_lll in flock_llll :  # for each species
    for fl_ll in fl_lll :  # for each farm
      try :
        f = fl_ll[0][0].page.farm
        logger.info("Matching for farm %s", f)
      except IndexError :
        pass
      nfl_ll, counter = set_flock_id(fl_ll, species=sp, counter=counter)
  ses.add_all(flock_l)


def match_new_flocks(ses, constr) :
  """
  This tries to do the matching only for the farms when it is not yet done.
  There could be problems on using this if part of a farm is matched.

  Parameters
  ----------
  ses : sqlalchemy.Session
  constr : sqlalchemy.BinaryExpression list
    Constraints to filter on.

  """
  logger.info("Matching unmatched flocks")
  maxid = ses.query(dialog.sql_expr.func.max(declar.Flock.flock_id)).one()[0]
  maxid = maxid if maxid is not None else 0
  counter = itertools.count(maxid + 1)
  flock_l = ses.query(declar.Flock)\
               .join(declar.Page)\
               .join(declar.Farm)\
               .filter(*constr)\
               .all()
  flock_llll = group_flocks(flock_l)
  for sp, fl_lll in flock_llll :
    for fl_ll in fl_lll :
      if any([ fl.flock_id is None for fl_l in fl_ll for fl in fl_l ]) :
        nfl_ll, counter = set_flock_id(fl_ll, species=sp, counter=counter)
  ses.add_all(flock_l)


def index_of_flock_id(flock_l, flid) :
  try :
    return [ i for i, fl in enumerate(flock_l)
             if fl.flock_id == flid ][0]
  except IndexError :
    return -1


def score_matches(flock_l_l) :
  """
  Yield summed by page flock distance.

  Parameters
  ----------
  flock_l_l : declar.Flock list list
    Should correspond to a list of pages ordered by date,
    with the flocks for each.

  """
  for fl_past_l, fl_new_l in zip(flock_l_l[:-1], flock_l_l[1:]) :
    d_a = compute_distances(fl_past_l, fl_new_l)
    flid_s = set([ fl.flock_id for fl in fl_past_l + fl_new_l ])
    d = np.sum([ d_a[index_of_flock_id(fl_past_l, flid),
                     index_of_flock_id(fl_new_l, flid)]
                 for flid in flid_s ])
    yield (fl_past_l[0].page, d)


def score_flid(flock_l) :
  """
  Yield summed month-to-month flock distance.

  Parameters
  ----------
  flock_l : declar.Flock list
    Should correspond to one flock id ordered by date.

  """
  d_a = np.array([ compute_one_distance(fl_past, fl_new)
                   for fl_past, fl_new in zip(flock_l[:-1], flock_l[1:]) ])

  yield (flock_l[0].flock_id, d_a.sum())


class Flock(object) :
  def __init__(self, flid, bird_type, total_in, total_sos,
               all_in_all_out, commercial_feed, confinement,
               grazing_duration, grazing_range, boot_change,
               other_people, which_people, disinfectant, total_death,
               hpai, date_hpai, newcastle, date_newcastle,
               duck_plague, date_duck_plague, other, date_other) :
    self.flock_id = flid
    self.bird_type = bird_type
    self.total_in = total_in
    self.total_sos = total_sos
    self.all_in_all_out = all_in_all_out
    self.commercial_feed = commercial_feed
    self.confinement = confinement
    self.grazing_duration = grazing_duration
    self.grazing_range = grazing_range
    self.boot_change = boot_change
    self.other_people = other_people
    self.which_people = which_people
    self.disinfectant = disinfectant
    self.total_death = total_death
    self.hpai = hpai
    self.date_hpai = date_hpai
    self.newcastle = newcastle
    self.date_newcastle = date_newcastle
    self.duck_plague = duck_plague
    self.date_duck_plague = date_duck_plague
    self.other = other
    self.date_other = date_other


def characterize_flock(flock_l, na_values=[None]) :
  """
  Return a summary of a flock during its lifetime

  Parameters
  ----------
  flock_l : declar.Flock list
    Should correspond to one flock id ordered by date.

  """
  # policy is if an attribute is one value or NA + one value,
  # then we set one value
  # if they're are conflicting values : what do we do ?
  # seems to mostly give us lots of None
  # or another rule for some : later non null takes precedence ?
  attr_d = {'flid' : flock_l[0].flock_id}
  for total_attr, attr in [('total_in', 'in_since_last'),
                           ('total_sos', 'sell_or_slaughter_since_last'),
                           ('total_death', 'death_since_last')] :
    # None is replaced by 0 : Is that correct ?
    a = np.array([ getattr(fl, attr) if getattr(fl, attr) is not None else 0
                   for fl in flock_l ])
    attr_d[total_attr] = a.sum()

  vacc_attr_l = [
    'hpai', 'date_hpai', 'newcastle', 'date_newcastle',
    'duck_plague', 'date_duck_plague', 'other', 'date_other'
  ]
  attr_l = [
    'bird_type', 'all_in_all_out', 'commercial_feed', 'confinement',
    'grazing_duration', 'grazing_range', 'boot_change',
    'other_people', 'which_people', 'disinfectant'
  ] + vacc_attr_l
  for attr in attr_l :
    attr_d[attr] = None
    val_l = [ getattr(fl, attr) for fl in flock_l ]
    val_s = set(val_l)
    if len(val_s) == 1 :
      attr_d[attr] = next(iter(val_s))
    else :
      if attr in ['bird_type'] + vacc_attr_l :
        # keep last value
        attr_d[attr] = util.last_not_none(val_l)
      elif len(val_s) == 2 and val_s.intersection(na_values) != set() :
        v1, v2 = val_s
        if v1 in na_values :
          attr_d[attr] = v1
        elif v2 in na_values :
          attr_d[attr] = v2
        else :
          pass
      elif attr == 'confinement' :
        attr_d[attr] = na_values[1]
      else :
        pass

  return Flock(**attr_d)
