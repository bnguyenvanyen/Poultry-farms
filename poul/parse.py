#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Summary
-------

Functions common to `poul_qt` and `poul_cli` to parse arguments
from the command line.

"""
import logging
import sys

import poul.util as util

logger = logging.getLogger(__name__)

def add_base_arguments(parser) :
  # parser.add_argument(
  #   '-p',
  #   '--protocol',
  #   default="sqlite",
  #   choices=["sqlite", "mysql", "postgresql"],
  #   help="Set the protocol to be used. Defaults to sqlite.")

  parser.add_argument(
    '-db',
    '--database',
    default=":memory:",
    help=("Give the path to the database. "
          "Defaults to keeping the data in memory."))

  parser.add_argument(
    '--safe',
    action="store_true",
    help="Copies the database in memory and uses that one for \
    all following operations.")

  parser.add_argument(
    '-v',
    '--verbose',
    default=0,
    action="count",
    help="Verbosity level.")

  parser.add_argument(
    '-s',
    '--sa_verbose',
    default=0,
    action="count",
    help="Verbosity level for the SQLAlchemy engine.")

  parser.add_argument(
    "--allow",
    default=None,
    help="Allow logging by this module or subpackage. \
    The default is that all logging is allowed")

  parser.add_argument(
    '--log',
    default=None,
    type=str,
    help="Logging file. If not present, logs to stdout.")

  parser.add_argument(  # this might move to base
    '--lang',
    default="en",
    choices=["en", "vn"],
    type=str,
    help="Select language (for some things)."
  )

  return parser


# Proposed edits :
## possibly do not give edit, delete, extract as they are as
## full blown commands, but only let them do the useful stuff
## that way we drop the strange macro expander
## or possibly add additional subparsers for custom usecases
## edit-new, extract-all, etc...
## drop crunch only keep match ?
## can we make match interactive in case of conflicts ?
## we could possibly keep the 'edit', 'delete', 'extract' as parsers
## we call on ourselves ? In that case we can just abandon them
## I think that makes sense
## but should be in its own commit
def add_action_arguments(parser) :

  subp = parser.add_subparsers(help="actions", dest="action")
  parse_create = subp.add_parser('create-db', help="Create a new database.")
  parse_create.add_argument(
    '-d',
    '--dates',
    nargs=2,
    default=['2015-06', '2017-01'],
    help="Date range as yyyy-mm yyyy-mm. Default '2015-06' '2017-02'."
  )
  parse_create.add_argument(
    '-c',
    '--communes',
    type=str,
    nargs='*',
    default=['TLO', 'TPH'],
    help="Communes to include. Default 'TLO' 'TPH'."
  )
  parse_create.add_argument(
    '-f',
    '--farms',
    type=int,
    nargs='*',
    default=[26, 27],
    help="Number of farms per commune. Should be same length as communes.\n"
    "Default 26 27."
  )

  parse_add = subp.add_parser('add-forms', help="Add scanned pages.")
  parse_add.add_argument(
    'pattern',
    nargs='?',
    default=util.path.join('..', '2_split_png_files', '*', "*_T*_*[tk].png"),
    type=str,
    help="Will add the forms expanded from pattern.",
  )

  parse_csv = subp.add_parser('add-csv', help="Add flocks from csv")
  parse_csv.add_argument(
    'source',
    type=str,
    help="Will add from this csv files."
  )

  # parse_sync = subp.add_parser(
  subp.add_parser(
    'sync',
    help="Synchronize database enum tables with poul/resources."
  )

  parse_copy = subp.add_parser('copy', help="Copy the contents from other db.")
  parse_copy.add_argument(
    'source',
    type=str,
    help="Will add from this db files for the already existing farm-dates."
  )

  # parse_edit = subp.add_parser('edit', help="Edit datapoints.")
  # parse_edit.add_argument(
  #   '--select',
  #   **select_d
  # )
  # parse_edit.add_argument(
  #   '--where',
  #   **where_d
  # )

  parse_extract = subp.add_parser(
    'extract',
    help="Extract data as a csv file."
  )
  parse_extract.add_argument(
    'dest',
    default=None,
    nargs='?',
    type=str,
    help="Destination of the CSV. Defaults to '../4_output/database.csv'"
  )
  # parse_extract.add_argument(
  #   '--select',
  #   **select_d
  # )
  # parse_extract.add_argument(
  #   '--where',
  #   **where_d
  # )
  # parse_extract.add_argument(
  #   '--dest',
  #   default=None,
  #   const=sys.stdout,
  #   nargs='?',
  #   help="Destination of the data."
  # )
  # should act as 'extract --select all --where all --dest ...'

  # parse_delete = subp.add_parser(
  #   'delete',
  #   help="Delete data from the database."
  # )
  # parse_delete.add_argument(
  #   '--where',
  #   **where_d
  # )

  parse_plot = subp.add_parser(
    'plot',
    help="Plot data."
  )
  parse_plot.add_argument(
    'cmd',
    choices=[
      'all', 'simple', 'pop', 'farm', 'traj', 'flock', 'match', 'missing'
    ],
    help="Type of plot to produce."
  )
  parse_plot.add_argument(
    '--dest',
    default=None,
    const=sys.stdout,
    nargs='?',
    help="Destination of the output."
  )

  parse_match = subp.add_parser(
    'match',
    help="Match flocks from month to month : assign flock_id.",
  )
  parse_match.add_argument(
    '-a',
    '--all_',
    action='store_true',
    help="Flag to rematch everything from 0.",
  )
  parse_match.add_argument(
    '--commune',
    type=str,
    help="Communes for which to match.",
  )
  parse_match.add_argument(
    '--farm_number',
    type=str,
    help="Farm numbers for which to match.",
  )
  parse_match.add_argument(
    '--is_recto',
    type=str,
    help="Page sides for which to match.",
  )

  subp.add_parser('gui', help="Launch GUI")


  return parser


# def expand_macro(arg_l) :
#   # if no action specified
#   # and if the last thing is
#   #    csv
#   # or match (because crunch match is annoying)
#   ## those add --nogui automatically
#   # or missing ?
#   if all([ act not in arg_l for act in
#            ["add", "edit", "extract", "delete", "crunch", "plot"]]) :
#     if arg_l[-1] == "csv" :
#       expanded_l = arg_l[:-1] + ["--nogui",
#         "extract", "--select", "all", "--where", "all"]
#     elif arg_l[-1] == "match" :
#       expanded_l = arg_l[:-1] + ["--nogui", "crunch", "match"]
#     else :
#       expanded_l = arg_l
#   elif any([ act in arg_l for act in ["crunch", "plot"]]) :
#     expanded_l = ["--nogui"] + arg_l
#   else :
#     expanded_l = arg_l
#
#   return expanded_l


def log_config(verbose, sa_verbose, log, allow, **kw) :
  main_level = min(2, verbose)
  sa_level = min(2, sa_verbose)
  log_level_d = {0 : logging.WARNING,
                 1 : logging.INFO,
                 2 : logging.DEBUG}
  log_config = {
    "version" : 1,
    "formatters" : {
      "fmter" : {
        "format" : "%(name)s -- %(message)s"
      }
    },
    "handlers" : {
    },
    "filters" : {
    },
    "loggers": {
      "sqlalchemy.engine" : {
        "level" : log_level_d[sa_level]
      }
    },
    "root" : {
      "level" : log_level_d[main_level],
      "handlers" : []
    },
    "disable_existing_loggers" : False
  }
  if log is None :
    log_config["handlers"]["console"] = {
      "class" : "logging.StreamHandler",
      "formatter" : "fmter",
      "level" : log_level_d[main_level],
      "stream" : "ext://sys.stdout"
    }
    log_config["root"]["handlers"] = ["console"]
  else :
    log_config["handlers"]["fichier"] = {
      "class" : "logging.FileHandler",
      "formatter" : "fmter",
      "level" : log_level_d[main_level],
      "filename" : log
    }
    log_config["root"]["handlers"] = ["fichier"]
  if allow is not None :
    # for name in args.allow :
    log_config["filters"]["allow"] = {"name" : allow}
    for handler in log_config["handlers"].values() :
      handler["filters"] = ["allow"]

  return log_config


# def parse_select(attr_row, filt_l) :
#   # build an Attribute_row from spec
#   # can we accept subset specs or filter specs ?
#   # implement it as filter ?
#   if "all" in filt_l :
#     return attr_row
#   else :
#     internal_filt_l = [ k for k in filt_l
#                             if k.split(".")[0] not in util.Base_row._names ]
#     external_filt_l = [ k for k in filt_l
#                              if k.split(".")[0] in util.Base_row._names ]
#     name_filt_l = [ k for k in external_filt_l
#                     if len(k.split("."))==1 ]
#     attr_name_filt_l = [ tuple(k.split(".")) for k in external_filt_l
#                          if len(k.split("."))==2 ]
#     internal_filt = dia.Attribute_row.make_filter(internal_filt_l)
#     logger.debug("internal_filt : %s\nname_filt : %s\nattr_name_filt : %s",
#                  internal_filt_l, name_filt_l, attr_name_filt_l)
#
#     def filt(name, attr_name, attr) :
#       return (any([ name in name_filt_l,
#                    (name, attr_name) in attr_name_filt_l,
#                    (name_filt_l==[] and attr_name_filt_l==[]) ])
#           and internal_filt(attr))
#     return util.filteri_row(filt, attr_row)
#
#
# def parse_where(constr_l) :
#   constr = dia.empty_constraints()
#   if "all" in constr_l :
#     return constr
#   else :
#     for kc in constr_l :
#       k, c = kc.split("=")
#       name, attr_name = k.split(".")
#       # FIXME util.query_conv
#       constr[name][attr_name] = c
#     return constr
