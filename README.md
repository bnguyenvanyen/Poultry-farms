Smallholder poultry farms data management and analysis
======================================================


Description
-----------
This program aims at handling the data from the Cà Mau poultry farms study,
with different tasks undertaken :
- Maintain a database and provide tools to dialogue with it (through SQLAlchemy)
  to contain the clean data
- Transcript data from the forms (pictures with handwritten text) to 
  the database, either "by hand" (Graphical User Interface),
  or by optical recognition.
  (for now only by hand)
- Extract data from the database (pandas)
- Basic analysis like flock matching.
- Some convenient plotting functions.

The different routines are accessed through subcommands, with
the subcommand 'gui' launching a GUI.


Some other scripts are also shipped for convenience :  
- `poul_pdf_to_png.sh` to split up pdfs into grayscale png
- `skipper.py` to copy PNGs with apt data-informed names
- `unskipper.py` to copy the remaining PNGs skipped by `skipper.py`
- `deskew_pages.py` to deskew the apt-named PNGs
- `check_sides.py` to check that every front has a back


Licensing
---------
This program is under the GPLv3 license, details are in the file LICENSE.

The latest version
------------------
Is to be found at https://gitlab.com/bnguyenvanyen/Poultry-farms

Installation
------------
Install with python3 setup.py install. (broken ?)
The dependencies that must be satisfied follow
(debian or pip packages are listed)

For everything :  
- Python3 : python3, python3-dev
- Numpy : python3-numpy
- Pandas : python3-pandas
- SQLAlchemy : python3-sqlalchemy
- Python Imaging Library (PIL) : python3-pil

For 'add-forms' :  
- Opencv3 : build from source. Instructions at
  http://docs.opencv.org/3.1.0/d7/d9f/tutorial_linux_install.html

For 'plot' :  
- Matplotlib : python3-matplotlib
- Seaborn : python3-seaborn

For 'gui' :  
- PyQt5 : python3-pyqt5 (and dependencies)


Getting started
---------------
Run :  
```
  python3 poul.py --help
```
to get a first look at the options.

Typical workflow is 'create-db', 'add-forms', then 'add-csv' or 'gui'>Edit.  
Then 'flockmatch' to follow flock history,
then 'plot' to do some basic analysis.  
And 'extract' to get a csv file for other analyses.

Example
-------
```
python3 poul.py -db 2017_TPH.db create-db --dates 2017-01 2017-12 --communes TPH --farms 27
python3 poul.py -db 2017_TPH.db add-forms "../2_split_png_files/2017*/*_TPH_*.png"
python3 poul.py -db 2017_TPH.db gui
```

Then in the GUI, `Edit > Pages from query`
Query for :  
  new : True  
  commune : TPH  
  month : 1-12  
  year : 2017  

Be careful, changes are only committed to the database 
at the end of the process.

Documentation
-------------
The API reference was built with Sphinx, and can be found in 'doc/'.  
You can use [index.html](doc/_build/html/index.html) as a start-point.  
Otherwise, please refer to the (extensive) documentations 
of the libraries the project is built upon.
(Mostly SQLAlchemy and PyQt, but also Numpy, Matplotlib or Pyocr).

Contact
-------
You can ask question or report any bugs to the author at
benjamin.nguyen.van.yen@gmail.com
